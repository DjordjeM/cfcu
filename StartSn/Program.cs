﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using SenseNet.ContentRepository;
using SenseNet.ContentRepository.Storage;

namespace StartSn
{
    class Program
    {
        static void Main(string[] args)
        {


            var startSettings = new RepositoryStartSettings
            {
                Console = Console.Out,
                StartLuceneManager = StorageContext.Search.IsOuterEngineEnabled,
                PluginsPath = AppDomain.CurrentDomain.BaseDirectory,
                StartWorkflowEngine = true
            };

            Repository.Start(startSettings);

            //http://stackoverflow.com/questions/2366168/run-console-application-from-other-console-app
            if (args.Length == 0)
            {
                StartBackup();
            }
            else
            {
                try
                {
                    StartBackup(args[0], args[1], args[2]);
                }
                catch(Exception e)
                {
                    string sSource = "StartSn";
                    string sLog = "Application";
                    string sEvent = e.Message;

                    if (!EventLog.SourceExists(sSource))
                        EventLog.CreateEventSource(sSource, sLog);

                    EventLog.WriteEntry(sSource, sEvent);
                    EventLog.WriteEntry(sSource, sEvent,
                        EventLogEntryType.Error, 234);
                }
            }

            Repository.Shutdown();
            
            
            //string sSource;
            //string sLog;
            //string sEvent;

            //WebRequest request = WebRequest.Create("http://localhost");
            //request.Timeout = 300000;
            //try
            //{
            //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //}
            //catch (WebException ex)
            //{
            //    //if (response == null || response.StatusCode != HttpStatusCode.OK)
            //    //{
            //        sSource = "StartSn";
            //        sLog = "Application";
            //        sEvent = ex.Message;

            //        if (!EventLog.SourceExists(sSource))
            //            EventLog.CreateEventSource(sSource, sLog);

            //        EventLog.WriteEntry(sSource, sEvent);
            //        EventLog.WriteEntry(sSource, sEvent,
            //            EventLogEntryType.Error, 234);
            ////}
            
            //}
        }

        static void StartBackup(string backupType="-FULL", string fsPath="-WEB C:\\inetpub\\Sensenet ", string dbPath="-DATABASE F:\\backup ")
        {

            string ArgumentsString;
            
            ProcessStartInfo startInfo = new ProcessStartInfo("Backup.exe");
            
            startInfo.CreateNoWindow = false;
            startInfo.RedirectStandardOutput = true;

            switch (backupType)
            {
                case "-FULL":
                    ArgumentsString = backupType + " " + fsPath + " " + dbPath;
                    break;
                case "-DATABASE": 
                    ArgumentsString = backupType  + " " + dbPath;
                    break;
                default:
                    ArgumentsString = "-INDEX";
                    break;
            }

            startInfo.Arguments = ArgumentsString;

            Process.Start(startInfo);
        }
    }
}
