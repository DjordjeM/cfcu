﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.UI.SingleContentView" %>
<%@ Import Namespace="SenseNet.ContentRepository" %>
<%@ Import Namespace="SenseNet.ContentRepository.Fields" %>
<%@ Import Namespace="SenseNet.Portal.UI.Controls" %>
<%@ Import Namespace="SenseNet.Portal.Virtualization" %>
<%@ Import Namespace="SenseNet.Search" %>


<sn:ScriptRequest ID="Scriptrequest2" runat="server" Path="$skin/scripts/sn/SN.AdvancedSearch.js" />
<sn:ScriptRequest ID="Scriptrequest11" runat="server" Path="$skin/scripts/datatables/DataTables-1.10.10/js/jquery.dataTables.min.js" />
<%--<sn:ScriptRequest ID="Scriptrequest4" runat="server" Path="$skin/scripts/datatables/Buttons-1.1.0/js/dataTables.buttons.min.js" />
<sn:ScriptRequest ID="Scriptrequest3" runat="server" Path="$skin/scripts/datatables/JSZip-2.5.0/jszip.min.js" />
<sn:ScriptRequest ID="Scriptrequest6" runat="server" Path="$skin/scripts/datatables/pdfmake-0.1.18/build/pdfmake.min.js" />
<sn:ScriptRequest ID="Scriptrequest5" runat="server" Path="$skin/scripts/datatables/pdfmake-0.1.18/build/vfs_fonts.js" />

<sn:ScriptRequest ID="Scriptrequest9" runat="server" Path="$skin/scripts/datatables/Buttons-1.1.0/js/buttons.flash.min.js" />

<sn:ScriptRequest ID="Scriptrequest7" runat="server" Path="$skin/scripts/datatables/Buttons-1.1.0/js/buttons.html5.min.js" />
<sn:ScriptRequest ID="Scriptrequest8" runat="server" Path="$skin/scripts/datatables/Buttons-1.1.0/js/buttons.print.min.js" />

<sn:ScriptRequest ID="Scriptrequest10" runat="server" Path="$skin/scripts/datatables/RowReorder-1.1.0/js/dataTables.rowReorder.min.js" />

<sn:CssRequest ID="CssRequest2" runat="server" CSSPath="$skin/styles/datatables/DataTables-1.10.10/css/jquery.dataTables.min.css" />
<sn:CssRequest ID="CssRequest3" runat="server" CSSPath="$skin/styles/datatables/Buttons-1.1.0/css/buttons.dataTables.min.css" />
<sn:CssRequest ID="CssRequest4" runat="server" CSSPath="$skin/styles/datatables/font-awesome-4.4.0/css/font-awesome.min.css" />
<sn:CssRequest ID="CssRequest5" runat="server" CSSPath="$skin/styles/datatables/RowReorder-1.1.0/css/rowReorder.dataTables.min.css" />--%>

<%--<sn:ScriptRequest ID="Scriptrequest1" runat="server" Path="$skin/scripts/sn/SN.Picker.js" />--%>
<sn:CssRequest ID="CssRequest1" runat="server" CSSPath="$skin/styles/SN.AdvancedSearch.css" />
<sn:CssRequest ID="kendocss1" runat="server" CSSPath="$skin/styles/kendoui/kendo.common.min.css" />
<sn:CssRequest ID="kendocss2" runat="server" CSSPath="$skin/styles/kendoui/kendo.metro.min.css" />

<%--Sencha ExtJS--%>

<sn:CssRequest ID="CssRequest111" runat="server" CSSPath="$skin/scripts/ext-5.1.1/build/packages/ext-theme-neptune/build/resources/ext-theme-neptune-all_01.css" />
<sn:CssRequest ID="CssRequest112" runat="server" CSSPath="$skin/scripts/ext-5.1.1/build/packages/ext-theme-neptune/build/resources/ext-theme-neptune-all_02.css" />

<sn:ScriptRequest ID="Scriptrequest111" runat="server" Path="$skin/scripts/ext-5.1.1/build/ext-all.js"/>
<sn:ScriptRequest ID="Scriptrequest112" runat="server" Path="$skin/scripts/ext-5.1.1/build/packages/ext-theme-neptune/build/ext-theme-neptune.js"/>
<sn:ScriptRequest ID="Scriptrequest1" runat="server" Path="$skin/scripts/ext-5.1.1/build/examples/ux/grid/TransformGrid.js"/>

<%  
    var currentUser = SenseNet.ContentRepository.User.Current;
    SenseNet.ContentRepository.Storage.Security.IGroup adminGroup = null;
    using (new SenseNet.ContentRepository.Storage.Security.SystemAccount())
    {
        adminGroup = SenseNet.ContentRepository.Group.Administrators;
        //fasf = SenseNet.ContentRepository.Repository.;
    }

    if (adminGroup != null && currentUser.IsInGroup(adminGroup))
    {
        var allowedchildtypes = (SenseNet.Portal.Virtualization.PortalContext.Current.ContextNode as SenseNet.ContentRepository.GenericContent).GetAllowedChildTypes().ToArray();

        Dictionary<String, String> contentTypes = new Dictionary<String, String>();
        foreach (var ctd in allowedchildtypes.Select(ct => SenseNet.ContentRepository.Content.Create(ct)))
        {
            contentTypes.Add(ctd.Name, ctd.DisplayName);
        }
        var ctds = Newtonsoft.Json.JsonConvert.SerializeObject(contentTypes);
        
        
        //var settings = new QuerySettings { Top = 1 };
        //var results = ContentQuery.Query("InTree:/Root/Sites/Default_Site/IPA2013 +TypeIs:TransmissionNote", settings);
        //var content = SenseNet.ContentRepository.Content.LoadByIdOrPath(results.Identifiers.FirstOrDefault().ToString());

        var ctype = SenseNet.ContentRepository.Schema.ContentType.GetByName("TransmissionNote");
        var doctypesFs = (ChoiceFieldSetting)ctype.GetFieldSettingByName("DocumentType");
        var doctypesOptions = doctypesFs.Options;
        
        //ChoiceFieldSetting doctypes = ((ChoiceFieldSetting)content.Fields["DocumentType"].FieldSetting);
        //var doctypesOptions = doctypes.Options;
        
        
        string doctypesOptionscombined = "[";
        //string doctypesOptionscombined = String.Join(",",doctypesOptions.F);

        foreach (var item in doctypesOptions)
        {
            doctypesOptionscombined += "\"" + item.Text + "\"" + ",";
        }

        doctypesOptionscombined = doctypesOptionscombined.Replace("\"" + "None" + "\"" + ",","");
        doctypesOptionscombined = doctypesOptionscombined.TrimEnd(',');
        doctypesOptionscombined = doctypesOptionscombined + "]";

       // Response.Write(this.ParentPath + "<br />");
%>

<input type="hidden" value='<%= ctds%>' id="ctdString" />
<input type="hidden" value='<%= doctypesOptionscombined%>' id="doctypesString" />
<asp:Panel CssClass="sn-tags" runat="server" ID="quickPanel" DefaultButton="btnAdvancedSearch">
<div class="sn-advanced-search-row sn-inputunit ui-helper-clearfix">
        <div class="label sn-iu-label">
            <span class="sn-iu-title">
                Period Start<br />
            </span>
        </div>
        <input type="text" cssclass="sn-ctrl-text sn-ctrl-medium ui-widget-content ui-corner-all" runat="server" id="periodStart" class="datepicker">
    </div>
<div class="sn-advanced-search-row sn-inputunit ui-helper-clearfix">
        <div class="label sn-iu-label">
            <span class="sn-iu-title">
                Period End
            </span>
        </div>
        <input type="text" cssclass="sn-ctrl-text sn-ctrl-medium ui-widget-content ui-corner-all" runat="server" id="periodEnd" class="datepicker">
    </div>
<div class="sn-advanced-search-row sn-inputunit ui-helper-clearfix">
        <div class="label sn-iu-label">
            <span class="sn-iu-title">Document Type
            </span>
        </div>
        
        <input runat="server" id="docType" data-bind="value: selectedType" data-placeholder="Select type..." style="width:250px" class="doc">

        <script type="text/javascript">

            var doct = document.getElementById("doctypesString").value;
            var dataX = JSON.parse(doct);

            var viewModel = kendo.observable({
                isPrimitive: true,

                selectedType:dataX//"Contract notice,Corrigendum,Tender dossier/Call for proposals,Evaluation committee,Evaluation reports and award decissions,Contract,Contract Addenda"
            });
            kendo.bind($('.doc'), viewModel);
        </script>
        <%--<input type="text" data-bind="value: value, source: name" cssclass="sn-ctrl-text sn-ctrl-medium ui-widget-content ui-corner-all" runat="server" id="docType" class="doctype" style="width:250px">--%>
        <%--<input type="text" data-value-update="keyup" data-bind="value: inputValue" cssclass="sn-ctrl-text sn-ctrl-medium ui-widget-content ui-corner-all" runat="server" id="docType" class="doctype" style="width:250px">--%>
         <%--<sn:CheckBoxGroup ID="docType" runat="server" FieldName="Style" />--%>
    </div>
    <%--<div class="sn-advanced-search-row sn-inputunit ui-helper-clearfix">
        <div class="label sn-iu-label">
            <span class="sn-iu-title">Format
            </span>
        </div>
        <input cssclass="sn-ctrl-text sn-ctrl-medium ui-widget-content ui-corner-all" runat="server" id="rptFormat" class="format">
    </div>--%>
</asp:Panel>
<div class="sn-advanced-search-row">
    <asp:Button CssClass="sn-button sn-submit ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" runat="server" ID="btnAdvancedSearch" class="submit-button" Text="Search" />
</div>
<%}
    else
    {
%>
<p><%=GetGlobalResourceObject("ParametricSearchPortlet", "LoginNotification")%></p>
<%   
    }%>