﻿// using $skin/scripts/jquery/jquery.js
// using $skin/scripts/kendoui/kendo.web.min.js
// resource ParametricSearchPortlet

//var ctds = JSON.parse($('#ctdString').val());
//var types = [];
//$.each(ctds, function (i, item) {
//    var type = {};
//    type.name = i;
//    type.displayName = item;
//    types.push(type);
//});

//var data = document.getElementById("doctypesString").value//$('#doctypesString').val();

//document.getElementById("doctypesString").value;
//alert(data);
//var data = [
//                     "Contract notice", "Corrigendum", "Tender dossier/Call for proposals", "Evaluation committee",
//                       "Evaluation reports and award decissions", "Contract", "Contract Addenda"
//];


$(function () {
    //if ($('#ctdString').length > 0) {

    document.forms[0].action = document.location.protocol + "//" + document.location.hostname + document.location.pathname;

    var data = document.getElementById("doctypesString").value;
    //datanew = JSON.parse
    //alert(JSON.parse(data));

    //alert($('#progString').val());

    $('.doc').kendoMultiSelect({
        autoClose: false,
        dataSource: {
            data: JSON.parse(data)
        },
        value: JSON.parse(data),
        dataBound: function () {
            //alert('ondatabound fired!');
            //$('.doc').val(kendo.stringify(data, null, 4));
            $('.doc').val(JSON.parse(data).join());
            //alert('databound: ' + $('.doc').val());
        },
        change: function (e) {
            var value = this.value();
            //alert('change event fired2, value: ' + value);
               
            $('.doc').val(value);
            //alert('doctype field value: ' + $('.doc').val())
            //alert($('doc').val());
        }
    });

    var prgtwo = $('#progString').val();

   //$('.prog').kendoMultiSelect({
       // autoClose: false,
       // placeholder: "Select program...",
       //datasource: JSON.parse(prgtwo),//{
            //programs: JSON.parse(prgtwo)
        //},
        //dataTextField: "DisplayName",
       //dataValueField: "Name",
        //value: ["IPA2013"],
        //dataBound: function () {
        //alert('ondatabound fired!');
        //$('.doc').val(kendo.stringify(data, null, 4));
        //$('.doc').val(JSON.parse(data).join());
        //$('.prog').val(JSON.parse(prgtwo).join());
            //alert('databound: ' + $('.prog').value);

        //var value = this.widget.dataItems();
       // alert(value);
        //},
    //   change: function () {
    //       alert('onchage fired!');
    //    this._initChange = true;
    //    var value = this.widget.dataItems();
    //        alert(value);      
    //       //stringify
    //    value = JSON.stringify(value);
        
    //    this.bindings.stringifyValue.set(value || null);
          
    //    this._initChange = false;
    //}
    //});

    $("#grid3").DataTable({
        dom: '<"top"Brt><"clear">',
        buttons: [
        {
            extend: 'copyHtml5',
            text: '<i class="fa fa-files-o"></i>',
            titleAttr: 'Copy'
        },
            {
                extend: 'excelHtml5',
                text: '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel'
            },
            {
                extend: 'csvHtml5',
                text: '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV'
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                //customize: function (win) {
                //    $(win.document.body).find('td')
                //        //.addClass('compact')
                //        .css('text-align', 'center');
                //}
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print"></i>',
                titleAttr: 'Print',
                customize: function (win) {
                    $(win.document.body).find('td')
                        //.addClass('compact')
                        .css('text-align', 'center');
                }
            },
        ],
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false,
        "rowReorder": true,
       // "scrollY": 200,
        "paging": false,
        //"dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>'
        }
        );

    $("#grid4").DataTable({
        dom: '<"top"Brt><"clear">',
        "columnDefs": [
            { "visible": false, "targets": 9 }
        ],
        buttons: [
        {
            extend: 'copyHtml5',
            text: '<i class="fa fa-files-o"></i>',
            titleAttr: 'Copy'
        },
            {
                extend: 'excelHtml5',
                text: '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel'
            },
            {
                extend: 'csvHtml5',
                text: '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV'
            },
            {
                extend: 'pdfHtml5',
                text: '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                //customize: function (win) {
                //    $(win.document.body).find('td')
                //        //.addClass('compact')
                //        .css('text-align', 'center');
                //}
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print"></i>',
                titleAttr: 'Print',
                customize: function (win) {
                    $(win.document.body).find('td')
                        //.addClass('compact')
                        .css('text-align', 'center');
                }
            },
        ],
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false,
        "rowReorder": false,
        // "scrollY": 200,
        "paging": false,
        "drawCallback": function (settings) {
            var api = this.api();
            var rows = api.rows({ page: 'current' }).nodes();
            var last = null;

            api.column(9, { page: 'current' }).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                        '<tr class="group"><td>&nbsp;</td><td colspan="9" align="center"><strong>' + group + '</string></td></tr>'
                    );

                    last = group;
                }
            });
        }
        //"dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>'
    }
        );

    $("#grid6").DataTable({
        dom: '<"top"Brt><"clear">',
        "columnDefs": [
            { "visible": false, "targets": 25 }
            ],
        buttons: [
        {
            extend: 'copyHtml5',
            text: '<i class="fa fa-files-o"></i>',
            titleAttr: 'Copy'
        },
            {
                extend: 'excelHtml5',
                text: '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel'
            },
            {
                extend: 'csvHtml5',
                text: '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV'
            },
            {
                extend: 'pdfHtml5',
                text: '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                //customize: function (win) {
                //    $(win.document.body).find('td')
                //        //.addClass('compact')
                //        .css('text-align', 'center');
                //}
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print"></i>',
                titleAttr: 'Print',
                customize: function (win) {
                    $(win.document.body).find('td')
                        //.addClass('compact')
                        .css('text-align', 'center');
                }
            },
        ],
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false,
        "rowReorder": false,
        // "scrollY": 200,
        "paging": false,
        "drawCallback": function (settings) {
            var api = this.api();
            var rows = api.rows({ page: 'current' }).nodes();
            var last = null;

            api.column(25, { page: 'current' }).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                        '<tr class="group"><td colspan="25"><strong>' + group + '</string></td></tr>'
                    );

                    last = group;
                }
            });
        }
        //"dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>'
    }
        );


    $("#grid").kendoGrid({
        //toolbar: ["Excel"],
        //excel: {
        //    fileName: "Kendo UI Grid Export.xlsx"
        //},
        //dataSource: {
        //    type: "odata",
        //    transport: {
        //        read: "http://demos.telerik.com/kendo-ui/service/Northwind.svc/Products"
        //    },
        //    pageSize: 7
        //},
        // sortable: true,
        // pageable: true,
        //columns: [
        //    { width: 300, title: "Product Name" },
        //    { title: "Units On Order" },
        //    { title: "Units In Stock" }
        //]
    });
    //tableToGrid("#grid2");

   // $("#grid2").kendoGrid({
        //toolbar: ["Excel"],
        //excel: {
        //    fileName: "Kendo UI Grid Export.xlsx"
        //},
        //dataSource: {
        //    type: "odata",
        //    transport: {
        //        read: "http://demos.telerik.com/kendo-ui/service/Northwind.svc/Products"
        //    },
        //    pageSize: 7
        //},
        // sortable: true,
        //groupable: true,
        //columnMenu: true,
        //columns: [
        //    { width: 300, field: "Program", title: "Program Name"},
        //    { title: "Q1", 
        //        //columns:[{
        //        //    field:"qsub"},
        //        //{field:"qrate"}]
        //    },
        //        {
        //            title: "Q2",
        //            //columns: [{
        //            //    field: "q2sub",title: "Sub."},
        //            //{ field: "q2rate", title: "Rej." }]
        //        },
        //        {
        //            title: "Q3",
        //            //columns: [{
        //            //    field: "q3sub",title: "Sub."},
        //            //{ field: "q3rate", title: "Rej." }]
        //        },
        //        {
        //            title: "Q4",
        //            //columns: [{
        //            //    field: "q4sub",title: "Sub."},
        //            //{ field: "q4rate", title: "Rej." }]
        //        },
        //    ]
        //});

        //var prg = JSON.parse($('#progString').val());
        //alert(prg);
        //var programs = [];
        //$.each(prg, function (i, item) {
        //    var program = {};
        //    program.name = i;
        //    program.displayName = item;
        //    programs.push(program);
        //});
    
    
    //kendo.bind($('.prog'), viewModel);
        //alert(programs)
        //var ctds = JSON.parse($('#ctdString').val());

        //var types = [];
        //$.each(ctds, function (i, item) {
        //    var type = {};
        //    type.name = i;
        //    type.displayName = item;
        //    types.push(type);
        //});

        //types.sort(function (a, b) {
        //    if (a.displayName > b.displayName) {
        //        return 1;
        //    }
        //    if (a.displayName < b.displayName) {
        //        return -1;
        //    }
        //    // a must be equal to b
        //    return 0;
        //});



        $(".text").kendoMaskedTextBox();

        $(".datepicker").kendoDatePicker();

       // $(".multiselect").kendoMultiSelect();

        //$(".type").kendoDropDownList({
        //    dataTextField: "displayName",
        //    dataValueField: "name",
        //    dataSource: types,
        //    optionLabel: $('#res1').val()
        //});

        //var users = [];
        //var getUsers = $.ajax({
        //    url: "/OData.svc/Root/IMS?$select=DisplayName,Id&query=TypeIs:User&metadata=no",
        //    dataType: "json",
        //    type: "GET"
        //}).done(function (d) {
        //    $.each(d.d.results, function (i, item) {
        //        users.push(item);
        //    });

            $(".format").kendoDropDownList({
                //dataTextField: "DisplayName",
                //dataValueField: "Id",
                dataSource: {
                    data:["HTML", "Excel"]
                }
                //optionLabel: $('#res2').val()
            });
//        });

        $('.datepicker').bind("focus", function () {
            $(this).data("kendoDatePicker").open();
        });

        //$('.addPath').click(function () {
        //    if (!resultData) return;
        //    $('.path.text').val(resultData[0].Path);
        //    console.log(resultData[0].Path);
        //    var Input = $('.path');
        //    var strLength = Input.val().length * 2;
        //    Input.focus(); Input[0].setSelectionRange(strLength, strLength);
        //    return false;
        //});



        //var documenttypes = [];
        //var getDocumentTypes = $.ajax({
        //    url: "/OData.svc/transmissionnote('documenttype')",
        //    dataType: "json",
        //    type: "GET"
        //}).done(function (d) {
        //    $.each(d.d.results, function (i, item) {
        //        documenttypes.push(item);
        //    });
        //});
        

});
