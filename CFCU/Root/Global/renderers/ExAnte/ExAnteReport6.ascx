﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.Portlets.ContentCollectionView" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SenseNet.Portal.Portlets" %>
<%@ Import Namespace="SenseNet.Portal.Helpers" %>
<%@ Import Namespace="SenseNet.Portal.Virtualization" %>
<%@ Import Namespace="SenseNet.ContentRepository" %>
<%@ Import Namespace="SenseNet.ContentRepository.Storage" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="CFCU.Code" %>
<%@ Import Namespace="Newtonsoft.Json" %>


<%--<div class="sn-search-result-count">
    <span><%=GetGlobalResourceObject("SearchPortlet", "ResultCount")%>: </span><strong><%=Model.Pager.TotalCount%></strong>
</div>--%>

<div style="display:none">
    <sn:ActionMenu ID="ActionMenu1" runat="server" Text="hello" NodePath="/root" Scenario="ListItem"></sn:ActionMenu>
</div>
<% 
    List<string> allowedTypes = new List<string>();
    List<string> programs = new List<string>();

    foreach (var key in HttpContext.Current.Request.Form.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault();
        string keyValue = "";

        if (keyString == "docType")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            allowedTypes = keyValue.Split(',').ToList();
        }

        if (keyString == "Program")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            programs = JsonConvert.DeserializeObject<List<string>>(keyValue);
        }


        keyValue = HttpContext.Current.Request.Form[key];
    }

    //var typegroups2 = this.Model.Items.OrderBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType")));

    List<string> proctypes = new List<string>();
    proctypes.Add("Services");
    proctypes.Add("Supplies");
    proctypes.Add("Works");
    proctypes.Add("Grants");
    proctypes.Add("Twinnings");
    proctypes.Add("Framework");
                  
    List<Tuple<string, string, int, int, int, int>> dataitems = new List<Tuple<string, string, int, int, int, int>>();
    
    //populate default values

    for (int x = 1; x <= 4; x++)
    {
        foreach (var proctype in proctypes)
        {
            foreach (var dtype in allowedTypes)
            {
                dataitems.Add(new Tuple<string, string, int, int, int, int>(proctype, dtype, x, 0, 0, 0));
            }
        }
    }

        foreach (var content in this.Model.Items)
        {
            string docpath = content.Path;
            bool match = false;
            foreach (var program in programs)
            {
                if (docpath.Contains(program))
                {
                    match = true;
                    break;
                }
            }

            if (match == false)
                continue;
            
            int countapproved = 0;
            int countsuspended = 0;
            int countrejected = 0;
            var procuremnttype = content.ContentHandler.Parent.Parent.NodeType.Name.Replace("IPAProject", "");

            string doctype = content.ContentHandler.GetProperty<string>("EADocumentType");

            int submittedversion = content.ContentHandler.GetProperty<int>("EASubmittedVersion");

            string decision = content.ContentHandler.GetProperty<string>("EADecision");
            switch (content.ContentHandler.GetProperty<string>("EADecision"))
            {
                case "Approved":
                    countapproved++;
                    break;

                case "Suspended":
                    countsuspended++;
                    break;

                case "Rejected":
                    countrejected++;
                    break;
            }

            if (allowedTypes.Contains(doctype, StringComparer.OrdinalIgnoreCase))
            {
                dataitems.Add(new Tuple<string, string, int, int, int, int>(procuremnttype, doctype, submittedversion, countapproved, countsuspended, countrejected));
            }
        }
    DataTable consolidatedtable = new DataTable();
  for (int version = 1; version <=2; version++)
  {
      var final3 = dataitems.
          GroupBy(t => new { t.Item3, t.Item2, t.Item1 })
           .Select((g) =>
                    new
                    {
                        ProcurementType = g.Key.Item1,
                        DocumentType = g.Key.Item2,
                        Quarter = g.Key.Item3,

                        TotalApproved = g.Sum(x => x.Item4),
                        TotalSuspended = g.Sum(x => x.Item5),
                        TotalRejected = g.Sum(x => x.Item6),
                        TotalSubmitted = g.Sum(x => x.Item4) + g.Sum(x => x.Item5) + g.Sum(x => x.Item6),
                        ver = g.Key.Item3
                    })
                    .OrderBy(x => x.Quarter)
                  .Where(x => x.Quarter == version);

    var table = final3.ToPivotTable(
             item => item.ProcurementType,
             item => item.DocumentType,

             item => item.Any() ? item.First().TotalApproved : 0,
             item => item.Any() ? item.First().TotalSuspended : 0,
             item => item.Any() ? item.First().TotalRejected : 0,
             item => item.Any() ? item.First().TotalSubmitted : 0,
             version
        );
    
      if (version == 1)
      {
          //check if datatable not empty first
          consolidatedtable = table;
      }
      else
      {   
          foreach (DataRow dr in table.Rows)
          {
              consolidatedtable.Rows.Add(dr.ItemArray);
          }
      }
    
  }
  
    Response.Write(consolidatedtable.ConvertDataTableToHtmlTable(1) + "<br />");
%>
<%--<div class="sn-article-list sn-article-list-shortdetail">
    <ul>
    <%//var typegroups = this.Model.Items.OrderBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType")));
        foreach (var content in this.Model.Items)
        {
            var actionName = content.ContentHandler is User ? "Profile" : "Browse";
          %>
      
        <div class="sn-search-result ui-helper-clearfix">
            <div style="float:left; padding:3px 10px 3px 10px;">
              <%= SenseNet.Portal.UI.IconHelper.RenderIconTag(content.Icon, null, 32) %>
            </div>            
            <div style="padding:3px 0 5px 0;">               
              <a href="<%=Actions.ActionUrl(content, actionName)%>"><%= HttpUtility.HtmlEncode(content.DisplayName) %></a>
              <br/>
              <%= content.ContentHandler.ParentId == 0 ? string.Empty : content.ContentHandler.Parent.GetProperty<string>("DocumentType")  + "<br />" + content.ContentHandler.GetProperty<string>("Decision")%>
            </div>
        </div>
        
    <%} %>
    </ul>
</div>--%>



