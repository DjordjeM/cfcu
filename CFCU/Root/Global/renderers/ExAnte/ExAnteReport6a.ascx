﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.Portlets.ContentCollectionView" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SenseNet.Portal.Portlets" %>
<%@ Import Namespace="SenseNet.Portal.Helpers" %>
<%@ Import Namespace="SenseNet.Portal.Virtualization" %>
<%@ Import Namespace="SenseNet.ContentRepository" %>
<%@ Import Namespace="SenseNet.ContentRepository.Storage" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="CFCU.Code" %>
<%@ Import Namespace="Newtonsoft.Json" %>


<%--<div class="sn-search-result-count">
    <span><%=GetGlobalResourceObject("SearchPortlet", "ResultCount")%>: </span><strong><%=Model.Pager.TotalCount%></strong>
</div>--%>

<div style="display:none">
    <sn:ActionMenu ID="ActionMenu1" runat="server" Text="hello" NodePath="/root" Scenario="ListItem"></sn:ActionMenu>
</div>
<% 
    List<string> allowedTypes = new List<string>();
    List<string> programs = new List<string>();

    foreach (var key in HttpContext.Current.Request.Form.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault();
        string keyValue = "";

        if (keyString == "docType")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            allowedTypes = keyValue.Split(',').ToList();
        }

        if (keyString == "Program")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            programs = JsonConvert.DeserializeObject<List<string>>(keyValue);
        }


        keyValue = HttpContext.Current.Request.Form[key];
    }

    //var typegroups2 = this.Model.Items.OrderBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType")));

    List<string> proctypes = new List<string>();
    proctypes.Add("Services");
    proctypes.Add("Supplies");
    proctypes.Add("Works");
    proctypes.Add("Grants");
    proctypes.Add("Twinnings");
    proctypes.Add("Framework");
                  
    //List<Tuple<string, string, int, int, int, int>> dataitems = new List<Tuple<string, string, int, int, int, int>>();

    var output = "";
    output += "<table id='grid6a' class='display'>";
    output += "<thead>";
    output += "<tr>";
  output += "<th data-field='proctype'>Contract Type</th>";
    output += "<th data-field='project'>Project</th>";
    output += "<th data-field='docmt'>Document</th>";
    output += "<th data-field='dtype'>Document Type</th>";
    output += "<th data-field='docversion'>Version</th>";
    output += "<th data-field='docdecision'>Decision</th>";
    output += "</tr>";
    output += "</thead>";
    output += "<tbody>";
    
    
    
        foreach (var content in this.Model.Items)
        {
            string docpath = content.Path;
            bool match = false;
            foreach (var program in programs)
            {
                if (docpath.Contains(program))
                {
                    match = true;
                    break;
                }
            }

            if (match == false)
                continue;
            
            string doctype = content.ContentHandler.GetProperty<string>("EADocumentType");

            if (allowedTypes.Contains(doctype, StringComparer.OrdinalIgnoreCase))
            {
                //dataitems.Add(new Tuple<string, string, int, int, int, int>(procuremnttype, doctype, submittedversion, countapproved, countsuspended, countrejected));

                var procuremnttype = content.ContentHandler.Parent.Parent.NodeType.Name.Replace("IPAProject", "");

                var project = content.ContentHandler.Parent.ParentName.Replace("_2f","/");

                int submittedversion = content.ContentHandler.GetProperty<int>("EASubmittedVersion");

                string decision = content.ContentHandler.GetProperty<string>("EADecision");

                var actionName = content.ContentHandler is User ? "Profile" : "Browse";
                
                output += "<tr>";

                output += "<td>" + procuremnttype + "</td>";
                output += "<td>" + project + "</td>";
                output += "<td><a target='_blank' href=" + HttpUtility.UrlPathEncode(Actions.ActionUrl(content, actionName)) + ">" + HttpUtility.HtmlEncode(content.DisplayName) + "</a></td>";
                output += "<td style='text-align:center'>" + doctype + "</td>";
                output += "<td style='text-align:center'>" + submittedversion + "</td>";
                output += "<td style='text-align:center'>" + decision + "</td>";

                //output += "<td style='text-align:center'>" + String.Format("{0:P2}", (double)row.Item4 / (double)(row.Item2 + row.Item3 + row.Item4)) + "</td>";
                output += "</tr>";
            }
        }

        output += "</tbody>";
        output += "</table>";

  
   // Response.Write(consolidatedtable.ConvertDataTableToHtmlTable(1) + "<br />");
        Response.Write(output);
        Response.Flush();
%>
<%--<div class="sn-article-list sn-article-list-shortdetail">
    <ul>
    <%//var typegroups = this.Model.Items.OrderBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType")));
        foreach (var content in this.Model.Items)
        {
            var actionName = content.ContentHandler is User ? "Profile" : "Browse";
          %>
      
        <div class="sn-search-result ui-helper-clearfix">
            <div style="float:left; padding:3px 10px 3px 10px;">
              <%= SenseNet.Portal.UI.IconHelper.RenderIconTag(content.Icon, null, 32) %>
            </div>            
            <div style="padding:3px 0 5px 0;">               
              <a href="<%=Actions.ActionUrl(content, actionName)%>"><%= HttpUtility.HtmlEncode(content.DisplayName) %></a>
              <br/>
              <%= content.ContentHandler.ParentId == 0 ? string.Empty : content.ContentHandler.Parent.GetProperty<string>("DocumentType")  + "<br />" + content.ContentHandler.GetProperty<string>("Decision")%>
            </div>
        </div>
        
    <%} %>
    </ul>
</div>--%>