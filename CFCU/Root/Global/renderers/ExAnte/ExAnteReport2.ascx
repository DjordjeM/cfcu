﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.Portlets.ContentCollectionView" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SenseNet.Portal.Portlets" %>
<%@ Import Namespace="SenseNet.Portal.Helpers" %>
<%@ Import Namespace="SenseNet.Portal.Virtualization" %>
<%@ Import Namespace="SenseNet.ContentRepository" %>
<%@ Import Namespace="SenseNet.ContentRepository.Storage" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="CFCU.Code" %>
<%@ Import Namespace="Newtonsoft.Json" %>


<script runat="server" type="text/C#">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!IsPostBack)
        {
            foreach (var key in HttpContext.Current.Request.Params.AllKeys)
            {
                string keyString = key.Split('$').LastOrDefault();

                var ctrl = this.Parent.FindControlRecursive(keyString);
                var value = HttpContext.Current.Request[key].ToString();
                
                if (ctrl == null)
                    continue;
                if (ctrl is TextBox)
                {
                    (ctrl as TextBox).Text = value;
                }
                else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
                {
                    (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value = value;
                }
                else
                {
                    continue;
                }
             
            }

        }
    }
public string GetNonSpecificParams()
{
    var paramlist = "";
    foreach (var key in HttpContext.Current.Request.Params.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault(); 
        
        var ctrl = this.Parent.FindControlRecursive(keyString);
        var value = string.Empty;
        if (ctrl == null)
            continue;
        if (ctrl is TextBox)
        {
            value = (ctrl as TextBox).Text;
        }
        else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
        {
            value = (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value;
        }
        else
        {
            continue;
        }
        var firstAction = Model.Pager.Actions.FirstOrDefault();
        if (firstAction == null)
        {
            continue;
        }
        if (!string.IsNullOrEmpty(value) && !firstAction.Url.Contains(keyString + "="))
        {
            paramlist += "&" + keyString + "=" + HttpUtility.UrlEncode(value);
        }
       
    }    
    return paramlist;
}
public int GetQuarter(DateTime dt)
     {
         return (dt.Month - 1) / 3 + 1;
     }
</script>

<%--<div class="sn-search-result-count">
    <span><%=GetGlobalResourceObject("SearchPortlet", "ResultCount")%>: </span><strong><%=Model.Pager.TotalCount%></strong>
</div>--%>

<div style="display:none">
    <sn:ActionMenu ID="ActionMenu1" runat="server" Text="hello" NodePath="/root" Scenario="ListItem"></sn:ActionMenu>
</div>
<% 
    var allowedTypes = new List<string>();
    var programs = new List<string>();
    //var rptformat = "";
    foreach (var key in HttpContext.Current.Request.Form.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault();
        string keyValue = "";

        if (keyString == "docType")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            allowedTypes = keyValue.Split(',').ToList();
        }

        if (keyString == "Program")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            programs = JsonConvert.DeserializeObject<List<string>>(keyValue);
        }


        //Response.Write("key: " + keyString + " value: " + keyValue.ToString() + " allowedtypes length: "+ allowedTypes.Length +"<br />");

        //foreach (var tp in allowedTypes)
        //{
        //    Response.Write(tp + ',');
        //}
        //string keyValue = HttpContext.Current.Request.Form["docType"];
        //Response.Write("keyValue: " + keyValue + "<br />");
    }
    
    %>
<%
    var output = "";

    output += "<table id='grid3'>";
    output += "<thead>";
    output += "<th rowspan=\"2\">Program</th>";
    output += "<th colspan='2' style='text-align:center'>Q1</th>";
    output += "<th colspan='2' style='text-align:center'>Q2</th>";
    output += "<th colspan='2' style='text-align:center'>Q3</th>";
    output += "<th colspan='2' style='text-align:center'>Q4</th>";
    output += "</tr>";
   // output += "";
    output += "<tr>";
    output += "<th>Sub</th>";
    output += "<th>Rej. rate</th>";
    output += "<th>Sub</th>";
    output += "<th>Rej. rate</th>";
    output += "<th>Sub</th>";
    output += "<th>Rej. rate</th>";
    output += "<th>Sub</th>";
    output += "<th>Rej. rate</th>";
    output += "</tr>";
    output += "</thead>";
    output += "<tbody>";

    //output = "<table id='grid2' border='1'>";

         %>
<%--<table id="grid2" border="1">
    <thead>
        <tr>
            <th rowspan="2">Program</th>
            <th colspan="2" style="text-align:center">Q1</th>
            <th colspan="2" style="text-align:center">Q2</th>
            <th colspan="2" style="text-align:center">Q3</th>
            <th colspan="2" style="text-align:center">Q4</th>
        </tr>
        <tr>
            <th>Sub</th>
            <th>Rej. rate</th>
            <th>Sub</th>
            <th>Rej. rate</th>
            <th>Sub</th>
            <th>Rej. rate</th>
            <th>Sub</th>
            <th>Rej. rate</th>
        </tr>
    </thead>
    <tbody>--%>
<%
   // var typegroups2 = this.Model.Items.Where(x=>x.Path.Contains("test"));//.OrderBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType")));

    List<Tuple<int, string, string, int, int, int>> dataitems = new List<Tuple<int, string, string, int, int, int>>();

    //rework, query db for content names
    var programs2 = this.Model.Items.AsEnumerable().Select(c => c.ContentHandler.Parent.Parent.Parent.Parent.DisplayName).Distinct().ToList();
    foreach (var program in programs2)
    {

        for (int qtrs = 1; qtrs <= 4; qtrs++) //placeholders for quarters
        {
            dataitems.Add(new Tuple<int, string, string, int, int, int>(qtrs, program, "Approved", 0, 0, 0));
        }
    }
    //Response.Write("item count: " + content2.Count());


    foreach (var content in this.Model.Items)
    {
        //Response.Write("path: " + content.Path + "<br />");
        string docpath = content.Path;
        bool match = false;
        foreach (var program in programs)
        {
            if (docpath.Contains(program))
            {
                match = true;
                break;
            }
        }

        if (match == false)
            continue;
        
        //count++;
        //if (content.ContentType.Name == "Verso")
        //{
        int countapproved = 0;
        int countsuspended = 0;
        int countrejected = 0;
        //var procuremnttype = content.ContentHandler.Parent.Parent.Parent.GetProperty<string>("ProcurementType");
        string prog =  content.ContentHandler.Parent.Parent.Parent.Parent.GetProperty<string>("DisplayName");
        
        //Response.Write(procuremnttype + "<br />");
        //string doctype = content.ContentHandler.Parent.GetProperty<string>("DocumentType");

        DateTime decisiondt = content.ContentHandler.GetProperty<DateTime>("EADecisionDate");

        string decision = content.ContentHandler.GetProperty<string>("EADecision");
        switch (content.ContentHandler.GetProperty<string>("EADecision"))
        {
            case "Approved":
                countapproved++;
                break;

            case "Suspended":
                countsuspended++;
                break;

            case "Rejected":
                countrejected++;
                break;
        }

        //if (allowedTypes.Contains(doctype, StringComparer.OrdinalIgnoreCase))
        //{
        dataitems.Add(new Tuple<int, string, string, int, int, int>(GetQuarter(decisiondt), prog, decision, countapproved, countsuspended, countrejected));
        //}
        
    }
    
    var final = dataitems.
         GroupBy(t => new {t.Item1,t.Item2})
         //.Where(g => g.Count()>0)//o => allowedStatus.Contains(o.StatusCode)
              .Select(g => 
                  new {
                        Quarter = g.Key.Item1,
                        Program = g.Key.Item2, 
                        Total = g.Sum(tuple => tuple.Item4) + g.Sum(tuple => tuple.Item5) + g.Sum(tuple => tuple.Item6),
                        Rejected =  g.Sum(tuple => tuple.Item6)// / (g.Sum(tuple => tuple.Item4) + g.Sum(tuple => tuple.Item5) + g.Sum(tuple => tuple.Item6))
                      })
               .OrderBy(x => x.Quarter)
               .ThenBy(x => x.Program
                      );

    var table = final.ToPivotTable(
        item => item.Quarter,
        item => item.Program,
        
        item => item.Any() ? item.First().Rejected : 0,
        item => item.Any() ? item.First().Total : 0
            
        );
    //DataTable consolidatedtable = new DataTable();

    Response.Write(table.ConvertDataTableToHtmlTable("", "grid4"));
    
%>

<%--<div class="sn-article-list sn-article-list-shortdetail">
    <ul>
    <%//var typegroups = this.Model.Items.OrderBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType")));
        foreach (var content in this.Model.Items)
        {
            var actionName = content.ContentHandler is User ? "Profile" : "Browse";
          %>
      
        <div class="sn-search-result ui-helper-clearfix">
            <div style="float:left; padding:3px 10px 3px 10px;">
              <%= SenseNet.Portal.UI.IconHelper.RenderIconTag(content.Icon, null, 32) %>
            </div>            
            <div style="padding:3px 0 5px 0;">               
              <a href="<%=Actions.ActionUrl(content, actionName)%>"><%= HttpUtility.HtmlEncode(content.DisplayName) %></a>
              <br/>
              <%= content.ContentHandler.ParentId == 0 ? string.Empty : content.ContentHandler.Parent.GetProperty<string>("DocumentType")  + "<br />" + content.ContentHandler.GetProperty<string>("Decision")%>
            </div>
        </div>
        
    <%} %>
    </ul>
</div>--%>



