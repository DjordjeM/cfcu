﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.Portlets.ContentCollectionView" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SenseNet.Portal.Portlets" %>
<%@ Import Namespace="SenseNet.Portal.Helpers" %>
<%@ Import Namespace="SenseNet.Portal.Virtualization" %>
<%@ Import Namespace="SenseNet.ContentRepository" %>
<%@ Import Namespace="SenseNet.ContentRepository.Storage" %>

<script runat="server" type="text/C#">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!IsPostBack)
        {
            foreach (var key in HttpContext.Current.Request.Params.AllKeys)
            {
                string keyString = key.Split('$').LastOrDefault();

                var ctrl = this.Parent.FindControlRecursive(keyString);
                var value = HttpContext.Current.Request[key].ToString();
                
                if (ctrl == null)
                    continue;
                if (ctrl is TextBox)
                {
                    (ctrl as TextBox).Text = value;
                }
                else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
                {
                    (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value = value;
                }
                else
                {
                    continue;
                }
             
            }

        }
    }
public string GetNonSpecificParams()
{
    var paramlist = "";
    foreach (var key in HttpContext.Current.Request.Params.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault(); 
        
        var ctrl = this.Parent.FindControlRecursive(keyString);
        var value = string.Empty;
        if (ctrl == null)
            continue;
        if (ctrl is TextBox)
        {
            value = (ctrl as TextBox).Text;
        }
        else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
        {
            value = (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value;
        }
        else
        {
            continue;
        }
        var firstAction = Model.Pager.Actions.FirstOrDefault();
        if (firstAction == null)
        {
            continue;
        }
        if (!string.IsNullOrEmpty(value) && !firstAction.Url.Contains(keyString + "="))
        {
            paramlist += "&" + keyString + "=" + HttpUtility.UrlEncode(value);
        }
       
    }    
    return paramlist;
}
public int GetQuarter(DateTime dt)
     {
         return (dt.Month - 1) / 3 + 1;
     }
</script>

<div class="sn-search-result-count">
    <span><%=GetGlobalResourceObject("SearchPortlet", "ResultCount")%>: </span><strong><%=Model.Pager.TotalCount%></strong>
</div>

<div style="display:none">
    <sn:ActionMenu ID="ActionMenu1" runat="server" Text="hello" NodePath="/root" Scenario="ListItem"></sn:ActionMenu>
</div>
<% 
    List<string> allowedTypes = new List<string>();
    //var rptformat = "";
    
    foreach (var key in HttpContext.Current.Request.Form.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault();
        string keyValue = "";

        if (keyString == "docType")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            allowedTypes = keyValue.Split(',').ToList();
        }
        //if (keyString == "rptFormat")
        //{
        //    keyValue = HttpContext.Current.Request.Form[key];
        //    rptformat = keyValue;
        //}
        keyValue = HttpContext.Current.Request.Form[key];


        //Response.Write("key: " + keyString + " value: " + keyValue.ToString() + " allowedtypes length: "+ allowedTypes.Length +"<br />");

        //foreach (var tp in allowedTypes)
        //{
        //    Response.Write(tp + ',');
        //}
        //string keyValue = HttpContext.Current.Request.Form["docType"];
        //Response.Write("keyValue: " + keyValue + "<br />");
    }
    //foreach (string key in HttpContext.Current.Request.Form.AllKeys)
    //{

    //    string keyValue = HttpContext.Current.Request.Form[key];
    //    Response.Write("keyValue: " + keyValue + "<br />");
    //}
    // Response.Write("doctype value: " + Request.Form["docType"]);
    %>

<%--<table id="grid" border="1">
    <thead>
        <tr>
            <th rowspan="2">Document Type</th>
            <th colspan="2" style="text-align:center">Version 1</th>
            <th colspan="2" style="text-align:center">Version 2</th>
            <th colspan="2" style="text-align:center">Version 3</th>
            <th colspan="2" style="text-align:center">Version 4</th>
        </tr>
        <tr>
            <th>Approved</th>
            <th>Rejected</th>
            <th>Approved</th>
            <th>Rejected</th>
            <th>Approved</th>
            <th>Rejected</th>
            <th>Approved</th>
            <th>Rejected</th>
        </tr>
    </thead>
    <tbody>--%>
<%
    var output = "";

    output += "<table id='grid3'>";
    output += "<thead>";
    output += "<tr>";
    output += "<th rowspan='2'>Document Type</th>";
    output += "<th colspan='2' style='text-align:center'>Version 1</th>";
    output += "<th colspan='2' style='text-align:center'>Version 2</th>";
    output += "<th colspan='2' style='text-align:center'>Version 3</th>";
    output += "<th colspan='2' style='text-align:center'>Version 4</th>";
    output += "</tr>";
    output += "<tr>";
    for (int i = 1; i <= 4; i++)
    {
        output += "<th>Approved</th><th>Rejected</th>"; 
    }
        //output += "<th>Approved</th><th>Rejected</th><th>Approved</th><th>Rejected</th><th>Approved</th><th>Rejected</th><th>Approved</th><th>Rejected</th>";
    output += "</tr>";
    output += "</thead>";
    output += "<tbody>";
    //var typegroups2 = this.Model.Items.OrderBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType")));

    List<Tuple<string, int, string, int, int, int>> dataitems = new List<Tuple<string, int, string, int, int, int>>();
    //List<int,int,string,int,int,int> dataitems2 = new 

    //for (int qtrs = 1; qtrs <= 4; qtrs++) //placeholders for quarters
    //{
    //    dataitems.Add(new Tuple<int, string, string, int, int, int>(qtrs, "", "", 0, 0, 0));
    //}
    foreach (var content in this.Model.Items)
    {
        //count++;
        //if (content.ContentType.Name == "Verso")
        //{
        int countapproved = 0;
        int countapprovedcomments = 0;
        int countrejected = 0;
        int version = content.ContentHandler.GetProperty<int>("DocumentVersion");
        string doctype = content.ContentHandler.GetProperty<string>("DocumentType");

        //DateTime decisiondt = content.ContentHandler.GetProperty<DateTime>("DecisionDate");

        string decision = content.ContentHandler.GetProperty<string>("Recommendation");

        switch (content.ContentHandler.GetProperty<string>("Recommendation"))
        {
            case "Approved":
                countapproved++;
                break;

            case "Approved with comments":
                countapprovedcomments++;
                break;

            case "Rejected":
                countrejected++;
                break;
        }

        if (allowedTypes.Contains(doctype, StringComparer.OrdinalIgnoreCase))
        {
        dataitems.Add(new Tuple<string, int, string, int, int, int>(Convert.ToString(version), allowedTypes.IndexOf(doctype)+1, doctype, countapproved, countapprovedcomments, countrejected));
        }
    }


    var final = dataitems.
         GroupBy(t=> new { t.Item2, t.Item1,t.Item3 })
         .Where(g => g.Count()>0)//o => allowedStatus.Contains(o.StatusCode)
              .Select(g => Tuple.Create(g.Key.Item1,g.Key.Item2, g.Key.Item3, g.Sum(tuple => tuple.Item4) + g.Sum(tuple => tuple.Item5), g.Sum(tuple => tuple.Item6)))
              .OrderBy(t=>t.Item2)
              .ThenBy(t=>t.Item1);

    //var final2 = dataitems.
    //     GroupBy(t => new { t.Item2, t.Item1, t.Item3 })
    //     .Where(g => g.Count() > 0)//o => allowedStatus.Contains(o.StatusCode)
    //          .Select(g => new { 
    //                                g.Key.Item1, g.Key.Item2, g.Key.Item3, 
    //                                approved = g.Sum(x => x.Item4) + g.Sum(x => x.Item5), 
    //                                rejected = g.Sum(x => x.Item6) 
    //                            })
    //          .OrderBy(t => t.Item2)
    //          .ThenBy(t => t.Item1)
    //          .ToList();

    var final3 = dataitems.
         GroupBy(t => new { t.Item3, t.Item2, t.Item1 })
               .Select((g) =>
                 new
                 {
                     DocType = g.Key.Item3,
                     DocOrder = g.Key.Item2,
                     DocVersion = g.Key.Item1,

                     Approved  = g.Sum(x => x.Item4) + g.Sum(x => x.Item5), 
                     Rejected = g.Sum(x => x.Item6) 
                 })
                 .OrderBy(g =>g.DocOrder);

    var final4 = final3.
        GroupBy(z => z.DocType)
        .Select(c =>
            new
            {
                DocuType = c.Key,
                rest = c
            });
            
    var sums = final.
        GroupBy(t=>t.Item1)
        .Select(g => Tuple.Create(g.Key, g.Sum(tuple => tuple.Item4),g.Sum(tuple=>tuple.Item5)));
    
    var sums2 = final3.
          GroupBy(t=>t.DocVersion)
          .Select(g=>Tuple.Create(g.Key, g.Sum(x => x.Approved), g.Sum(x => x.Rejected)));   
    
   // int grandtotal = sums.Sum(t => t.Item2) + sums.Sum(t => t.Item3);
    
    

    var output2 = output;
    
    
    foreach (var fin in final4)
    {
        //Response.Write(String.Format("groups.key: {0}", fin.DocuType + "<br />"));
        output2 += String.Format("<tr><td>{0}</td>",fin.DocuType);
        int count = 4;
        foreach (var bla in fin.rest)
        {
            //Response.Write(String.Format("Approved: {0} -- Rejected{1}", bla.Approved, bla.Rejected + "<br />"));
            output2 += String.Format("<td style='text-align:center'>{0}</td><td style='text-align:center'>{1}</td>", bla.Approved, bla.Rejected);
            count--;
        }
        if (count !=0)
        {
            for (int i = 1; i <= count; i++)
            {
                output2 += "<td style='text-align:center'>-</td><td style='text-align:center'>-</td>";
            }
            output2 += "</tr>";
        }
    }
%>

    <%  var lastdtype = "";
        var currentdtype = "";
        var lastcellindex = 0;
        var currentcellindex = 1;
        int maxcellindex = 1;
        foreach (var row in final)
        {
            currentdtype = row.Item3;

            if (currentdtype != lastdtype) //new document type
            {
                
                if (lastcellindex >= 1)
                {
                    for (int n = lastcellindex; n < 4; n++)
                    {
                        output += "<td style='text-align:center'>-</td>";
                        output += "<td style='text-align:center'>-</td>";
                        %>
                            <%--<td style="text-align:center">-</td>
                            <td style="text-align:center">-</td>--%>
                           
                    <%
                        }
                    if (lastcellindex > maxcellindex)
                    {
                        maxcellindex = lastcellindex; 
                    }
                       
                    lastcellindex = 0;
                    currentcellindex = 0;
                    output += "</tr>";
                    %>
                        <%--</tr>--%>
                    <%
                 }
                    %>
                     <%--<tr>
                        <td><%= row.Item3 %></td>
            
                        <td style="text-align:center"><%=row.Item4 %></td>
                        <td style="text-align:center"><%=row.Item5 %></td>--%>
        <%
                output += "<tr>";
                output += "<td>" + row.Item3 + "</td>";
                output += "<td style='text-align:center'>" + row.Item4 + "</td>";
                output += "<td style='text-align:center'>" + row.Item5 + "</td>";

                lastdtype = currentdtype;
                //currentcellindex++;
                lastcellindex = currentcellindex;
                continue;
            }
    %>
                        <%--<td style="text-align:center"><%=row.Item4 %></td>
                        <td style="text-align:center"><%=row.Item5 %></td>--%>
    <%
            output += "<td style='text-align:center'>" + row.Item4 + "</td>";
            output += "<td style='text-align:center'>" + row.Item5 + "</td>";

            currentcellindex++;
            lastdtype = currentdtype;

            lastcellindex = Convert.ToInt16(row.Item1);

            if (lastcellindex > maxcellindex)
            {
                maxcellindex = lastcellindex;
            }
        }
        
        for (int i = lastcellindex; i<4; i++)
        {
            output += "<td style='text-align:center'>-</td><td style='text-align:center'>-</td>";
      %>
                <%--<td style="text-align:center">-</td><td style="text-align:center">-</td>--%>
      <%
        }
      %>
        <%--</tr>
        <tr>
            <td>Sum</td>--%>
    <%
        output += "</tr><tr><td>Sum</td>";
        output2 += "<tr><td>Sum</td>";
        
        foreach (var sum2 in sums2)
        {
            output2 += String.Format("<td style='text-align:center'>{0}</td>",sum2.Item2);
            output2 += String.Format("<td style='text-align:center'>{0}</td>",sum2.Item3);
        }
        
        foreach (var sum in sums)
        {
            //Response.Write("version: " + sum.Item1 + " approved: " + sum.Item2 + " rejected: " + sum.Item3 + "<br />");
            output += "<td style='text-align:center'>" + sum.Item2 + "</td>";
            output += "<td style='text-align:center'>" + sum.Item3 + "</td>";

         %>
            <%--<td style="text-align:center"><%=sum.Item2%></td>
            <td style="text-align:center"><%=sum.Item3%></td>--%>
        
     <% }
         for (int i = maxcellindex; i < 4; i++)
         {
             output += "<td style='text-align:center'>-</td><td style='text-align:center'>-</td>";
         }
         output2 += "</tr>";
        output += "</tr>";
        //output += "<tr>";
        // output += "<td>Overall";
        // output += "</td>";
        // output += "<td colspan='8' style='text-align:center'>" + grandtotal + "</td>";
        // output += "</tr>";
         output += "</tbody></table>";
         output2 += "</tbody></table>";

    //     if (rptformat == "Excel")
    //{
    //    HttpContext.Current.Response.Clear();

    //    Response.ContentType = "application/msexcel";
    //    Response.AddHeader("content-disposition", "attachment; filename=Print.xls");
    //    Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
    //    Response.Write("<head>");
    //    Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
    //    Response.Write("<!--[if gte mso 9]><xml>");
    //    Response.Write("<x:ExcelWorkbook>");
    //    Response.Write("<x:ExcelWorksheets>");
    //    Response.Write("<x:ExcelWorksheet>");
    //    Response.Write("<x:Name>Report Data</x:Name>");
    //    Response.Write("<x:WorksheetOptions>");
    //    Response.Write("<x:Print>");
    //    Response.Write("<x:ValidPrinterInfo/>");
    //    Response.Write("</x:Print>");
    //    Response.Write("</x:WorksheetOptions>");
    //    Response.Write("</x:ExcelWorksheet>");
    //    Response.Write("</x:ExcelWorksheets>");
    //    Response.Write("</x:ExcelWorkbook>");
    //    Response.Write("</xml>");
    //    Response.Write("<![endif]--> ");
    //    Response.Write(output); // give ur html string here
    //    Response.Write("</head>");
    //    Response.Flush();

    //    //HttpContext.Current.Response.Clear();
    //    Response.End();
    //}
    //else
    //{
    //    Response.Write(output);
    //}

         Response.Write(output2 + "<br />");
            //Response.Write(output);
         %>  
        <%--</tr>
        <tr>
            <td>Overall</td>
            <td colspan="8" style="text-align:center"><%=grandtotal %></td>
        </tr>
</tbody>

</table>--%>
<%--<div class="sn-article-list sn-article-list-shortdetail">
    <ul>
    <%//var typegroups = this.Model.Items.OrderBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType")));
        foreach (var content in this.Model.Items)
        {
            var actionName = content.ContentHandler is User ? "Profile" : "Browse";
          %>
      
        <div class="sn-search-result ui-helper-clearfix">
            <div style="float:left; padding:3px 10px 3px 10px;">
              <%= SenseNet.Portal.UI.IconHelper.RenderIconTag(content.Icon, null, 32) %>
            </div>            
            <div style="padding:3px 0 5px 0;">               
              <a href="<%=Actions.ActionUrl(content, actionName)%>"><%= HttpUtility.HtmlEncode(content.DisplayName) %></a>
              <br/>
              <%= content.ContentHandler.ParentId == 0 ? string.Empty : content.ContentHandler.Parent.GetProperty<string>("DocumentType")  + "<br />" + content.ContentHandler.GetProperty<string>("Decision")%>
            </div>
        </div>
        
    <%} %>
    </ul>
</div>--%>


