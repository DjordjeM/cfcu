﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.Portlets.ContentCollectionView" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SenseNet.Portal.Portlets" %>
<%@ Import Namespace="SenseNet.Portal.Helpers" %>
<%@ Import Namespace="SenseNet.Portal.Virtualization" %>
<%@ Import Namespace="SenseNet.ContentRepository" %>
<%@ Import Namespace="SenseNet.ContentRepository.Storage" %>

<script runat="server" type="text/C#">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!IsPostBack)
        {
            foreach (var key in HttpContext.Current.Request.Params.AllKeys)
            {
                string keyString = key.Split('$').LastOrDefault();

                var ctrl = this.Parent.FindControlRecursive(keyString);
                var value = HttpContext.Current.Request[key].ToString();
                
                if (ctrl == null)
                    continue;
                if (ctrl is TextBox)
                {
                    (ctrl as TextBox).Text = value;
                }
                else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
                {
                    (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value = value;
                }
                else
                {
                    continue;
                }
             
            }

        }
    }
public string GetNonSpecificParams()
{
    var paramlist = "";
    foreach (var key in HttpContext.Current.Request.Params.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault(); 
        
        var ctrl = this.Parent.FindControlRecursive(keyString);
        var value = string.Empty;
        if (ctrl == null)
            continue;
        if (ctrl is TextBox)
        {
            value = (ctrl as TextBox).Text;
        }
        else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
        {
            value = (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value;
        }
        else
        {
            continue;
        }
        var firstAction = Model.Pager.Actions.FirstOrDefault();
        if (firstAction == null)
        {
            continue;
        }
        if (!string.IsNullOrEmpty(value) && !firstAction.Url.Contains(keyString + "="))
        {
            paramlist += "&" + keyString + "=" + HttpUtility.UrlEncode(value);
        }
       
    }    
    return paramlist;
}
public int GetQuarter(DateTime dt)
     {
         return (dt.Month - 1) / 3 + 1;
     }
</script>

<%--<div class="sn-search-result-count">
    <span><%=GetGlobalResourceObject("SearchPortlet", "ResultCount")%>: </span><strong><%=Model.Pager.TotalCount%></strong>
</div>--%>

<div style="display:none">
    <sn:ActionMenu ID="ActionMenu1" runat="server" Text="hello" NodePath="/root" Scenario="ListItem"></sn:ActionMenu>
</div>
<% 
    var allowedTypes = new string[6];
    //var rptformat = "";
    foreach (var key in HttpContext.Current.Request.Form.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault();
        string keyValue = "";

        if (keyString == "docType")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            allowedTypes = keyValue.Split(',').ToArray();
        }

        //if (keyString == "rptFormat")
        //{
        //    keyValue = HttpContext.Current.Request.Form[key];
        //    rptformat = keyValue;
        //}
        //keyValue = HttpContext.Current.Request.Form[key];


        //Response.Write("key: " + keyString + " value: " + keyValue.ToString() + " allowedtypes length: "+ allowedTypes.Length +"<br />");

        //foreach (var tp in allowedTypes)
        //{
        //    Response.Write(tp + ',');
        //}
        //string keyValue = HttpContext.Current.Request.Form["docType"];
        //Response.Write("keyValue: " + keyValue + "<br />");
    }
    //foreach (string key in HttpContext.Current.Request.Form.AllKeys)
    //{

    //    string keyValue = HttpContext.Current.Request.Form[key];
    //    Response.Write("keyValue: " + keyValue + "<br />");
    //}
    // Response.Write("doctype value: " + Request.Form["docType"]);
    %>
<%
    var output = "";

    output += "<table id='grid3'>";
    output += "<thead>";
    output += "<th rowspan=\"2\">Program</th>";
    output += "<th colspan='2' style='text-align:center'>Q1</th>";
    output += "<th colspan='2' style='text-align:center'>Q2</th>";
    output += "<th colspan='2' style='text-align:center'>Q3</th>";
    output += "<th colspan='2' style='text-align:center'>Q4</th>";
    output += "</tr>";
   // output += "";
    output += "<tr>";
    output += "<th>Sub</th>";
    output += "<th>Rej. rate</th>";
    output += "<th>Sub</th>";
    output += "<th>Rej. rate</th>";
    output += "<th>Sub</th>";
    output += "<th>Rej. rate</th>";
    output += "<th>Sub</th>";
    output += "<th>Rej. rate</th>";
    output += "</tr>";
    output += "</thead>";
    output += "<tbody>";

    //output = "<table id='grid2' border='1'>";

         %>
<%--<table id="grid2" border="1">
    <thead>
        <tr>
            <th rowspan="2">Program</th>
            <th colspan="2" style="text-align:center">Q1</th>
            <th colspan="2" style="text-align:center">Q2</th>
            <th colspan="2" style="text-align:center">Q3</th>
            <th colspan="2" style="text-align:center">Q4</th>
        </tr>
        <tr>
            <th>Sub</th>
            <th>Rej. rate</th>
            <th>Sub</th>
            <th>Rej. rate</th>
            <th>Sub</th>
            <th>Rej. rate</th>
            <th>Sub</th>
            <th>Rej. rate</th>
        </tr>
    </thead>
    <tbody>--%>
<%
    //var typegroups2 = this.Model.Items.OrderBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType")));

    List<Tuple<int, string, int, int, int>> dataitems = new List<Tuple<int, string, int, int, int>>();

    for (int qtrs = 1; qtrs <= 4; qtrs++) //placeholders for quarters
    {
        dataitems.Add(new Tuple<int, string, int, int, int>(qtrs, "Approved", 0, 0, 0));
    }
    //Response.Write("item count: " + content2.Count());


    foreach (var content in this.Model.Items)
    {
        //count++;
        //if (content.ContentType.Name == "Verso")
        //{
        int countapproved = 0;
        int countsuspended = 0;
        int countrejected = 0;
        var procuremnttype = content.ContentHandler.Parent.Parent.Parent.GetProperty<string>("ProcurementType");

        //Response.Write(procuremnttype + "<br />");
        //string doctype = content.ContentHandler.Parent.GetProperty<string>("DocumentType");

        DateTime decisiondt = content.ContentHandler.GetProperty<DateTime>("DecisionDate");

        string decision = content.ContentHandler.GetProperty<string>("Decision");
        switch (content.ContentHandler.GetProperty<string>("Decision"))
        {
            case "Approved":
                countapproved++;
                break;

            case "Suspended":
                countsuspended++;
                break;

            case "Rejected":
                countrejected++;
                break;
        }

        //if (allowedTypes.Contains(doctype, StringComparer.OrdinalIgnoreCase))
        //{
        dataitems.Add(new Tuple<int, string, int, int, int>(GetQuarter(decisiondt), decision, countapproved, countsuspended, countrejected));
        //}


    }

    // var allowedTypes = new[]{"Corrigendum"};
    var final = dataitems.
         GroupBy(t => t.Item1)
         .Where(g => g.Count()>0)//o => allowedStatus.Contains(o.StatusCode)
              .Select(g => Tuple.Create(g.Key, g.Sum(tuple => tuple.Item3), g.Sum(tuple => tuple.Item4), g.Sum(tuple => tuple.Item5)));
    //Response.Write("final.count: " + final.Count());
    //int totalapproved = 0;
    //int totalsuspended = 0;
    //int totalrejected = 0 ;
    int totalsubmitted = 0;
    //double percentrejected = 0;

    output += "<tr>";
    output += "<td>IPA 2013</td>";
%>

<%--<tr>
    <td>IPA 2013</td>--%>
    <% 
    foreach (var row in final)
    {

        totalsubmitted = (int)row.Item2 + (int)row.Item3 + (int)row.Item4;
        if (totalsubmitted != 0)
        {
            output += "<td class='dt-center''>" + totalsubmitted.ToString() + "</td>";
            output += "<td class='dt-center'>" + String.Format("{0:P2}", (double)row.Item4 / (double)totalsubmitted) + "</td>";
%>
            <%--<td style="text-align:center"><%=totalsubmitted.ToString() %></td>
            <td style="text-align:center"><%=String.Format("{0:P2}.", (double)row.Item4 / (double)totalsubmitted) %></td>--%>
            <%
                }
                else
                {
                    output += "<td class='dt-center''>0</td><td class='dt-center'>N/A</td>";
                    %>

            <%--<td style="text-align:center">0</td><td style="text-align:center">N/A</td>--%>
            <%--<%=row.Item2+row.Item3+row.Item4 %>--%>
            
            
            
                <% 
                            totalsubmitted = 0;
                            //}
                        }
                    }
                    output += "</tr></tbody></table>";

    //if (rptformat == "Excel")
    //{
    //    HttpContext.Current.Response.Clear();

    //    Response.ContentType = "application/msexcel";
    //    Response.AddHeader("content-disposition", "attachment; filename=Print.xls");
    //    Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
    //    Response.Write("<head>");
    //    Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
    //    Response.Write("<!--[if gte mso 9]><xml>");
    //    Response.Write("<x:ExcelWorkbook>");
    //    Response.Write("<x:ExcelWorksheets>");
    //    Response.Write("<x:ExcelWorksheet>");
    //    Response.Write("<x:Name>Report Data</x:Name>");
    //    Response.Write("<x:WorksheetOptions>");
    //    Response.Write("<x:Print>");
    //    Response.Write("<x:ValidPrinterInfo/>");
    //    Response.Write("</x:Print>");
    //    Response.Write("</x:WorksheetOptions>");
    //    Response.Write("</x:ExcelWorksheet>");
    //    Response.Write("</x:ExcelWorksheets>");
    //    Response.Write("</x:ExcelWorkbook>");
    //    Response.Write("</xml>");
    //    Response.Write("<![endif]--> ");
    //    Response.Write(output); // give ur html string here
    //    Response.Write("</head>");
    //    Response.Flush();

    //    //HttpContext.Current.Response.Clear();
    //    Response.End();
    //}
    //else
    //{
    //    Response.Write(output);
    //}
     Response.Write(output);
                    %> 
        <%--</tr>

</tbody>

</table>--%>
<%--<script type="text/javascript">
    ("#grid2").dataTable();
</script>--%>
<%--<div class="sn-article-list sn-article-list-shortdetail">
    <ul>
    <%//var typegroups = this.Model.Items.OrderBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType")));
        foreach (var content in this.Model.Items)
        {
            var actionName = content.ContentHandler is User ? "Profile" : "Browse";
          %>
      
        <div class="sn-search-result ui-helper-clearfix">
            <div style="float:left; padding:3px 10px 3px 10px;">
              <%= SenseNet.Portal.UI.IconHelper.RenderIconTag(content.Icon, null, 32) %>
            </div>            
            <div style="padding:3px 0 5px 0;">               
              <a href="<%=Actions.ActionUrl(content, actionName)%>"><%= HttpUtility.HtmlEncode(content.DisplayName) %></a>
              <br/>
              <%= content.ContentHandler.ParentId == 0 ? string.Empty : content.ContentHandler.Parent.GetProperty<string>("DocumentType")  + "<br />" + content.ContentHandler.GetProperty<string>("Decision")%>
            </div>
        </div>
        
    <%} %>
    </ul>
</div>--%>



