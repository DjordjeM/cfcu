﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="SNCR=SenseNet.ContentRepository" %>


<% //foreach (var content in this.Model.Items) %>
<asp:ListView ID="ContentList" runat="server" EnableViewState="false" GroupPlaceholderID="groupPlaceHolder1" ItemPlaceholderID="itemPlaceHolder">
    
    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
        <table class="sn-table">
            <thead>
                <tr>
                    <td>
                        View
                    </td>
                    <td>
                        Edit
                    </td>
                    <td>
                        Project Reference Number
                    </td>
                    <td>
                        Project Name
                    </td>
                    <td>
                        Document Type
                    </td>
                    <td>
                        Version
                    </td>
                    <td>
                        Created
                    </td>
                </tr>
            </thead>
            <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
        </table>       
    </LayoutTemplate>
    <GroupTemplate>
        <tr>
            <asp:PlaceHolder runat="server" ID="itemPlaceHolder"></asp:PlaceHolder>
        </tr>
    </GroupTemplate>
    <ItemTemplate>
        <tr>
        <td>
            <sn:ActionLinkButton NodePath='<%#Eval("Path") %>' 
                                 ActionName="Browse" 
                                 IconVisible="true" runat="server" />
        </td>
        <td>
            <sn:ActionLinkButton ID="ActionLinkButton1" NodePath='<%#Eval("Path") %>' 
                                 ActionName="Edit" 
                                 IconVisible="true" runat="server">
            </sn:ActionLinkButton>
        </td>
        <td>
            <%# ((Container.DataItem as SNCR.Content).ContentHandler.Parent.ParentName.Replace("_2f", "/")) + " (" + (Container.DataItem as SNCR.Content).ContentHandler.Parent.Parent.Parent.ParentName + " / " + (Container.DataItem as SNCR.Content).ContentHandler.Parent.Parent.ParentName.Split(' ').First() +")" %>
        </td>
        <td>
            <%# (Container.DataItem as SNCR.Content).ContentHandler.Parent.Parent.GetProperty<string>("Description") %>
        </td>
        <td>
            <%#Eval("DocumentType") %>
        </td>
        <td>
             <%#Eval("DocumentVersion") %>
        </td>
        <td>
            <%--<%=GetGlobalResourceObject("Renderers", "Created")%>--%><span class='<%# "sn-date-" + Eval("Id") + "-CreationDate" %>'>
        </td>
        </tr>
        <script>
            $(function () {
                SN.Util.setFullLocalDate('<%# "span.sn-date-" + Eval("Id") + "-CreationDate" %>', '<%= System.Globalization.CultureInfo.CurrentUICulture%>',
                    '<%#Eval("CreationDate") %>',
                    '<%= System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern %>',
                    '<%= System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortTimePattern %>');
            });
        </script>
    </ItemTemplate>
    <%--<LayoutTemplate>
        <div>
            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
        </div>       
    </LayoutTemplate>
    <ItemTemplate>
            <div>
            <sn:ActionLinkButton NodePath='<%#Eval("Path") %>' 
                                 ActionName="Browse" 
                                 IconVisible="true" runat="server"> 
            </sn:ActionLinkButton><span><%#Eval("DocumentType") %></span>
            <sn:ActionLinkButton ID="ActionLinkButton1" NodePath='<%#Eval("Path") %>' 
                                 ActionName="Edit" 
                                 IconVisible="true" runat="server">
            </sn:ActionLinkButton><span><%# ((Container.DataItem as SNCR.Content).ContentHandler.Parent.ParentName.Replace("_2f", "/")) + " (" + (Container.DataItem as SNCR.Content).ContentHandler.Parent.Parent.Parent.ParentName + " / " + (Container.DataItem as SNCR.Content).ContentHandler.Parent.Parent.ParentName.Split(' ').First() +")"%></span>
            <%=GetGlobalResourceObject("Renderers", "Created")%><span class='<%# "sn-date-" + Eval("Id") + "-CreationDate" %>'>
        </div>
        <script>
            $(function () {
                SN.Util.setFullLocalDate('<%# "span.sn-date-" + Eval("Id") + "-CreationDate" %>', '<%= System.Globalization.CultureInfo.CurrentUICulture%>',
                    '<%#Eval("CreationDate") %>',
                    '<%= System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern %>',
                    '<%= System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortTimePattern %>');
            });
        </script>
    </ItemTemplate>--%>
</asp:ListView>