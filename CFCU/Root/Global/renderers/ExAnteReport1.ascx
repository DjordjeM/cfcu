﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.Portlets.ContentCollectionView" CodeBehind="/Code/ExAnteReport1.ascx.cs"%>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SenseNet.Portal.Portlets" %>
<%@ Import Namespace="SenseNet.Portal.Helpers" %>
<%@ Import Namespace="SenseNet.Portal.Virtualization" %>
<%@ Import Namespace="SenseNet.ContentRepository" %>
<%@ Import Namespace="SenseNet.ContentRepository.Storage" %>
<%@ Import Namespace="OfficeOpenXml" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="HtmlAgilityPack" %>


<%--<sn:ScriptRequest ID="Scriptrequest2" runat="server" Path="$skin/scripts/jszip/jszip.min.js" />--%>

<script runat="server" type="text/C#">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!IsPostBack)
        {
            foreach (var key in HttpContext.Current.Request.Params.AllKeys)
            {
                string keyString = key.Split('$').LastOrDefault();

                var ctrl = this.Parent.FindControlRecursive(keyString);
                var value = HttpContext.Current.Request[key].ToString();
                
                if (ctrl == null)
                    continue;
                if (ctrl is TextBox)
                {
                    (ctrl as TextBox).Text = value;
                }
                else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
                {
                    (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value = value;
                }
                else
                {
                    continue;
                }
             
            }

        }
    }
public string GetNonSpecificParams()
{
    var paramlist = "";
    foreach (var key in HttpContext.Current.Request.Params.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault(); 
        
        var ctrl = this.Parent.FindControlRecursive(keyString);
        var value = string.Empty;
        if (ctrl == null)
            continue;
        if (ctrl is TextBox)
        {
            value = (ctrl as TextBox).Text;
        }
        else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
        {
            value = (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value;
        }
        else
        {
            continue;
        }
        var firstAction = Model.Pager.Actions.FirstOrDefault();
        if (firstAction == null)
        {
            continue;
        }
        if (!string.IsNullOrEmpty(value) && !firstAction.Url.Contains(keyString + "="))
        {
            paramlist += "&" + keyString + "=" + HttpUtility.UrlEncode(value);
        }
       
    }    
    return paramlist;
}

</script>
<%--<sn:ScriptRequest ID="Scriptrequest1" runat="server" Path="$skin/scripts/jszip/jszip.min.js" />
<div class="sn-search-result-count">
    <span><%=GetGlobalResourceObject("SearchPortlet", "ResultCount")%>: </span><strong><%=Model.Pager.TotalCount%></strong>
</div>--%>

<div style="display:none">
    <sn:ActionMenu ID="ActionMenu1" runat="server" Text="hello" NodePath="/root" Scenario="ListItem"></sn:ActionMenu>
</div>
<% 
    var allowedTypes = new string[6];
    var rptformat = "";
    foreach (var key in HttpContext.Current.Request.Form.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault();
        string keyValue = "";

        if (keyString == "docType")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            allowedTypes = keyValue.Split(',').ToArray();
        }
        keyValue = HttpContext.Current.Request.Form[key];


        //if (keyString == "rptFormat")
        //{
        //    keyValue = HttpContext.Current.Request.Form[key];
        //    rptformat = keyValue;
        //}


        //Response.Write("key: " + keyString + " value: " + keyValue.ToString() + " allowedtypes length: "+ allowedTypes.Length +"<br />");

        //foreach (var tp in allowedTypes)
        //{
        //    Response.Write(tp + ',');
        //}
        //string keyValue = HttpContext.Current.Request.Form["docType"];
        //Response.Write("keyValue: " + keyValue + "<br />");
    }
    //foreach (string key in HttpContext.Current.Request.Form.AllKeys)
    //{

    //    string keyValue = HttpContext.Current.Request.Form[key];
    //    Response.Write("keyValue: " + keyValue + "<br />");
    //}

    //Response.Write("doctype value: " + Request.Form["docType"]);
    %>
<%--<script src="http://cdn.kendostatic.com/2014.3.1029/js/jszip.min.js"></script>--%>
<% 
    var output = "";
    output += "<table id='grid3' class='display'>";
    output += "<thead>";
    output += "<tr>";
    output += "<th data-field='dtype'>Document Type</th>";
    output += "<th data-field='submitted'>Submitted</th>";
    output += "<th data-field='approved'>Approved</th>";
    output += "<th data-field='suspended'>Suspended</th>";
    output += "<th data-field='rejected'>Rejected</th>";
    output += "<th data-field='percentrejected'>Percent rejected</th>";
    output += "</tr>";
    output += "</thead>";
    //output += "</tbody>";
    %>

<%--<table id="grid" border="1">
    <thead>
        <tr>
            <th data-field="type">Document Type</th>
            <th data-field="submitted">Submitted</th>
            <th data-field="approved">Approved</th>
            <th data-field="suspended">Suspended</th>
            <th data-field="rejected">Rejected</th>
            <th data-field="percentrejected">% rejected</th>
        </tr>
    </thead>
    <tbody>--%>
<%
    //var typegroups2 = this.Model.Items.OrderBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType")));

    List<Tuple<string, string, int, int, int>> dataitems = new List<Tuple<string, string, int, int, int>>();


    //Response.Write("item count: " + content2.Count());

    foreach (var content in this.Model.Items)
    {
        //count++;
        //if (content.ContentType.Name == "Verso")
        //{
        int countapproved = 0;
        int countsuspended = 0;
        int countrejected = 0;
        //string program = content.ContentHandler
        var procuremnttype = content.ContentHandler.Parent.Parent.NodeType.Name.Replace("IPAProject", "");
        //Response.Write(procuremnttype + "<br />");
        string doctype = content.ContentHandler.GetProperty<string>("EADocumentType");

        string decision = content.ContentHandler.GetProperty<string>("EADecision");
        switch (content.ContentHandler.GetProperty<string>("EADecision"))
        {
            case "Approved":
                countapproved++;
                break;

            case "Suspended":
                countsuspended++;
                break;

            case "Rejected":
                countrejected++;
                break;
        }

        if (allowedTypes.Contains(doctype, StringComparer.OrdinalIgnoreCase))
        {
            dataitems.Add(new Tuple<string, string, int, int, int>(doctype, decision, countapproved, countsuspended, countrejected));
        }


    }

    // var allowedTypes = new[]{"Corrigendum"};
    var final = dataitems.
         GroupBy(t => t.Item1)
         .Where(g => g.Count()>0)//o => allowedStatus.Contains(o.StatusCode)
              .Select(g => Tuple.Create(g.Key, g.Sum(tuple => tuple.Item3), g.Sum(tuple => tuple.Item4), g.Sum(tuple => tuple.Item5)));

    int totalapproved = 0;
    int totalsuspended = 0;
    int totalrejected = 0 ;
    int totalsubmitted = 0;
    //double percentrejected = 0;
    foreach (var row in final)
    {
        totalapproved += row.Item2;
        totalsuspended += row.Item3;
        totalrejected += row.Item4;

        output += "<tr>";
        output += "<td>" + row.Item1 + "</td>";
        output += "<td style='text-align:center'>" + (row.Item2+row.Item3+row.Item4).ToString() + "</td>";
        output += "<td style='text-align:center'>" + row.Item2 + "</td>";
        output += "<td style='text-align:center'>" + row.Item3 + "</td>";
        output += "<td style='text-align:center'>" + row.Item4 + "</td>";

        output += "<td style='text-align:center'>" + String.Format("{0:P2}", (double)row.Item4 / (double)(row.Item2 + row.Item3 + row.Item4)) + "</td>";
        output += "</tr>";
%>
       <%-- <tr>
            <td><%= row.Item1%></td>
            <td style="text-align:center"><%=row.Item2+row.Item3+row.Item4 %></td>
            <td style="text-align:center"><%=row.Item2 %></td>
            <td style="text-align:center"><%=row.Item3 %></td>
            <td style="text-align:center"><%=row.Item4 %></td>
            
            <td style="text-align:center"><%=String.Format("{0:P2}.", (double)row.Item4/(double)(row.Item2 + row.Item3 + row.Item4)) %></td>
        </tr>--%>
<%}
    totalsubmitted = totalapproved + totalsuspended + totalrejected;
    double percentrejected = (double) totalrejected/ (double)totalsubmitted;

    output += "<tr>";
    output += "<td>Total</td>";
    output += "<td style='text-align:center'>" + totalsubmitted + "</td>";
    output += "<td style='text-align:center'>" + totalapproved + "</td>";
    output += "<td style='text-align:center'>" + totalsuspended + "</td>";
    output += "<td style='text-align:center'>" + totalrejected + "</td>";
    output += "<td style='text-align:center'>" + String.Format("{0:P2}", percentrejected) + "</td>";
    output += "</tr>";
    output += "</tbody>";
    output += "</table>";

    //if (rptformat == "Excel")
    //{
    //    var doc = new HtmlDocument();
    //        doc.LoadHtml(output);
    //        var nodes = doc.DocumentNode.SelectNodes("//tr");
    //        var tbl = new DataTable("MyTable");

    //        var headers = nodes[0]
    //            .Elements("th")
    //            .Select(th => th.InnerText.Trim());
    //        foreach (var header in headers)
    //        {
    //            tbl.Columns.Add(header);
    //        }

    //        var rows = nodes.Skip(1).Select(tr => tr
    //            .Elements("td")
    //            .Select(td => td.InnerText.Trim())
    //            .ToArray());
    //        foreach (var row in rows)
    //        {
    //            tbl.Rows.Add(row);
    //        }
    //        //ExportExcel(tbl);

            
    //    //Generate Excel file

    //        Response.Clear();


    //        using (ExcelPackage pck = new ExcelPackage())
    //        {

    //            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report1");

    //            ws.Cells["A1"].LoadFromDataTable(tbl, true);
    //            //pck.Save();

    //            pck.SaveAs(Response.OutputStream);
    //            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    //            Response.AddHeader("content-disposition", "attachment;  filename=Sample1.xlsx");

    //            //Response.Clear();
    //            //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    //            //Response.AddHeader("content-disposition", "attachment;  filename=Rpt1.xlsx");

    //            //Response.BinaryWrite(pck.GetAsByteArray());
    //            //HttpContext.Current.Response.EndFlush();

    //        }
            
    //    Response.End();
    //   // File.
    //    //HttpContext.Current.Response.Clear();

    //    //Response.ContentType = "application/msexcel";
    //    //Response.AddHeader("content-disposition", "attachment; filename=Print.xls");
    //    //Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
    //    //Response.Write("<head>");
    //    //Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
    //    //Response.Write("<!--[if gte mso 9]><xml>");
    //    //Response.Write("<x:ExcelWorkbook>");
    //    //Response.Write("<x:ExcelWorksheets>");
    //    //Response.Write("<x:ExcelWorksheet>");
    //    //Response.Write("<x:Name>Report Data</x:Name>");
    //    //Response.Write("<x:WorksheetOptions>");
    //    //Response.Write("<x:Print>");
    //    //Response.Write("<x:ValidPrinterInfo/>");
    //    //Response.Write("</x:Print>");
    //    //Response.Write("</x:WorksheetOptions>");
    //    //Response.Write("</x:ExcelWorksheet>");
    //    //Response.Write("</x:ExcelWorksheets>");
    //    //Response.Write("</x:ExcelWorkbook>");
    //    //Response.Write("</xml>");
    //    //Response.Write("<![endif]--> ");
    //    //Response.Write(output); // give ur html string here
    //    //Response.Write("</head>");
    //    //Response.Flush();

    //    ////HttpContext.Current.Response.Clear();
    //    //Response.End();
    //}
    //else
    //{
    //    Response.Write(output);
    //}

    Response.Write(output);
    Response.Flush();

    %>
        <%--<tr>
            <td>Total</td>
            <td style="text-align:center"><%=totalsubmitted %></td>
            <td style="text-align:center"><%=totalapproved %></td>
            <td style="text-align:center"><%=totalsuspended%></td>
            <td style="text-align:center"><%=totalrejected %></td>
            <td style="text-align:center"><%=String.Format("{0:P2}.", percentrejected)  %></td>
        </tr>
</tbody>

</table>--%>

<%--<script type="text/javascript">
    $("#grid2").kendoGrid({
        toolbar: ["Excel"],
        excel: {
            fileName: "Kendo UI Grid Export.xlsx"
        },
    });
</script>--%>

<%--<div class="sn-article-list sn-article-list-shortdetail">
    <ul>
    <%//var typegroups = this.Model.Items.OrderBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType")));
        foreach (var content in this.Model.Items)
        {
            var actionName = content.ContentHandler is User ? "Profile" : "Browse";
          %>
      
        <div class="sn-search-result ui-helper-clearfix">
            <div style="float:left; padding:3px 10px 3px 10px;">
              <%= SenseNet.Portal.UI.IconHelper.RenderIconTag(content.Icon, null, 32) %>
            </div>            
            <div style="padding:3px 0 5px 0;">               
              <a href="<%=Actions.ActionUrl(content, actionName)%>"><%= HttpUtility.HtmlEncode(content.DisplayName) %></a>
              <br/>
              <%= content.ContentHandler.ParentId == 0 ? string.Empty : content.ContentHandler.Parent.GetProperty<string>("DocumentType")  + "<br />" + content.ContentHandler.GetProperty<string>("Decision")%>
            </div>
        </div>
        
    <%} %>
    </ul>
</div>--%>



