﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.Portlets.ContentCollectionView" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SenseNet.Portal.Portlets" %>
<%@ Import Namespace="SenseNet.Portal.Helpers" %>
<%@ Import Namespace="SenseNet.Portal.Virtualization" %>
<%@ Import Namespace="SenseNet.ContentRepository" %>
<%@ Import Namespace="SenseNet.ContentRepository.Storage" %>

<%--<sn:ScriptRequest ID="Scriptrequest1" runat="server" Path="$skin/scripts/datatables/DataTables-1.10.10/jquery.dataTables.js" />--%>
<%--<sn:CssRequest ID="Cssrequest1" runat="server" CSSPath="$skin/styles/datatables/DataTables-1.10.10/jquery.dataTables.css" />--%>

<%--<script runat="server" type="text/C#">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!IsPostBack)
        {
            foreach (var key in HttpContext.Current.Request.Params.AllKeys)
            {
                string keyString = key.Split('$').LastOrDefault();

                var ctrl = this.Parent.FindControlRecursive(keyString);
                var value = HttpContext.Current.Request[key].ToString();
                
                if (ctrl == null)
                    continue;
                if (ctrl is TextBox)
                {
                    (ctrl as TextBox).Text = value;
                }
                else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
                {
                    (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value = value;
                }
                else
                {
                    continue;
                }
             
            }

        }
    }
public string GetNonSpecificParams()
{
    var paramlist = "";
    foreach (var key in HttpContext.Current.Request.Params.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault(); 
        
        var ctrl = this.Parent.FindControlRecursive(keyString);
        var value = string.Empty;
        if (ctrl == null)
            continue;
        if (ctrl is TextBox)
        {
            value = (ctrl as TextBox).Text;
        }
        else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
        {
            value = (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value;
        }
        else
        {
            continue;
        }
        var firstAction = Model.Pager.Actions.FirstOrDefault();
        if (firstAction == null)
        {
            continue;
        }
        if (!string.IsNullOrEmpty(value) && !firstAction.Url.Contains(keyString + "="))
        {
            paramlist += "&" + keyString + "=" + HttpUtility.UrlEncode(value);
        }
       
    }    
    return paramlist;
}

</script>--%>

<%--<div class="sn-search-result-count">
    <span><%=GetGlobalResourceObject("SearchPortlet", "ResultCount")%>: </span><strong><%=Model.Pager.TotalCount%></strong>
</div>--%>

<%--<div style="display:none">
    <sn:ActionMenu ID="ActionMenu1" runat="server" Text="hello" NodePath="/root" Scenario="ListItem"></sn:ActionMenu>
</div>--%>
<% 
    var allowedTypes = new string[6];
    //var rptformat = "";
    foreach (var key in HttpContext.Current.Request.Form.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault();
        string keyValue = "";

        if (keyString == "docType")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            allowedTypes = keyValue.Split(',').ToArray();
        }

        //if (keyString == "rptFormat")
        //{
        //    keyValue = HttpContext.Current.Request.Form[key];
        //    rptformat = keyValue;
        //}
        //keyValue = HttpContext.Current.Request.Form[key];

        //Response.Write("key: " + keyString + " value: " + keyValue.ToString() + " allowedtypes length: "+ allowedTypes.Length +"<br />");
        //foreach (var tp in allowedTypes)
        //{
        //    Response.Write(tp + ',');
        //}
        //string keyValue = HttpContext.Current.Request.Form["docType"];
        //Response.Write("keyValue: " + keyValue + "<br />");
    }
    //foreach (string key in HttpContext.Current.Request.Form.AllKeys)
    //{

    //    string keyValue = HttpContext.Current.Request.Form[key];
    //    Response.Write("keyValue: " + keyValue + "<br />");
    //}
    // Response.Write("doctype value: " + Request.Form["docType"]);
    %>

<%--<table id="grid" border="1">
    <thead>
        <tr>
            <th>Document Type</th>
            <th>Legality / regularity</th>
            <th>Compliance with program</th>
            <th>Sound financial management</th>
            <th>Quality of submitted documents</th>
        </tr>
    </thead>
    <tbody>--%>
<%
    var output = "";


    output += "<table id='grid3' class='display'>";
    output += "<thead><tr>";
    output += "<th>Document Type</th><th>Legality / regularity</th><th>Compliance with program</th><th>Sound financial management</th> <th>Quality of submitted documents</th>";
    output += "</tr></thead><tbody>";
    //var typegroups2 = this.Model.Items.OrderBy((c => c.ContentHandler.GetProperty<string>("RejectionReason"))).AsEnumerable().GroupBy((c => c.ContentHandler.GetProperty<string>("RejectionReason")));

    List<Tuple<string, string, int, int, int, int>> dataitems = new List<Tuple<string, string, int, int, int, int>>();


    //Response.Write("item count: " + content2.Count());


    foreach (var content in this.Model.Items)
    {
        //count++;
        //if (content.ContentType.Name == "Verso")
        //{
        if (content.ContentHandler.GetProperty<string>("Decision") != "Rejected")
        {
            //Response.Write(content.ContentHandler.GetProperty<string>("Decision") + "<br />");
            continue;
        }
        int countlegality = 0;
        int countcompliance = 0;
        int countmanagement = 0;
        int countquality = 0;

        var procuremnttype = content.ContentHandler.Parent.Parent.Parent.NodeType.Name.Replace("IPAProject","");//.GetProperty<string>("ContentType");//.GetStoredValue("ProcurementType");
        //procuremnttype = procuremnttype.Replace("IPAProject","");
        string doctype = content.ContentHandler.Parent.GetProperty<string>("DocumentType");
        //string reasons = content.ContentHandler.GetProperty<string>("RejectionReasons");

        string decision = content.ContentHandler.GetProperty<string>("Decision");
        string[] reasons = content.ContentHandler.GetProperty<string>("RejectionReasons").Split(';');
        foreach (string reason in reasons) {
            // Response.Write("reasons: " + reason + "<br />");

            switch (reason)
            {
                case "legality":
                    countlegality++;
                    break;

                case "compliance":
                    countcompliance++;
                    break;

                case "management":
                    countmanagement++;
                    break;

                case "quality":
                    countquality++;
                    break;
            }
        }

        if (allowedTypes.Contains(doctype, StringComparer.OrdinalIgnoreCase))
        {
            dataitems.Add(new Tuple<string, string, int, int, int, int>(doctype, decision, countlegality, countcompliance, countmanagement, countquality));
        }


    }

    // var allowedTypes = new[]{"Corrigendum"};
    var final = dataitems.
         GroupBy(t => t.Item1)
         .Where(g => g.Count()>0)//o => allowedStatus.Contains(o.StatusCode)
              .Select(g => Tuple.Create(g.Key, g.Sum(tuple => tuple.Item3), g.Sum(tuple => tuple.Item4), g.Sum(tuple => tuple.Item5), g.Sum(tuple => tuple.Item6)));

    //int totalapproved = 0;
    //int totalsuspended = 0;
    //int totalrejected = 0 ;
    //int totalsubmitted = 0;
    ////double percentrejected = 0;
    foreach (var row in final)
    {
        //totalapproved += row.Item2;
        //totalsuspended += row.Item3;
        //totalrejected += row.Item4;
        output += "<tr>";
        output += "<td>" + row.Item1 + "</td>";
        output += "<td style='text-align:center'>" + row.Item2 + "</td>";
        output += "<td style='text-align:center'>" + row.Item3 + "</td>";
        output += "<td style='text-align:center'>" + row.Item4 + "</td>";
        output += "<td style='text-align:center'>" + row.Item5 + "</td>";
        //output += "</tr>";

        output += "</tr>";
    }
    output += "</tbody></table>";   
     
    //if (rptformat == "Excel")
    //{
    //    HttpContext.Current.Response.Clear();

    //    Response.ContentType = "application/msexcel";
    //    Response.AddHeader("content-disposition", "attachment; filename=Print.xls");
    //    Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
    //    Response.Write("<head>");
    //    Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
    //    Response.Write("<!--[if gte mso 9]><xml>");
    //    Response.Write("<x:ExcelWorkbook>");
    //    Response.Write("<x:ExcelWorksheets>");
    //    Response.Write("<x:ExcelWorksheet>");
    //    Response.Write("<x:Name>Report Data</x:Name>");
    //    Response.Write("<x:WorksheetOptions>");
    //    Response.Write("<x:Print>");
    //    Response.Write("<x:ValidPrinterInfo/>");
    //    Response.Write("</x:Print>");
    //    Response.Write("</x:WorksheetOptions>");
    //    Response.Write("</x:ExcelWorksheet>");
    //    Response.Write("</x:ExcelWorksheets>");
    //    Response.Write("</x:ExcelWorkbook>");
    //    Response.Write("</xml>");
    //    Response.Write("<![endif]--> ");
    //    Response.Write(output); // give ur html string here
    //    Response.Write("</head>");
    //    Response.Flush();

    //    //HttpContext.Current.Response.Clear();
    //    //Response.End();
    //}
    //else
    //{
    //    //output = output;
    //        Response.Write(output);
    //}
        Response.Write(output);
%>
        <%--<tr>
            <td><%= row.Item1%></td>--%>
            <%--<td style="text-align:center"><%=row.Item2+row.Item3+row.Item4 %></td>--%>
            <%--<td style="text-align:center"><%=row.Item2 %></td>
            <td style="text-align:center"><%=row.Item3 %></td>
            <td style="text-align:center"><%=row.Item4 %></td>
            <td style="text-align:center"><%=row.Item5 %></td>--%>
            
            <%--<td style="text-align:center"><%=String.Format("{0:P2}.", (double)row.Item4/(double)(row.Item2 + row.Item3 + row.Item4)) %></td>--%>
        <%--</tr>--%>
<%//}
    //totalsubmitted = totalapproved + totalsuspended + totalrejected;
    //double percentrejected = (double) totalrejected/ (double)totalsubmitted;
    %>
        <%--<tr>
            <td>Total</td>
            
            <td style="text-align:center"><%=totalsubmitted %></td>
            <td style="text-align:center"><%=totalapproved %></td>
            <td style="text-align:center"><%=totalsuspended%></td>
            <td style="text-align:center"><%=totalrejected %></td>
            <td style="text-align:center"><%=String.Format("{0:P2}.", percentrejected)  %></td>
        </tr>--%>
<%--</tbody>

</table>--%>
<%--<script type="text/javascript">
    //$("#grid5").DataTable({
    //    dom: '<"top"Brt><"clear">',
    //    buttons: [
    //    {
    //        extend: 'copyHtml5',
    //        text: '<i class="fa fa-files-o"></i>',
    //        titleAttr: 'Copy'
    //    },
    //        {
    //            extend: 'excelHtml5',
    //            text: '<i class="fa fa-file-excel-o"></i>',
    //            titleAttr: 'Excel'
    //        },
    //        {
    //            extend: 'csvHtml5',
    //            text: '<i class="fa fa-file-text-o"></i>',
    //            titleAttr: 'CSV'
    //        },
    //        {
    //            extend:    'pdfHtml5',
    //            text:      '<i class="fa fa-file-pdf-o"></i>',
    //            titleAttr: 'PDF'
    //        },
    //        {
    //            extend: 'print',
    //            text: '<i class="fa fa-print"></i>',
    //            titleAttr: 'Print'
    //        },
    //    ],
    //    "paging": false,
    //    "ordering": false,
    //    "info": false,
    //    "searching": false,
    //    "rowReorder": true,
    //   // "scrollY": 200,
    //    "paging": false,
    //    //"dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>'
    //    }
    //    );
</script>--%>
<%--<div class="sn-article-list sn-article-list-shortdetail">
    <ul>
    <%//var typegroups = this.Model.Items.OrderBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType")));
        foreach (var content in this.Model.Items)
        {
            var actionName = content.ContentHandler is User ? "Profile" : "Browse";
          %>
      
        <div class="sn-search-result ui-helper-clearfix">
            <div style="float:left; padding:3px 10px 3px 10px;">
              <%= SenseNet.Portal.UI.IconHelper.RenderIconTag(content.Icon, null, 32) %>
            </div>            
            <div style="padding:3px 0 5px 0;">               
              <a href="<%=Actions.ActionUrl(content, actionName)%>"><%= HttpUtility.HtmlEncode(content.DisplayName) %></a>
              <br/>
              <%= content.ContentHandler.ParentId == 0 ? string.Empty : content.ContentHandler.Parent.GetProperty<string>("DocumentType")  + "<br />" + content.ContentHandler.GetProperty<string>("Decision")%>
            </div>
        </div>
        
    <%} %>
    </ul>
</div>--%>



