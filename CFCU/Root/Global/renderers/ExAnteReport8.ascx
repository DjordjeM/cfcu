﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.Portlets.ContentCollectionView" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SenseNet.Portal.Portlets" %>
<%@ Import Namespace="SenseNet.Portal.Helpers" %>
<%@ Import Namespace="SenseNet.Portal.Virtualization" %>
<%@ Import Namespace="SenseNet.ContentRepository" %>
<%@ Import Namespace="SenseNet.ContentRepository.Storage" %>

<script runat="server" type="text/C#">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!IsPostBack)
        {
            foreach (var key in HttpContext.Current.Request.Params.AllKeys)
            {
                string keyString = key.Split('$').LastOrDefault();

                var ctrl = this.Parent.FindControlRecursive(keyString);
                var value = HttpContext.Current.Request[key].ToString();
                
                if (ctrl == null)
                    continue;
                if (ctrl is TextBox)
                {
                    (ctrl as TextBox).Text = value;
                }
                else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
                {
                    (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value = value;
                }
                else
                {
                    continue;
                }
             
            }

        }
    }
public string GetNonSpecificParams()
{
    var paramlist = "";
    foreach (var key in HttpContext.Current.Request.Params.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault(); 
        
        var ctrl = this.Parent.FindControlRecursive(keyString);
        var value = string.Empty;
        if (ctrl == null)
            continue;
        if (ctrl is TextBox)
        {
            value = (ctrl as TextBox).Text;
        }
        else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
        {
            value = (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value;
        }
        else
        {
            continue;
        }
        var firstAction = Model.Pager.Actions.FirstOrDefault();
        if (firstAction == null)
        {
            continue;
        }
        if (!string.IsNullOrEmpty(value) && !firstAction.Url.Contains(keyString + "="))
        {
            paramlist += "&" + keyString + "=" + HttpUtility.UrlEncode(value);
        }
       
    }    
    return paramlist;
}

</script>

<%--<div class="sn-search-result-count">
    <span><%=GetGlobalResourceObject("SearchPortlet", "ResultCount")%>: </span><strong><%=Model.Pager.TotalCount%></strong>
</div>--%>

<div style="display:none">
    <sn:ActionMenu ID="ActionMenu1" runat="server" Text="hello" NodePath="/root" Scenario="ListItem"></sn:ActionMenu>
</div>
<% 
    var allowedTypes = new string[6];
    //var rptformat = "";
    foreach (var key in HttpContext.Current.Request.Form.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault();
        string keyValue = "";

        if (keyString == "docType")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            allowedTypes = keyValue.Split(',').ToArray();
        }
        //if (keyString == "rptFormat")
        //{
        //    keyValue = HttpContext.Current.Request.Form[key];
        //    rptformat = keyValue;
        //}
        //keyValue = HttpContext.Current.Request.Form[key];



        // Response.Write("key: " + keyString + " value: " + keyValue.ToString() + " allowedtypes length: "+ allowedTypes.Length +"<br />");

        //foreach (var tp in allowedTypes)
        //{
        //    Response.Write(tp + ',');
        //}
        //string keyValue = HttpContext.Current.Request.Form["docType"];
        //Response.Write("keyValue: " + keyValue + "<br />");
    }
    //foreach (string key in HttpContext.Current.Request.Form.AllKeys)
    //{

    //    string keyValue = HttpContext.Current.Request.Form[key];
    //    Response.Write("keyValue: " + keyValue + "<br />");
    //}

    //Response.Write("doctype value: " + Request.Form["docType"]);
    %>

<%--<table id="grid" border="1">
    <thead>
        <tr>
            <th>Type of contract</th>
            <th>Total number of decisions</th>
            
            <th>% Approved</th>
            <th>% Suspended</th>
            <th>% Rejected</th>
            <th>% Rejected on 1st submission</th>
            <th>% Rejected of further submissions</th>
        </tr>
    </thead>
    <tbody>--%>
<%
    var output = "";

    output += "<table id='grid3'>";
    output += "<thead><tr>";
    output += "<th>Type of contract</th>";
    output += "<th>Total number of decisions</th>";

    output += "<th>% Approved</th>";
    output += "<th>% Suspended</th>";
    output += "<th>% Rejected</th>";

    output += "<th style='text-align:center'>% Rejected <br />on 1st submission</th>";
    output += "<th style='text-align:center'>% Rejected <br />on further submissions</th>";

    output += "</thead><tbody>";
    //var typegroups2 = this.Model.Items.OrderBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType")));

    List<Tuple<string, int, int, int, int>> dataitems = new List<Tuple<string, int, int, int, int>>();

    foreach (var content in this.Model.Items)
    {
        //count++;
        //if (content.ContentType.Name == "Verso")
        //{
        int countapproved = 0;
        int countsuspended = 0;
        int countrejected = 0;
        var procuremnttype = content.ContentHandler.Parent.Parent.Parent.NodeType.Name.Replace("IPAProject", "");
       // Response.Write(procuremnttype + "<br />");
        string doctype = content.ContentHandler.Parent.GetProperty<string>("DocumentType");

        string procedurestep = content.ContentHandler.Parent.GetProperty<string>("DocumentNature").Substring(1,2);
        //Response.Write("procedure step: " + procedurestep);


        int submittedversion = content.ContentHandler.Parent.GetProperty<int>("SubmittedVersion");

        string decision = content.ContentHandler.GetProperty<string>("Decision");
        switch (content.ContentHandler.GetProperty<string>("Decision"))
        {
            case "Approved":
                countapproved++;
                break;

            case "Suspended":
                countsuspended++;
                break;

            case "Rejected":
                countrejected++;
                break;
        }

        if (allowedTypes.Contains(doctype, StringComparer.OrdinalIgnoreCase))
        {
            dataitems.Add(new Tuple<string, int, int, int, int>(procuremnttype, submittedversion, countapproved, countsuspended, countrejected));
        }


    }

    var final = dataitems.
         GroupBy(t => t.Item1)
         .Where(g => g.Count()>0)
              .Select(g => Tuple.Create(g.Key, g.Sum(tuple => tuple.Item3)+g.Sum(tuple => tuple.Item4)+g.Sum(tuple => tuple.Item5),(double)( (g.Sum(tuple => tuple.Item3))/(double)((g.Sum(tuple => tuple.Item3)+g.Sum(tuple => tuple.Item4)+g.Sum(tuple => tuple.Item5)))), (double) (g.Sum(tuple => tuple.Item4)/(double)((g.Sum(tuple => tuple.Item3)+g.Sum(tuple => tuple.Item4)+g.Sum(tuple => tuple.Item5)))), (double) (g.Sum(tuple => tuple.Item5)/(double)((g.Sum(tuple => tuple.Item3)+g.Sum(tuple => tuple.Item4)+g.Sum(tuple => tuple.Item5)))),g.Sum(tuple=>tuple.Item5)));

    var dataitemsfirst = dataitems.Where(x => x.Item2.Equals(1));
    var dataitemssecond = dataitems.Where(x => x.Item2 != 1);

    Dictionary<string, double> firstsubmissionrejected = dataitemsfirst
    .GroupBy(t =>  t.Item1)
    .ToDictionary(g => g.Key, g => (double)((double)g.Sum(tuple => tuple.Item5) / (double)((g.Sum(tuple => tuple.Item3) + g.Sum(tuple => tuple.Item4) + g.Sum(tuple => tuple.Item5)))));

    Dictionary<string, double> secondsubmissionrejected = dataitemssecond
    .GroupBy(t =>  t.Item1)
    .ToDictionary(g => g.Key, g => (double)((double)g.Sum(tuple => tuple.Item5) / (double)((g.Sum(tuple => tuple.Item3) + g.Sum(tuple => tuple.Item4) + g.Sum(tuple => tuple.Item5)))));

    foreach (var row in final)
    {
        output += "<tr>";
        output += "<td>" + row.Item1 + "</td>";
        output += "<td style='text-align:center'>" + row.Item2 + "</td>";
        output += "<td style='text-align:center'>" + String.Format("{0:P2}", (double) row.Item3)+ "</td>";
        output += "<td style='text-align:center'>" + String.Format("{0:P2}", (double) row.Item4) + "</td>";
        output += "<td style='text-align:center'>" + String.Format("{0:P2}", (double) row.Item5) + "</td>";
        output += "<td style='text-align:center'>";
        if (firstsubmissionrejected.ContainsKey(row.Item1)) {
            output += String.Format("{0:P2}", (double)firstsubmissionrejected[row.Item1]);
                }
        else
        {
            output += "0.00%";
        }
        output += "</td>";
        output += "<td style='text-align:center'>";
        if (secondsubmissionrejected.ContainsKey(row.Item1)) {
            output += String.Format("{0:P2}", (double)secondsubmissionrejected[row.Item1]);
                }
        else
        {
            output += "0.00%";
        }
        output += "</td>";
        output += "</tr>";
%>
        <%--<tr>
            <td><%= row.Item1%></td>
            <td style="text-align:center"><%=row.Item2 %></td>
            <td style="text-align:center"><%=String.Format("{0:P2}.", (double) row.Item3) %></td>
            <td style="text-align:center"><%=String.Format("{0:P2}.", (double) row.Item4) %></td>
            <td style="text-align:center"><%=String.Format("{0:P2}.", (double) row.Item5) %></td>
            <td style="text-align:center"><%=firstsubmissionrejected.ContainsKey(row.Item1) ? String.Format("{0:P2}.", (double)firstsubmissionrejected[row.Item1]):"0" %></td>
            <td style="text-align:center"><%=secondsubmissionrejected.ContainsKey(row.Item1) ? String.Format("{0:P2}.", (double)secondsubmissionrejected[row.Item1]):"0" %></td>
            <%--<td style="text-align:center"><%=String.Format("{0:P2}.", (double) (1-(firstsubmissionrejected/totalrejected))) %></td>
            
            <%--<td style="text-align:center"><%=String.Format("{0:P2}.", (double)row.Item4/(double)(row.Item2 + row.Item3 + row.Item4)) %></td>
        </tr>--%>
<%}
    output += "</tbody></table>";

    //if (rptformat == "Excel")
    //{
    //    HttpContext.Current.Response.Clear();

    //    Response.ContentType = "application/msexcel";
    //    Response.AddHeader("content-disposition", "attachment; filename=Print.xls");
    //    Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
    //    Response.Write("<head>");
    //    Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
    //    Response.Write("<!--[if gte mso 9]><xml>");
    //    Response.Write("<x:ExcelWorkbook>");
    //    Response.Write("<x:ExcelWorksheets>");
    //    Response.Write("<x:ExcelWorksheet>");
    //    Response.Write("<x:Name>Report Data</x:Name>");
    //    Response.Write("<x:WorksheetOptions>");
    //    Response.Write("<x:Print>");
    //    Response.Write("<x:ValidPrinterInfo/>");
    //    Response.Write("</x:Print>");
    //    Response.Write("</x:WorksheetOptions>");
    //    Response.Write("</x:ExcelWorksheet>");
    //    Response.Write("</x:ExcelWorksheets>");
    //    Response.Write("</x:ExcelWorkbook>");
    //    Response.Write("</xml>");
    //    Response.Write("<![endif]--> ");
    //    Response.Write(output); // give ur html string here
    //    Response.Write("</head>");
    //    Response.Flush();

    //    //HttpContext.Current.Response.Clear();
    //    Response.End();
    //}
    //else
    //{
    //    Response.Write(output);
    //}
    Response.Write(output);
    %>
<%--</tbody>

</table>--%>
<%--<div class="sn-article-list sn-article-list-shortdetail">
    <ul>
    <%//var typegroups = this.Model.Items.OrderBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType")));
        foreach (var content in this.Model.Items)
        {
            var actionName = content.ContentHandler is User ? "Profile" : "Browse";
          %>
      
        <div class="sn-search-result ui-helper-clearfix">
            <div style="float:left; padding:3px 10px 3px 10px;">
              <%= SenseNet.Portal.UI.IconHelper.RenderIconTag(content.Icon, null, 32) %>
            </div>            
            <div style="padding:3px 0 5px 0;">               
              <a href="<%=Actions.ActionUrl(content, actionName)%>"><%= HttpUtility.HtmlEncode(content.DisplayName) %></a>
              <br/>
              <%= content.ContentHandler.ParentId == 0 ? string.Empty : content.ContentHandler.Parent.GetProperty<string>("DocumentType")  + "<br />" + content.ContentHandler.GetProperty<string>("Decision")%>
            </div>
        </div>
        
    <%} %>
    </ul>
</div>--%>



