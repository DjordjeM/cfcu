﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.Portlets.ContentCollectionView" CodeBehind="/Code/ExAnteReport1.ascx.cs"%>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SenseNet.Portal.Portlets" %>
<%@ Import Namespace="SenseNet.Portal.Helpers" %>
<%@ Import Namespace="SenseNet.Portal.Virtualization" %>
<%@ Import Namespace="SenseNet.ContentRepository" %>
<%@ Import Namespace="SenseNet.ContentRepository.Storage" %>
<%@ Import Namespace="OfficeOpenXml" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="HtmlAgilityPack" %>
<%@ Import Namespace="Microsoft.Reporting.WebForms" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>


<%--<sn:ScriptRequest ID="Scriptrequest2" runat="server" Path="$skin/scripts/jszip/jszip.min.js" />--%>

<script runat="server" type="text/C#">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        //ScriptManager Smgr = ScriptManager.GetCurrent(Page);
        ////ScriptManagerProxy SmgrProxy = new ScriptManagerProxy();
        //Smgr.EnableScriptGlobalization = true;
        ScriptManager Smgr = ScriptManager.GetCurrent(Page);
        Smgr.EnableViewState = true;
        
        
        if (!IsPostBack)
        {
            foreach (var key in HttpContext.Current.Request.Params.AllKeys)
            {
                string keyString = key.Split('$').LastOrDefault();

                var ctrl = this.Parent.FindControlRecursive(keyString);
                var value = HttpContext.Current.Request[key].ToString();
                
                if (ctrl == null)
                    continue;
                if (ctrl is TextBox)
                {
                    (ctrl as TextBox).Text = value;
                }
                else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
                {
                    (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value = value;
                }
                else
                {
                    continue;
                }

            }

            string Path = "/Root/Global/renderers/ReportFiles/Report1.rdlc";

            Node node = Node.LoadNode(Path);
            var binaryData = node.GetBinary("Binary");
            Stream binaryStream = binaryData.GetStream();

            ScriptManager Smgr2 = ScriptManager.GetCurrent(Page);
            Smgr2.EnableViewState = true;

            //using (StreamReader rdlcSR = new StreamReader(binaryStream))
            //{
            //    ReportViewer ReportViewer1 = new ReportViewer();
                
            //    //ReportViewer1.ProcessingMode = ProcessingMode.Local;
                
            //    ReportViewer1.LocalReport.LoadReportDefinition(rdlcSR);

            //    ReportViewer1.LocalReport.Refresh();
            //}
            
            //ReportViewer ReportViewer1 = new ReportViewer();

            //ReportViewer1.ProcessingMode = ProcessingMode.Local;

            //ReportViewer1.LocalReport.LoadReportDefinition(binaryStream);

            //ReportDataSource datasource = new ReportDataSource("Results", dsResults.Tables[0]);

            //ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
public string GetNonSpecificParams()
{
    var paramlist = "";
    foreach (var key in HttpContext.Current.Request.Params.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault(); 
        
        var ctrl = this.Parent.FindControlRecursive(keyString);
        var value = string.Empty;
        if (ctrl == null)
            continue;
        if (ctrl is TextBox)
        {
            value = (ctrl as TextBox).Text;
        }
        else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
        {
            value = (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value;
        }
        else
        {
            continue;
        }
        var firstAction = Model.Pager.Actions.FirstOrDefault();
        if (firstAction == null)
        {
            continue;
        }
        if (!string.IsNullOrEmpty(value) && !firstAction.Url.Contains(keyString + "="))
        {
            paramlist += "&" + keyString + "=" + HttpUtility.UrlEncode(value);
        }
       
    }    
    return paramlist;
}

</script>
<%--<sn:ScriptRequest ID="Scriptrequest1" runat="server" Path="$skin/scripts/jszip/jszip.min.js" />
<div class="sn-search-result-count">
    <span><%=GetGlobalResourceObject("SearchPortlet", "ResultCount")%>: </span><strong><%=Model.Pager.TotalCount%></strong>
</div>--%>




<div style="display:none">
    <sn:ActionMenu ID="ActionMenu1" runat="server" Text="hello" NodePath="/root" Scenario="ListItem"></sn:ActionMenu>
</div>
<% 
    var allowedTypes = new string[6];
    var rptformat = "";
    foreach (var key in HttpContext.Current.Request.Form.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault();
        string keyValue = "";

        if (keyString == "docType")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            allowedTypes = keyValue.Split(',').ToArray();
        }
        keyValue = HttpContext.Current.Request.Form[key];


        //if (keyString == "rptFormat")
        //{
        //    keyValue = HttpContext.Current.Request.Form[key];
        //    rptformat = keyValue;
        //}


        //Response.Write("key: " + keyString + " value: " + keyValue.ToString() + " allowedtypes length: "+ allowedTypes.Length +"<br />");

        //foreach (var tp in allowedTypes)
        //{
        //    Response.Write(tp + ',');
        //}
        //string keyValue = HttpContext.Current.Request.Form["docType"];
        //Response.Write("keyValue: " + keyValue + "<br />");
    }
    //foreach (string key in HttpContext.Current.Request.Form.AllKeys)
    //{

    //    string keyValue = HttpContext.Current.Request.Form[key];
    //    Response.Write("keyValue: " + keyValue + "<br />");
    //}

    //Response.Write("doctype value: " + Request.Form["docType"]);
    %>
<%--<script src="http://cdn.kendostatic.com/2014.3.1029/js/jszip.min.js"></script>--%>
<% 
    var output = "";
    output += "<div id='test'><table id='gridX' class='displayX'>";
    output += "<thead>";
    output += "<tr>";
    output += "<th data-field='dtype'>Document Type</th>";
    output += "<th data-field='submitted'>Submitted</th>";
    output += "<th data-field='approved'>Approved</th>";
    output += "<th data-field='suspended'>Suspended</th>";
    output += "<th data-field='rejected'>Rejected</th>";
    output += "<th data-field='percentrejected'>Percent rejected</th>";
    output += "</tr>";
    output += "</thead>";

    DataSet dsResults = new DataSet();

    DataTable dtResults = new DataTable("Results");
    
    dtResults.Columns.AddRange(new DataColumn[] 
    {
        new DataColumn("DocumentType", System.Type.GetType("System.String")),
        new DataColumn("Submitted",System.Type.GetType("System.Int16")),
        new DataColumn("Approved",System.Type.GetType("System.Int16")),
        new DataColumn("Suspended",System.Type.GetType("System.Int16")),
        new DataColumn("Rejected",System.Type.GetType("System.Int16")),
        new DataColumn("PercentRejected",System.Type.GetType("System.Double"))
    }
        );

    dsResults.Tables.Add(dtResults);
    
    //DataRow newResultsRow = dsResults.Tables["Results"].NewRow();

    //newResultsRow["CustomerID"] = "ALFKI";
    //newResultsRow["CompanyName"] = "Alfreds Futterkiste";

    //dsResults.Tables["Customers"].Rows.Add(newResultsRow);

    //var typegroups2 = this.Model.Items.OrderBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType")));

    List<Tuple<string, string, int, int, int>> dataitems = new List<Tuple<string, string, int, int, int>>();


    //Response.Write("item count: " + content2.Count());

    foreach (var content in this.Model.Items)
    {
        
        int countapproved = 0;
        int countsuspended = 0;
        int countrejected = 0;
        
        var procuremnttype = content.ContentHandler.Parent.Parent.NodeType.Name.Replace("IPAProject", "");
        
        string doctype = content.ContentHandler.GetProperty<string>("EADocumentType");

        string decision = content.ContentHandler.GetProperty<string>("EADecision");
        switch (content.ContentHandler.GetProperty<string>("EADecision"))
        {
            case "Approved":
                countapproved++;
                break;

            case "Suspended":
                countsuspended++;
                break;

            case "Rejected":
                countrejected++;
                break;
        }

        if (allowedTypes.Contains(doctype, StringComparer.OrdinalIgnoreCase))
        {
            dataitems.Add(new Tuple<string, string, int, int, int>(doctype, decision, countapproved, countsuspended, countrejected));
        }


    }

    // var allowedTypes = new[]{"Corrigendum"};
    var final = dataitems.
         GroupBy(t => t.Item1)
         .Where(g => g.Count()>0)//o => allowedStatus.Contains(o.StatusCode)
              .Select(g => Tuple.Create(g.Key, g.Sum(tuple => tuple.Item3), g.Sum(tuple => tuple.Item4), g.Sum(tuple => tuple.Item5)));

    int totalapproved = 0;
    int totalsuspended = 0;
    int totalrejected = 0 ;
    int totalsubmitted = 0;
    //double percentrejected = 0;
    foreach (var row in final)
    {
        totalapproved += row.Item2;
        totalsuspended += row.Item3;
        totalrejected += row.Item4;

        output += "<tr>";
        output += "<td>" + row.Item1 + "</td>";
        output += "<td style='text-align:center'>" + (row.Item2+row.Item3+row.Item4).ToString() + "</td>";
        output += "<td style='text-align:center'>" + row.Item2 + "</td>";
        output += "<td style='text-align:center'>" + row.Item3 + "</td>";
        output += "<td style='text-align:center'>" + row.Item4 + "</td>";

        output += "<td style='text-align:center'>" + String.Format("{0:P2}", (double)row.Item4 / (double)(row.Item2 + row.Item3 + row.Item4)) + "</td>";
        output += "</tr>";

        DataRow newResultsRow = dsResults.Tables["Results"].NewRow();

        newResultsRow["DocumentType"] = row.Item1;
        newResultsRow["Submitted"] = row.Item2 + row.Item3 + row.Item4;
        newResultsRow["Approved"] = row.Item2;
        newResultsRow["Suspended"] = row.Item3;
        newResultsRow["Rejected"] = row.Item4;
        newResultsRow["PercentRejected"] = (double)row.Item4 / (double)(row.Item2 + row.Item3 + row.Item4);

        dsResults.Tables["Results"].Rows.Add(newResultsRow);
}
    totalsubmitted = totalapproved + totalsuspended + totalrejected;
    double percentrejected = (double) totalrejected/ (double)totalsubmitted;

    
    output += "<tr>";
    output += "<td>Total</td>";
    output += "<td style='text-align:center'>" + totalsubmitted + "</td>";
    output += "<td style='text-align:center'>" + totalapproved + "</td>";
    output += "<td style='text-align:center'>" + totalsuspended + "</td>";
    output += "<td style='text-align:center'>" + totalrejected + "</td>";
    output += "<td style='text-align:center'>" + String.Format("{0:P2}", percentrejected) + "</td>";
    output += "</tr>";
    output += "</tbody>";
    output += "</table></div>";

    DataRow newTotalResultsRow = dsResults.Tables[0].NewRow();

    newTotalResultsRow["DocumentType"] = "TOTAL";
    newTotalResultsRow["Submitted"] = totalsubmitted;
    newTotalResultsRow["Approved"] = totalapproved;
    newTotalResultsRow["Suspended"] = totalsuspended;
    newTotalResultsRow["Rejected"] = totalrejected;
    newTotalResultsRow["PercentRejected"] = 0;//test value else percentrejected;

    dsResults.Tables["Results"].Rows.Add(newTotalResultsRow);

    Response.Write(output);
    Response.Flush();

    //retrieve rdlc file

    string Path = "/Root/Global/renderers/ReportFiles/Report1.rdlc";
    
    Node node = Node.LoadNode(Path);
    var binaryData = node.GetBinary("Binary");
    Stream binaryStream = binaryData.GetStream();
    
   
    //XmlBinaryReaderSession
    //System.IO.StreamReader stream1 = binaryData.Ge 

    StreamReader stream = new System.IO.StreamReader(binaryStream);
    XmlTextReader reader = null;
    reader = new XmlTextReader(stream);
    while (!stream.EndOfStream)
    {
        reader.Read();
    }
       
    //setup report
    //if (!Page.IsPostBack)
    //{
    //ReportViewer ReportViewer1 = new ReportViewer();

    //ReportViewer1.ProcessingMode = ProcessingMode.Local;

    //ReportViewer1.LocalReport.LoadReportDefinition(binaryStream);

    //StringWriter stringWriter = new StringWriter();
    //HtmlTextWriter htw = new HtmlTextWriter(stringWriter);
    
    //ReportViewer1.RenderControl(htw);

   // ReportDataSource datasource = new ReportDataSource("Results", dsResults.Tables[0]);

  //  ReportViewer1.LocalReport.DataSources.Add(datasource);

        //ReportViewer1.Width = 300;

        //ScriptManager Smgr = ScriptManager.GetCurrent(Page);
        
        
        //if (Smgr == null) throw new Exception("ScriptManager not found.");
        //StringWriter stringWriter = new StringWriter();
        //HtmlTextWriter htw = new HtmlTextWriter(stringWriter);
        //ReportViewer1.RenderControl(htw);
        //htw.Flush();
   // }  
    
    
    %>
        
<div id="grdxxx"></div>
<div id="rptvwr">
    <%--<form runat="server">--%>
    
       <%-- <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"  >
            <Services>
                <asp:ServiceReference Path="scriptresource.axd" />
                <asp:ServiceReference Path="webresource.axd" />
            </Services>
        </asp:ScriptManagerProxy>--%>
   
    <%--<sn:SNScriptManager ID="SNScriptManager100" ScriptPath="webresource.axd" runat="server"></sn:SNScriptManager>--%>
    
    <%
         
        //SmgrProxy
        //ScriptManager Smgr = ScriptManager.GetCurrent(Page);
        //Smgr.EnableViewState = true;
        //Smgr.DataBind();
        
        ReportViewer ReportViewer1 = new ReportViewer();
        
        ReportViewer1.ProcessingMode = ProcessingMode.Local;

        ReportViewer1.LocalReport.LoadReportDefinition(binaryStream);

        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(stringWriter);

        ReportViewer1.AsyncRendering = true;
        //ReportViewer1.

        ReportViewer1.RenderControl(htw);
         %>

        <%--<rsweb:ReportViewer ID="ReportViewer1" runat="server">
    </rsweb:ReportViewer>--%>
    <%--</form>--%>
</div>
<script type="text/javascript">
    //Ext.Loader.setConfig({
    //    enabled: true
    //});
    ////Ext.Loader.setPath('Ext.ux', 'ext-5.1.1/build/examples/ux');
    //Ext.require([
    //    'Ext.data.*',
    //    'Ext.grid.*',
    //    'Ext.ux.grid.TransformGrid'
    //]);
    Ext.onReady(function () {
        //('here');
        //var btn = Ext.get("create-grid");
        //btn.on("click", function () {
        //btn.dom.disabled = true;

        // create the grid
        var grid = Ext.create('Ext.ux.grid.TransformGrid', "gridX", {
            stripeRows: true,
            renderTo: 'grdxxx',
            //contentEl: "test"
            height: 130
        });
        //grid.render('div#test');
        //var gridarray = Ext.ComponentQuery.query('test');
        //alert(gridarray[0]);
        //alert(Ext.ComponentQuery.query('grdxxx'));
    });
    
    //});

    //Ext.create('Ext.data.Store', {
    //    storeId: 'simpsonsStore',
    //    fields: ['name', 'email', 'phone'],
    //    data: {
    //        'items': [
    //            { 'name': 'Lisa', "email": "lisa@simpsons.com", "phone": "555-111-1224" },
    //            { 'name': 'Bart', "email": "bart@simpsons.com", "phone": "555-222-1234" },
    //            { 'name': 'Homer', "email": "home@simpsons.com", "phone": "555-222-1244" },
    //            { 'name': 'Marge', "email": "marge@simpsons.com", "phone": "555-222-1254" }
    //        ]
    //    },
    //    proxy: {
    //        type: 'memory',
    //        reader: {
    //            type: 'json',
    //            root: 'items'
    //        }
    //    }
    //});


    //var nameRenderer = function () {
    //    return '<div ext-xtype="textfield"></div>';

    //}


    //var simpsonPanel = Ext.create('Ext.grid.Panel', {
    //    title: 'Simpsons',
    //    store: Ext.data.StoreManager.lookup('simpsonsStore'),
    //    columns: [
    //        { text: 'Name', dataIndex: 'name', renderer: nameRenderer },
    //        { text: 'Email', dataIndex: 'email', flex: 1 },
    //        { text: 'Phone', dataIndex: 'phone' }
    //    ],
    //    height: 200,
    //    width: 400,
    //    listeners: {
    //        viewready: function (view) {
    //            var els = view.el.query('div[ext-xtype]');
    //            Ext.each(els, function (domEl) {
    //                var xtype = Ext.get(domEl).getAttribute('ext-xtype');
    //                Ext.widget(xtype, { renderTo: domEl });
    //            }, this);
    //            view.up('viewport').doLayout();


    //        }
    //    }
    //});
    //Ext.application({
    //    name: 'Fiddle',

    //    launch: function () {
    //        Ext.create('Ext.container.Viewport', {
    //            layout: {
    //                type: 'fit'

    //            },
    //            items: simpsonPanel

    //        });
    //    }
    //});
</script>

<%--<rsweb:ReportViewer ID="ReportViewer1" runat="server">
</rsweb:ReportViewer>--%>


<%--<script type="text/javascript">
    $("#grid2").kendoGrid({
        toolbar: ["Excel"],
        excel: {
            fileName: "Kendo UI Grid Export.xlsx"
        },
    });
</script>--%>

<%--<div class="sn-article-list sn-article-list-shortdetail">
    <ul>
    <%//var typegroups = this.Model.Items.OrderBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType")));
        foreach (var content in this.Model.Items)
        {
            var actionName = content.ContentHandler is User ? "Profile" : "Browse";
          %>
      
        <div class="sn-search-result ui-helper-clearfix">
            <div style="float:left; padding:3px 10px 3px 10px;">
              <%= SenseNet.Portal.UI.IconHelper.RenderIconTag(content.Icon, null, 32) %>
            </div>            
            <div style="padding:3px 0 5px 0;">               
              <a href="<%=Actions.ActionUrl(content, actionName)%>"><%= HttpUtility.HtmlEncode(content.DisplayName) %></a>
              <br/>
              <%= content.ContentHandler.ParentId == 0 ? string.Empty : content.ContentHandler.Parent.GetProperty<string>("DocumentType")  + "<br />" + content.ContentHandler.GetProperty<string>("Decision")%>
            </div>
        </div>
        
    <%} %>
    </ul>
</div>--%>



