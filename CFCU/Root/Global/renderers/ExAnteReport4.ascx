﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.Portlets.ContentCollectionView" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SenseNet.Portal.Portlets" %>
<%@ Import Namespace="SenseNet.Portal.Helpers" %>
<%@ Import Namespace="SenseNet.Portal.Virtualization" %>
<%@ Import Namespace="SenseNet.ContentRepository" %>
<%@ Import Namespace="SenseNet.ContentRepository.Storage" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="CFCU.Code" %>

<script runat="server" type="text/C#">
public int GetQuarter(DateTime dt)
     {
         return (dt.Month - 1) / 3 + 1;
     }
</script>

<%--<div class="sn-search-result-count">
    <span><%=GetGlobalResourceObject("SearchPortlet", "ResultCount")%>: </span><strong><%=Model.Pager.TotalCount%></strong>
</div>--%>

<div style="display:none">
    <sn:ActionMenu ID="ActionMenu1" runat="server" Text="hello" NodePath="/root" Scenario="ListItem"></sn:ActionMenu>
</div>
<% 
    List<string> allowedTypes = new List<string>();
    
    foreach (var key in HttpContext.Current.Request.Form.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault();
        string keyValue = "";

        if (keyString == "docType")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            allowedTypes = keyValue.Split(',').ToList();
        }
    }

    //var typegroups2 = this.Model.Items.OrderBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType")));

    List<string> proctypes = new List<string>();
    proctypes.Add("Services");
    proctypes.Add("Supplies");
    proctypes.Add("Works");
    proctypes.Add("Grants");
    proctypes.Add("Twinnings");
    proctypes.Add("Framework");
    
    List<Tuple<int, string, string, int, int, int>> dataitems = new List<Tuple<int, string, string, int, int, int>>();
        
    foreach (var content in this.Model.Items)
    {
        int countapproved = 0;
        int countsuspended = 0;
        int countrejected = 0;
        var procuremnttype = content.ContentHandler.Parent.Parent.Parent.NodeType.Name.Replace("IPAProject","");
        string doctype = content.ContentHandler.Parent.GetProperty<string>("DocumentType");

        DateTime decisiondt = content.ContentHandler.GetProperty<DateTime>("DecisionDate");

        string decision = content.ContentHandler.GetProperty<string>("Decision");
        switch (content.ContentHandler.GetProperty<string>("Decision"))
        {
            case "Approved":
                countapproved++;
                break;

            case "Suspended":
                countsuspended++;
                break;

            case "Rejected":
                countrejected++;
                break;
        }

        if (allowedTypes.Contains(doctype, StringComparer.OrdinalIgnoreCase))
        {
            dataitems.Add(new Tuple<int, string, string, int, int, int>(GetQuarter(decisiondt), procuremnttype, doctype, countapproved, countsuspended, countrejected));
        }
    }

    for (int n = 1; n <= 4; n++)
    {
        foreach (var dtype in allowedTypes)
        {
            foreach (var proctype in proctypes)
            {
                dataitems.Add(new Tuple<int, string, string, int, int, int>(n, proctype, dtype, 0, 0, 0));
            }
        }
    }
    
    DataTable consolidatedtable = new DataTable();
    
    foreach (var procurementtype in proctypes)
    {
        var final3 = dataitems.
            GroupBy(t => new { t.Item3, t.Item2, t.Item1 })
             .Select((g) =>
                      new
                      {
                          ProcurementType = g.Key.Item2,
                          DocumentType = g.Key.Item3,
                          Quarter = g.Key.Item1,

                          TotalSubmitted = g.Sum(x => x.Item4) + g.Sum(x => x.Item5) + g.Sum(x => x.Item6),
                          TotalRejected = g.Sum(x => x.Item6)
                      })
                      .OrderBy(x => x.Quarter)
                      .ThenBy(x => x.ProcurementType)
                      .Where(x => x.ProcurementType.Equals(procurementtype));

        var table = final3.ToPivotTable(
                 item => item.Quarter,
                 item => item.DocumentType,

                 item => item.Any() ? item.First().TotalRejected : 0,
                 item => item.Any() ? item.First().TotalSubmitted : 0,
                 procurementtype
            );

        if (consolidatedtable == null || consolidatedtable.Rows.Count == 0)
        {
            //check if datatable not empty first
            consolidatedtable = table;
        }
        else
        {
            foreach (DataRow dr in table.Rows)
            {
                consolidatedtable.Rows.Add(dr.ItemArray);
            }
        }
    }

    Response.Write(consolidatedtable.ConvertDataTableToHtmlTable("Services", "", "grid4") + "<br />");
%>  
        
<%--<div class="sn-article-list sn-article-list-shortdetail">
    <ul>
    <%//var typegroups = this.Model.Items.OrderBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType")));
        foreach (var content in this.Model.Items)
        {
            var actionName = content.ContentHandler is User ? "Profile" : "Browse";
          %>
      
        <div class="sn-search-result ui-helper-clearfix">
            <div style="float:left; padding:3px 10px 3px 10px;">
              <%= SenseNet.Portal.UI.IconHelper.RenderIconTag(content.Icon, null, 32) %>
            </div>            
            <div style="padding:3px 0 5px 0;">               
              <a href="<%=Actions.ActionUrl(content, actionName)%>"><%= HttpUtility.HtmlEncode(content.DisplayName) %></a>
              <br/>
              <%= content.ContentHandler.ParentId == 0 ? string.Empty : content.ContentHandler.Parent.GetProperty<string>("DocumentType")  + "<br />" + content.ContentHandler.GetProperty<string>("Decision")%>
            </div>
        </div>
        
    <%} %>
    </ul>
</div>--%>
