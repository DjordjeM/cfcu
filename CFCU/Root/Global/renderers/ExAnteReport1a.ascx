﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.Portlets.ContentCollectionView" CodeBehind="~/Code/ExAnteReport1a.ascx.cs" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SenseNet.Portal.Portlets" %>
<%@ Import Namespace="SenseNet.Portal.Helpers" %>
<%@ Import Namespace="SenseNet.Portal.Virtualization" %>
<%@ Import Namespace="SenseNet.ContentRepository" %>
<%@ Import Namespace="SenseNet.ContentRepository.Storage" %>

<script runat="server" type="text/C#">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!IsPostBack)
        {
            foreach (var key in HttpContext.Current.Request.Params.AllKeys)
            {
                string keyString = key.Split('$').LastOrDefault();

                var ctrl = this.Parent.FindControlRecursive(keyString);
                var value = HttpContext.Current.Request[key].ToString();
                
                if (ctrl == null)
                    continue;
                if (ctrl is TextBox)
                {
                    (ctrl as TextBox).Text = value;
                }
                else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
                {
                    (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value = value;
                }
                else
                {
                    continue;
                }
             
            }

        }
    }
public string GetNonSpecificParams()
{
    var paramlist = "";
    foreach (var key in HttpContext.Current.Request.Params.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault(); 
        
        var ctrl = this.Parent.FindControlRecursive(keyString);
        var value = string.Empty;
        if (ctrl == null)
            continue;
        if (ctrl is TextBox)
        {
            value = (ctrl as TextBox).Text;
        }
        else if (ctrl is System.Web.UI.HtmlControls.HtmlInputText)
        {
            value = (ctrl as System.Web.UI.HtmlControls.HtmlInputText).Value;
        }
        else
        {
            continue;
        }
        var firstAction = Model.Pager.Actions.FirstOrDefault();
        if (firstAction == null)
        {
            continue;
        }
        if (!string.IsNullOrEmpty(value) && !firstAction.Url.Contains(keyString + "="))
        {
            paramlist += "&" + keyString + "=" + HttpUtility.UrlEncode(value);
        }
       
    }    
    return paramlist;
}

</script>

<div class="sn-search-result-count">
    <span><%=GetGlobalResourceObject("SearchPortlet", "ResultCount")%>: </span><strong><%=Model.Pager.TotalCount%></strong>
</div>

<div style="display:none">
    <sn:ActionMenu ID="ActionMenu1" runat="server" Text="hello" NodePath="/root" Scenario="ListItem"></sn:ActionMenu>
</div>
<% 
    var allowedTypes = new string[6];
    foreach (var key in HttpContext.Current.Request.Form.AllKeys)
    {
        string keyString = key.Split('$').LastOrDefault();
        string keyValue = "";

        if (keyString == "docType")
        {
            keyValue = HttpContext.Current.Request.Form[key];
            allowedTypes = keyValue.Split(',').ToArray();
        }
        keyValue = HttpContext.Current.Request.Form[key];

        //Response.Write("key: " + keyString + " value: " + keyValue.ToString() + " allowedtypes length: "+ allowedTypes.Length +"<br />");
        
        //foreach (var tp in allowedTypes)
        //{
        //    Response.Write(tp + ',');
        //}
        //string keyValue = HttpContext.Current.Request.Form["docType"];
        //Response.Write("keyValue: " + keyValue + "<br />");
    }
    //foreach (string key in HttpContext.Current.Request.Form.AllKeys)
    //{

    //    string keyValue = HttpContext.Current.Request.Form[key];
    //    Response.Write("keyValue: " + keyValue + "<br />");
    //}

    //Response.Write("doctype value: " + Request.Form["docType"]);
    %>


<%
    //var typegroups2 = this.Model.Items.OrderBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c => c.ContentHandler.Parent.GetProperty<string>("DocumentType")));

    List<Tuple<string, string, int, int, int>> dataitems = new List<Tuple<string, string, int, int, int>>();

    List<Tuple<string, string, int, int, int, int>> dataitems2 = new List<Tuple<string, string, int, int, int, int>>();





    foreach (var content in this.Model.Items)
    {

        int countapproved = 0;
        int countsuspended = 0;
        int countrejected = 0;
        var procuremnttype = content.ContentHandler.Parent.Parent.Parent.GetProperty<string>("ProcurementType");
        Response.Write(procuremnttype + "<br />");
        string doctype = content.ContentHandler.Parent.GetProperty<string>("DocumentType");

        string decision = content.ContentHandler.GetProperty<string>("Decision");
        switch (content.ContentHandler.GetProperty<string>("Decision"))
        {
            case "Approved":
                countapproved++;
                break;

            case "Suspended":
                countsuspended++;
                break;

            case "Rejected":
                countrejected++;
                break;
        }

        int countlegality = 0;
        int countcompliance = 0;
        int countmanagement = 0;
        int countquality = 0;

        string[] reasons = content.ContentHandler.GetProperty<string>("RejectionReasons").Split(';');
        foreach (string reason in reasons)
        {

            switch (reason)
            {
                case "legality":
                    countlegality++;
                    break;

                case "compliance":
                    countcompliance++;
                    break;

                case "management":
                    countmanagement++;
                    break;

                case "quality":
                    countquality++;
                    break;
            }
        }

        if (allowedTypes.Contains(doctype, StringComparer.OrdinalIgnoreCase))
        {
            dataitems.Add(new Tuple<string, string, int, int, int>(doctype, decision, countapproved, countsuspended, countrejected));
            dataitems2.Add(new Tuple<string, string, int, int, int, int>(doctype, decision, countlegality, countcompliance, countmanagement, countquality));
        }

    }

    var final = dataitems.
         GroupBy(t => t.Item1)
         .Where(g => g.Count()>0)
              .Select(g => Tuple.Create(g.Key, g.Sum(tuple => tuple.Item3), g.Sum(tuple => tuple.Item4), g.Sum(tuple => tuple.Item5)));

    var final_t2 = dataitems2.
         GroupBy(t => t.Item1)
         .Where(g => g.Count()>0)//o => allowedStatus.Contains(o.StatusCode)
              .Select(g => Tuple.Create(g.Key, g.Sum(tuple => tuple.Item3), g.Sum(tuple => tuple.Item4), g.Sum(tuple => tuple.Item5), g.Sum(tuple => tuple.Item6)));


    int totalapproved = 0;
    int totalsuspended = 0;
    int totalrejected = 0 ;
    int totalsubmitted = 0;

%>
    <table id="grid" border="1" runat="server"> <%--Total number of ex-ante processed--%>
    <%--<thead>--%>
        <asp:TableHeaderRow ID="TableRow1" runat="server">
            <asp:TableHeaderCell>Document Type</asp:TableHeaderCell>
            <asp:TableHeaderCell>Submitted</asp:TableHeaderCell>
            <asp:TableHeaderCell>Approved</asp:TableHeaderCell>
            <asp:TableHeaderCell>Suspended</asp:TableHeaderCell>
            <asp:TableHeaderCell>Rejected</asp:TableHeaderCell>
            <asp:TableHeaderCell>% rejected</asp:TableHeaderCell>
        </asp:TableHeaderRow>
    <%--</thead>
    <tbody>--%>
<%
    foreach (var row in final)
    {
        totalapproved += row.Item2;
        totalsuspended += row.Item3;
        totalrejected += row.Item4;
%>
        <asp:TableRow runat="server">
            <asp:TableCell><%= row.Item1%></asp:TableCell>
            <asp:TableCell><%=row.Item2+row.Item3+row.Item4 %></asp:TableCell>
            <asp:TableCell><%=row.Item2 %></asp:TableCell>
            <asp:TableCell><%=row.Item3 %></asp:TableCell>
            <asp:TableCell><%=row.Item4 %></asp:TableCell>
            
            <asp:TableCell><%=String.Format("{0:P2}.", (double)row.Item4/(double)(row.Item2 + row.Item3 + row.Item4)) %></asp:TableCell>
        </asp:TableRow>
<%  }
    totalsubmitted = totalapproved + totalsuspended + totalrejected;
    double percentrejected = (double) totalrejected/ (double)totalsubmitted;
    %>
        <asp:TableRow runat="server">
            <asp:TableCell>Total</asp:TableCell>
            <asp:TableCell HorizontalAlign="Center"><%=totalsubmitted %></asp:TableCell>
            <asp:TableCell HorizontalAlign="Center""><%=totalapproved %></asp:TableCell>
            <asp:TableCell HorizontalAlign="Center"><%=totalsuspended%></asp:TableCell>
            <asp:TableCell HorizontalAlign="Center"><%=totalrejected %></asp:TableCell>
            <asp:TableCell HorizontalAlign="Center"><%=String.Format("{0:P2}.", percentrejected)  %></asp:TableCell>
        </asp:TableRow>
<%--</tbody>--%>
</table>

<br />

<asp:Table ID="grid2" border="1" runat="server">
    <asp:TableHeaderRow ID="TableRow2" runat="server">
            <asp:TableHeaderCell>Document Type</asp:TableHeaderCell>
            <asp:TableHeaderCell>Legality / regularity</asp:TableHeaderCell>
            <asp:TableHeaderCell>Compliance with program</asp:TableHeaderCell>
            <asp:TableHeaderCell>Sound financial management</asp:TableHeaderCell>
            <asp:TableHeaderCell>Quality of submitted documents</asp:TableHeaderCell>        
    </asp:TableHeaderRow> 
        <%
            foreach (var row in final_t2)
                {
                    
             %>
        <asp:TableRow runat="server">
            <asp:TableCell HorizontalAlign="Center"><%= row.Item1%></asp:TableCell>
            <asp:TableCell HorizontalAlign="Center"><%=row.Item2 %></asp:TableCell>
            <asp:TableCell HorizontalAlign="Center"><%=row.Item3 %></asp:TableCell>
            <asp:TableCell HorizontalAlign="Center"><%=row.Item4 %></asp:TableCell>
            <asp:TableCell HorizontalAlign="Center"><%=row.Item5 %></asp:TableCell>
        </asp:TableRow>
<%} %>
</asp:Table>
<br />
<div class="sn-article-list sn-article-list-shortdetail">
    <ul>
    <%//var typegroups = this.Model.Items.OrderBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType"))).AsEnumerable().GroupBy((c=>c.ContentHandler.Parent.GetProperty<string>("DocumentType")));
        foreach (var content in this.Model.Items)
        {
            var actionName = content.ContentHandler is User ? "Profile" : "Browse";
          %>
      
        <div class="sn-search-result ui-helper-clearfix">
            <div style="float:left; padding:3px 10px 3px 10px;">
              <%= SenseNet.Portal.UI.IconHelper.RenderIconTag(content.Icon, null, 32) %>
            </div>            
            <div style="padding:3px 0 5px 0;">               
              <a href="<%=Actions.ActionUrl(content, actionName)%>"><%= HttpUtility.HtmlEncode(content.DisplayName) %></a>
              <br/>
              <%= content.ContentHandler.ParentId == 0 ? string.Empty : content.ContentHandler.Parent.GetProperty<string>("DocumentType")  + "<br />" + content.ContentHandler.GetProperty<string>("Decision")%>
            </div>
        </div>
        
    <%} %>
    </ul>
</div>



