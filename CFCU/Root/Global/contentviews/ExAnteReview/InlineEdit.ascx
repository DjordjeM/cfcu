﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.UI.SingleContentView" %>

<sn:ShortText runat="server" ID="ShortTextField1" FieldName="EASenderTitle" />
<sn:ShortText runat="server" ID="ShortTextField2" FieldName="EAOS_IA" />
<sn:DatePicker runat="server" ID="DateTimeField1" FieldName="EADeliveryDate" />
<sn:DatePicker runat="server" ID="DateTimeField2" FieldName="EADeadlineReply" />
<sn:ShortText runat="server" ID="ShortTextField3" FieldName="EASubmittedVersion" />
<sn:DropDown runat="server" ID="ChoiceField1" FieldName="EADocumentNature" />
<sn:DropDown runat="server" ID="ChoiceField2" FieldName="EADocumentType" />
<sn:DropDown runat="server" ID="ChoiceField3" FieldName="EASubmissionNature" />
<sn:DatePicker runat="server" ID="DateTimeField3" FieldName="EADecisionDate" />
<sn:DropDown runat="server" ID="ChoiceField4" FieldName="EADecision" />
<sn:DropDown runat="server" ID="ChoiceField5" FieldName="EARejectionReasons" />

<%--
<sn:LongText runat="server" ID="LongTextField" FieldName="LongTextField" />
<sn:Number runat="server" ID="NumberField" FieldName="NumberField" />
<sn:WholeNumber runat="server" ID="IntegerField" FieldName="IntegerField" />
<sn:Boolean runat="server" ID="BooleanField" FieldName="BooleanField" />
<sn:DropDown runat="server" ID="ChoiceField" FieldName="ChoiceField" />
<sn:RadioButtonGroup runat="server" ID="ChoiceField2" FieldName="ChoiceField" />
<sn:DatePicker runat="server" ID="DateTimeField" FieldName="DateTimeField" />
<sn:ReferenceGrid runat="server" ID="ReferenceField" FieldName="ReferenceField" />
<sn:Binary runat="server" ID="BinaryField" FieldName="BinaryField" />
--%>

<%-- template example:
<sn:ShortText runat="server" ID="ShortTextField" FieldName="ShortTextField">
  <EditTemplate>
    <asp:TextBox ID="InnerShortText" runat="server"></asp:TextBox>
  </EditTemplate>
</sn:ShortText>
--%>

<%-- generic field control:  
<sn:GenericFieldControl runat="server" ID="GenericFieldControl1" ExcludedFields="Field1 Field2" />
--%>

<div class="sn-panel sn-buttons">
  <sn:CommandButtons ID="CommandButtons1" runat="server"/>
</div>
