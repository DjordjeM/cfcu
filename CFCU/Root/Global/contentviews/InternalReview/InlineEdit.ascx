﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.UI.SingleContentView" %>
<%@Import Namespace="SenseNet.Portal.UI" %>
<%--
<sn:ShortText runat="server" ID="ShortTextField" FieldName="ShortTextField" />
<sn:LongText runat="server" ID="LongTextField" FieldName="LongTextField" />
<sn:Number runat="server" ID="NumberField" FieldName="NumberField" />
<sn:WholeNumber runat="server" ID="IntegerField" FieldName="IntegerField" />
<sn:Boolean runat="server" ID="BooleanField" FieldName="BooleanField" />
<sn:DropDown runat="server" ID="ChoiceField" FieldName="ChoiceField" />
<sn:RadioButtonGroup runat="server" ID="ChoiceField2" FieldName="ChoiceField" />
<sn:DatePicker runat="server" ID="DateTimeField" FieldName="DateTimeField" />
<sn:ReferenceGrid runat="server" ID="ReferenceField" FieldName="ReferenceField" />
<sn:Binary runat="server" ID="BinaryField" FieldName="BinaryField" />
--%>

<%-- template example:
<sn:ShortText runat="server" ID="ShortTextField" FieldName="ShortTextField">
  <EditTemplate>
    <asp:TextBox ID="InnerShortText" runat="server"></asp:TextBox>
  </EditTemplate>
</sn:ShortText>

<sn:ContextInfo runat="server" Selector="CurrentContext" UsePortletContext="true" ID="myContext" />
--%>
<sn:ContextInfo runat="server" Selector="CurrentWorkspace" UsePortletContext="true" ID="myContext" />
<%
    var ws = myContext.Path.T;
    //var context = UITools.FindContextInfo(this, myContext);
    //var path = context.Path == null ? string.Empty : context.Path;
%>

<%-- generic field control:  --%>
<sn:GenericFieldControl runat="server" ID="GenericFieldControl1" ExcludedFields="Field1 Field2" />
<sn:ReferenceGrid ID="ReferenceGrid1" FieldName="Members" runat="server">
      <EditTemplate>
         <asp:TextBox ID="HiddenTextBoxControl" runat="server"  />
         <asp:Table ID="TableControl" runat="server" />
         <asp:Button ID="AddButtonControl" Text="Add" runat="server" 
      OnClientClick="SN.PickerApplication.open({MultiSelectMode: 'none', callBack: function(resultData) { if (!resultData) return; 
         alert('Selection finished!'); }}); return false;"/>
         <asp:Button ID="ChangeButtonControl" runat="server" Text="Change..." />
         <asp:Button ID="ClearButtonControl" Text="Clear" runat="server" />
      </EditTemplate>
   </sn:ReferenceGrid>
<% Response.Write(ReferenceGrid1.TreeRoots); %>
<div class="sn-panel sn-buttons">
  <sn:CommandButtons ID="CommandButtons1" runat="server"/>
</div>
