﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SenseNet.Portal.UI.SingleContentView" %>

<sn:GenericFieldControl runat="server" ID="GenericFieldControl1" ExcludedFields="ReferenceNumber Completion WorkspaceSkin IsWallContainer AllowedChildTypes InheritableVersioningMode VersioningMode InheritableApprovingMode" />

<%--  
<sn:ShortText runat="server" ID="ShortTextField" FieldName="ShortTextField" />
<sn:LongText runat="server" ID="LongTextField" FieldName="LongTextField" />
<sn:Number runat="server" ID="NumberField" FieldName="NumberField" />
<sn:WholeNumber runat="server" ID="IntegerField" FieldName="IntegerField" />
<sn:Boolean runat="server" ID="BooleanField" FieldName="BooleanField" />
<sn:DropDown runat="server" ID="ChoiceField" FieldName="ChoiceField" />
<sn:RadioButtonGroup runat="server" ID="ChoiceField2" FieldName="ChoiceField" />
<sn:DatePicker runat="server" ID="DateTimeField" FieldName="DateTimeField" />
<sn:ReferenceGrid runat="server" ID="ReferenceField" FieldName="ReferenceField" />
<sn:Binary runat="server" ID="BinaryField" FieldName="BinaryField" />
--%>

<%-- template example: --%>
<sn:ShortText runat="server" ID="ShortTextFieldX" FieldName="DisplayName">
  <EditTemplate>
    <asp:PlaceHolder ID="ResourceDiv" runat="server">
         <div class="sn-resdiv">test
            <asp:LinkButton ID="ResourceEditorLink" runat="server" /> TEST
            <asp:TextBox ID="Resources" runat="server" CssClass="sn-resbox" style="display:none" />
         </div>
      </asp:PlaceHolder>
    <asp:TextBox ID="InnerShortText" CssClass="sn-ctrl sn-ctrl-text sn-urlname-name" runat="server"></asp:TextBox>
    <asp:TextBox ID="NameAvailableControl" runat="server" style="display:none"></asp:TextBox>
  </EditTemplate>
</sn:ShortText>


<%-- generic field control:  
<sn:GenericFieldControl runat="server" ID="GenericFieldControl1" ExcludedFields="Field1 Field2" />
--%>

<div class="sn-panel sn-buttons">
  <sn:CommandButtons ID="CommandButtons1" runat="server"/>
</div>
