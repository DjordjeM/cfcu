﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SenseNet.Portal.UI.Controls;

using System.Reflection;
using SenseNet.ContentRepository.Fields;

namespace CFCU.Code
{
    public partial class ExAnteRpt1SearchForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
      
    }
}

namespace SenseNet.Portal.UI.Controls
{
    public class DropDownBindable : DropDown
    {
        [PersistenceMode(PersistenceMode.Attribute)]
        public string OptionsSource { get; set; }

        public override void SetData(object data)
        {
            if (data is List<string>)
                BuildControl(InnerListItemCollection, (List<string>)data);
            else
            {
                List<string> dataList = new List<string>();
                if (data != null)
                    dataList.Add(data.ToString());
                BuildControl(InnerListItemCollection, dataList);
            }
        }

        protected new void BuildControl(ListItemCollection itemCollection, List<string> selectedItems)
        {
            ChoiceField choiceField = this.Field as ChoiceField;
            if (choiceField == null)
                throw new InvalidCastException("ChoiceControl have to connect to a ChoiceField.");

            ChoiceFieldSetting setting = (ChoiceFieldSetting)this.Field.FieldSetting;
            BuildOptions(itemCollection, setting.Options, selectedItems);
        }

        protected new void BuildOptions(ListItemCollection listItems, List<ChoiceOption> configuredOptions, List<string> values)
        {
            if (OptionsSource == null)
            {
                base.BuildOptions(listItems, configuredOptions, values);
            }
            else
            {
                listItems.Clear();
                bool hasValues = values != null && values.Count != 0;
                List<string> valueList = hasValues ? new List<string>(values) : new List<string>();

                Type contentHandlerType = this.Content.ContentHandler.GetType();
                MethodInfo optionSourceMethod = contentHandlerType.GetMethod(OptionsSource);
                List<ChoiceOption> generatedOptions = (List<ChoiceOption>)optionSourceMethod.Invoke(this.Content.ContentHandler, new object[] { });
                foreach (ChoiceOption option in generatedOptions)
                {
                    ListItem newItem = new ListItem(option.Text, option.Value, option.Enabled);
                    if (hasValues)
                    {
                        newItem.Selected = valueList.Contains(option.Value);
                        valueList.Remove(option.Value);
                    }
                    else
                    {
                        newItem.Selected = option.Selected;
                    }
                    listItems.Add(newItem);
                }
            }
        }
    }
}