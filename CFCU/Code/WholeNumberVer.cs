﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SenseNet.ContentRepository.Storage;
using SenseNet.ContentRepository.Fields;
using SenseNet.Portal.UI.Controls;
using SenseNet.Portal.UI;

namespace CFCU.Code
{
    [ToolboxData("<{0}:WholeNumberVer ID=\"WholeNumberVer1\" runat=server></{0}:WholeNumberVer>")]
    public class WholeNumberVer : FieldControl, INamingContainer, ITemplateFieldControl
    {
        protected string PercentageControlID = "LabelForPercentage";
        // Fields ///////////////////////////////////////////////////////////////////////
        private TextBox _inputTextBox;
        //public Control GenericFieldControl;

        // Constructor //////////////////////////////////////////////////////////////////
		public WholeNumberVer()
        {
            InnerControlID = "InnerWholeNumberVer";
            _inputTextBox = new TextBox { ID = InnerControlID };
        }
        
        public override object GetData()
        {
            var innerControl = GetInnerControl() as TextBox;

            if ((!UseBrowseTemplate && !UseEditTemplate && !UseInlineEditTemplate) || innerControl == null)
            {
                #region original

                if (_inputTextBox.Text.Length == 0)
                    return null;

                return Convert.ToInt32(_inputTextBox.Text);

                #endregion
            }

            if (innerControl.Text.Length == 0)
                return null;

            return Convert.ToInt32(innerControl.Text);
        }

        public override void SetData(object data)
        {
            if (data == null)
            {
                _inputTextBox.Text = string.Empty;
            }
            else
            {
                _inputTextBox.Text = Convert.ToInt32(data) == int.MinValue ? string.Empty : data.ToString();
            }

            #region template

            if ((!UseBrowseTemplate && !UseEditTemplate) && !UseInlineEditTemplate)
                return;

            SetTitleAndDescription();

            var innerControl = GetInnerControl() as TextBox;
            var perc = GetLabelForPercentageControl();

            if (innerControl != null)
                innerControl.Text = Convert.ToString(_inputTextBox.Text);

            if (perc != null)
            {
                perc.Text = GetPercentageSign();
                perc.Visible = !string.IsNullOrEmpty(perc.Text);
            }

            #endregion
        }
        protected override void OnInit(EventArgs e)
        {

            base.OnInit(e);

            #region template

            if (UseBrowseTemplate || UseEditTemplate || UseInlineEditTemplate)
                return;

            #endregion

            #region original flow

            _inputTextBox.CssClass = String.IsNullOrEmpty(this.CssClass) ? "sn-ctrl sn-ctrl-number" : this.CssClass;
            Controls.Add(_inputTextBox);
            //_inputTextBox.ID = String.Concat(this.ID, "_", this.ContentHandler.Id.ToString());

            #endregion

        }
        
        private void ManipulateTemplateControls()
        {
            //
            //  This method is needed to ensure the common fieldcontrol logic.
            //
            var innerWholeNumber = GetInnerControl() as TextBox;
            var lt = GetLabelForTitleControl() as Label;
            var ld = GetLabelForDescription() as Label;

            if (innerWholeNumber == null) return;

            if (Field.ReadOnly)
            {
                var p = innerWholeNumber.Parent;
                if (p != null)
                {
                    p.Controls.Remove(innerWholeNumber);
                    if (lt != null) lt.AssociatedControlID = string.Empty;
                    if (ld != null) ld.AssociatedControlID = string.Empty;
                    p.Controls.Add(new LiteralControl(innerWholeNumber.Text));
                }
            }
            else if (ReadOnly)
            {
                innerWholeNumber.Enabled = !ReadOnly;
                innerWholeNumber.EnableViewState = false;
            }

            if (RenderMode != FieldControlRenderMode.InlineEdit)
                return;

            innerWholeNumber.Attributes.Add("Title", String.Concat(Field.DisplayName, " ", Field.Description));

        }

        private string GetPercentageSign()
        {
            var fs = this.Field.FieldSetting as IntegerFieldSetting;

            if (fs == null)
                return string.Empty;

            if (fs.ShowAsPercentage.HasValue && fs.ShowAsPercentage.Value)
                return "%";

            return string.Empty;
        }

        #region ITemplateFieldControl Members

        public Control GetInnerControl()
        {
            return this.FindControlRecursive(InnerControlID) as TextBox;
        }

        public Control GetLabelForDescription()
        {
            return this.FindControlRecursive(DescriptionControlID) as Label;
        }

        public Control GetLabelForTitleControl()
        {
            return this.FindControlRecursive(TitleControlID) as Label;
        }

        #endregion

        public override object Data
        {
            get
            {
                return _inputTextBox.Text;
            }
        }

        public Label GetLabelForPercentageControl()
        {
            return this.FindControlRecursive(PercentageControlID) as Label;
        }
    }
}