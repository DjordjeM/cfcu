﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SenseNet.ContentRepository;
using SenseNet.ContentRepository.Storage;
using SenseNet.ContentRepository.Schema;
using SenseNet.Search;

namespace CFCU.Code
{
    [ContentHandler]
    public class TransmissionNote : Folder
    {
        //============================================================== Constructors

        public TransmissionNote(Node parent) : this(parent, null) { }
        public TransmissionNote(Node parent, string nodeTypeName) : base(parent, nodeTypeName) { }
        protected TransmissionNote(NodeToken nt) : base(nt) { }

        //============================================================== Overrides

        public override void Save(NodeSaveSettings settings)
        {
            if (TNCheckDuplicate())
                base.Save();
            else
                throw new InvalidOperationException("Transmission note cannot be saved because it is a duplicate.");
        }
        protected bool TNCheckDuplicate(){
            
            var parentTN = base.ParentPath;
            var notes = ContentQuery.Query(MySafeQueries.InFolderTransmissionNote, null, parentTN, "TransmissionNote", GetProperty("DocumentType"), GetProperty("SubmittedVersion"));
            

            if (notes.Count < 1)
                return true; //no duplicates
            else
                return false;
        }
    }
}