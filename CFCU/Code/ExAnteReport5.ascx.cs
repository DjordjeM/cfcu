﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CFCU.Code
{
    public partial class ExAnteReport1 : System.Web.UI.UserControl
    {
        //public string DocumentType {get; set;}
        //public string DocumentOrder {get; set;}
        //public string DocumentVersion { get; set; }
        //public string Q1 {get; set;}   
        //public string Q2 {get; set;}   
        //public string Q3 {get; set;}
    }
    public class PivotedCrew
    {
        public string DocumentType { get; set; }
        public int DocumentOrder { get; set; }
        public string DocumentVersion { get; set; }
        public int Q1A { get; set; }
        public int Q1R { get; set; }
        public int Q2A { get; set; }
        public int Q2R { get; set; }
        public int Q3A { get; set; }
        public int Q3R { get; set; }
        public int Q4A { get; set; }
        public int Q4R { get; set; }
    }
    public class Rows
    {
        string doctype;
        int order;
        string version;
        int approved;
        int approvedcomments;
        int rejected;
    }

}