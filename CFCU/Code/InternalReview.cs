﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SenseNet.ContentRepository;
using SenseNet.ContentRepository.Storage;
using SenseNet.ContentRepository.Schema;
using SenseNet.Search;

namespace CFCU.Code
{
    [ContentHandler]
    public class InternalReview : GenericContent
    {
        //============================================================== Constructors

        public InternalReview(Node parent) : this(parent, null) { }
        public InternalReview(Node parent, string nodeTypeName) : base(parent, nodeTypeName) { }
        protected InternalReview(NodeToken nt) : base(nt) { }

        //============================================================== Overrides
        
        public override void Save(NodeSaveSettings settings)
        {
            if (IRCheckDuplicate())
                base.Save();
            else
                throw new InvalidOperationException("Internal review cannot be saved because it is a duplicate.");
        }

        protected bool IRCheckDuplicate()
        {

            var parentIR = base.ParentPath;
            var reviews = ContentQuery.Query(MySafeQueries.InFolderInternalReview, null, parentIR, base.ContentType.Name, GetProperty("DocumentType"), GetProperty("DocumentVersion"));

            if (reviews.Count < 1)
                return true; //no duplicates
            else
                return false;
        }
    }
}