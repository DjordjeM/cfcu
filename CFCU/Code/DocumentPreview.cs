﻿using SenseNet.Preview;
using SenseNet.ContentRepository.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using SenseNet.ContentRepository.Storage.Security;
using SenseNet.ContentRepository.Storage.Schema;
using SenseNet.ContentRepository;

namespace CFCU.Code
{
    public class CFCUPreviewProvider : DocumentPreviewProvider
    {

        public override string GetPreviewGeneratorTaskName(string contentPath)
        {
            //throw new NotImplementedException();
            return "SenseNetTaskAgent"; //CFCUPreviewImageGenerator

        }

        public override bool IsContentSupported(Node content)
        {
            //throw new NotImplementedException();
            return true;
        }

        public override string[] GetSupportedTaskNames()
        {
            var tasks = new string[1];
            tasks[0] = "CFCUPreviewImageGenerator";

            return tasks;
        }


    }

    //public class MyPreviewImageGenerator : PreviewImageGenerator
    //{
    //    private static readonly string MyExt1 = ".pdf";
    //    private static readonly string MyExt2 = ".doc";
    //    private static readonly string MyTaskExecutorName = "SenseNetTaskAgent";//

    //    public override string[] KnownExtensions { get { return new[] { MyExt1, MyExt2 }; } }

    //    public override string GetTaskNameByExtension(string extension)
    //    {
    //        if (extension.Equals(MyExt1, StringComparison.InvariantCultureIgnoreCase))
    //            return MyTaskExecutorName;
    //        else
    //            return base.GetTaskNameByExtension(extension);
    //    }

    //    public override void GeneratePreview(Stream docStream, IPreviewGenerationContext context)
    //    {
    //        //image generation logic that uses the context to save images
            
    //    }
    //}
}