﻿using System;
using SenseNet.ContentRepository;
using SenseNet.ContentRepository.Storage;
using SenseNet.ContentRepository.Schema;
using SenseNet.Search;

namespace CFCU.Code
{
    [ContentHandler]
    public class Verso : Folder
    {
        //============================================================== Constructors

        public Verso(Node parent) : this(parent, null) { }
        public Verso(Node parent, string nodeTypeName) : base(parent, nodeTypeName) { }
        protected Verso(NodeToken nt) : base(nt) { }

        //============================================================== Overrides

        public override void Save(NodeSaveSettings settings)
        {
            if (VersoCheckDuplicate())
                base.Save();
            else
                throw new InvalidOperationException("Verso cannot be saved because it is duplicate.");
        }
        private bool VersoCheckDuplicate(){
            
            var parentTN = base.ParentPath;
            var versos = ContentQuery.Query(MySafeQueries.InFolderAndSomeType,null,parentTN,"Verso");
            

            if (versos.Count < 1)
                return true; //no duplicates
            else
                return false;
        }
    }
    public class MySafeQueries : ISafeQueryHolder
    {
        //public static string AllDevices { get { return "+InTree:/Root/System/Devices +TypeIs:Device .AUTOFILTERS:OFF"; } }
        public static string InFolderAndSomeType { get { return "+InFolder:@0 +TypeIs:(@1)"; } }
        public static string InFolderTransmissionNote { get { return "+InFolder:@0 +TypeIs:(@1) +DocumentType:@2 +SubmittedVersion:@3"; } }
        public static string InFolderInternalReview { get { return "+InFolder:@0 +TypeIs:(@1) +DocumentType:@2 +DocumentVersion:@3"; } }
    }
} 