﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using SenseNet.ContentRepository.Fields;
using System.Data;
using OfficeOpenXml;

namespace CFCU.Code
{

    public partial class ExAnteReport1 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void ExportExcel(DataTable ds)
        {
            Response.Clear();


            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report1");

                ws.Cells["A1"].LoadFromDataTable(ds, true);
               

                pck.SaveAs(Response.OutputStream);
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=Sample1.xlsx");

                //Response.Clear();
                //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Response.AddHeader("content-disposition", "attachment;  filename=Rpt1.xlsx");

                //Response.BinaryWrite(pck.GetAsByteArray());
                //HttpContext.Current.Response.EndFlush();

            }
            Response.End();
        }
    }
}