﻿using System;
using SenseNet.ContentRepository;
using SenseNet.ContentRepository.Storage;
using SenseNet.ContentRepository.Schema;
using SenseNet.Search;

namespace CFCU.Code
{
    [ContentHandler]
    public class ExAnte : Folder
    {
        //============================================================== Constructors

        public ExAnte(Node parent) : this(parent, null) { }
        public ExAnte(Node parent, string nodeTypeName) : base(parent, nodeTypeName) { }
        protected ExAnte(NodeToken nt) : base(nt) { }

        //============================================================== Overrides
    }
        
}