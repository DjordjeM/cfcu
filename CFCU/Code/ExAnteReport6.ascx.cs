﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;

//TBD: Needs refactoring

namespace CFCU.Code
{
    public static class LinqPivotHelper
    {
        public static DataTable ToPivotTable<T, TColumn, TRow, TData>( //generic extension method
        this IEnumerable<T> source,
        Func<T, TColumn> columnSelector,
        Expression<Func<T, TRow>> rowSelector,
        Func<IEnumerable<T>, TData> dataSelector)
        {
            DataTable table = new DataTable();
            var rowName = ((MemberExpression)rowSelector.Body).Member.Name;
            table.Columns.Add(new DataColumn(rowName));
            var columns = source.Select(columnSelector).Distinct();

            foreach (var column in columns)
                table.Columns.Add(new DataColumn(column.ToString()));

            var rows = source.GroupBy(rowSelector.Compile())
                             .Select(rowGroup => new
                             {
                                 Key = rowGroup.Key,
                                 Values = columns.GroupJoin(
                                     rowGroup,
                                     c => c,
                                     r => columnSelector(r),
                                     (c, columnGroup) => dataSelector(columnGroup))
                             });

            foreach (var row in rows)
            {
                var dataRow = table.NewRow();
                var items = row.Values.Cast<object>().ToList();
                items.Insert(0, row.Key);
                dataRow.ItemArray = items.ToArray();
                table.Rows.Add(dataRow);
            }

            return table;
        }
        
        public static DataTable ToPivotTable<T, TColumn, TRow, TData>( //extension method overload specific for report 6
        this IEnumerable<T> source,
        Func<T, TColumn> columnSelector,
        Expression<Func<T, TRow>> rowSelector,
        Func<IEnumerable<T>, TData> dataSelector,
        Func<IEnumerable<T>, TData> dataSelectorSuspended,
        Func<IEnumerable<T>, TData> dataSelectorRejected,
        Func<IEnumerable<T>, TData> dataSelectorTotal,
            int version)
        {
            DataTable table = new DataTable();
            var rowName = ((MemberExpression)rowSelector.Body).Member.Name;
            table.Columns.Add(new DataColumn(rowName));
            var columns = source.Select(columnSelector).Distinct();

            foreach (var column in columns) //add suffix to column names
                for (int n = 1; n <= 4; n++ )
                    table.Columns.Add(new DataColumn(column.ToString() + " " + n.ToString(),typeof(double)));

            //add version column
            table.Columns.Add("version");


            var rows = source.GroupBy(rowSelector.Compile())
                             .Select(rowGroup => new
                             {
                                 Key = rowGroup.Key,
                                 Values = columns.GroupJoin(
                                     rowGroup,
                                     c => c,
                                     r => columnSelector(r),
                                     (c, columnGroup) => new
                                     {
                                         approved=dataSelector(columnGroup),
                                         suspended=dataSelectorSuspended(columnGroup),
                                         rejected=dataSelectorRejected(columnGroup),
                                         total=dataSelectorTotal(columnGroup)
                                     })
                             }

                             );
            

            List<string> items2 = new List<string>(); // list of actual values
            List<string> items2percent = new List<string>(); // list of decimal percentages
           

            foreach (var row in rows)
            {
                var dataRow = table.NewRow();
                var dataRow2 = table.NewRow();

                var items = row.Values.Cast<Object>();
                
                foreach (var item in items)
                {
                    string itemstring = item.ToString().Replace("{","").Replace("}","");

                    var itemsdic = itemstring.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                    .Select(part => part.Split('='))
                                    .ToDictionary(split => split[0].Trim(), split => split[1].Trim());

                    
                    if (itemsdic["total"] != "0")
                    {
                        foreach (var i in itemsdic)
                        {
                            items2.Add(i.Value);
                            double tempval = Convert.ToDouble(i.Value)/Convert.ToDouble(itemsdic["total"]);
                            items2percent.Add(tempval.ToString());
                        }
                    }
                    else{
                        foreach (var i in itemsdic)
                        {
                            items2.Add(i.Value);
                            items2percent.Add("0");
                        }

                }
                    itemsdic.Clear();                    
           }

                items2.Insert(0, row.Key.ToString());

                items2percent.Insert(0, "");

                dataRow.ItemArray = items2.ToArray();
                dataRow2.ItemArray = items2percent.ToArray();

                table.Rows.Add(dataRow);
                table.Rows.Add(dataRow2);
                items2.Clear();
                items2percent.Clear();

                //add values to version column

                int lastColumnIndex = table.Columns.Count - 1;

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    table.Rows[i][lastColumnIndex] = version;
                }
            }

            return table;
        }
        public static DataTable ToPivotTable<T, TColumn, TRow, TData>( //extension method overload specific for report 4
        this IEnumerable<T> source,
        Func<T, TColumn> columnSelector,
        Expression<Func<T, TRow>> rowSelector,
        Func<IEnumerable<T>, TData> dataSelectorRejected,
        Func<IEnumerable<T>, TData> dataSelectorTotal,
            string procurementType="None")
        {
            DataTable table = new DataTable();
            var rowName = ((MemberExpression)rowSelector.Body).Member.Name;
            table.Columns.Add(new DataColumn(rowName));
            var columns = source.Select(columnSelector).Distinct();

            foreach (var column in columns)
            {//add suffix to column names
                //for (int n = 1; n <=2; n++)
                table.Columns.Add(new DataColumn("Q" + column.ToString() + "S", typeof(double))); //Submitted
                table.Columns.Add(new DataColumn("Q" + column.ToString() + "RR", typeof(double)));//RejectionRate
            }
            
            if (procurementType != "None")
                //add version column
                table.Columns.Add("ProcurementType");

            var rows = source.GroupBy(rowSelector.Compile())
                             .Select(rowGroup => new
                             {
                                 Key = rowGroup.Key,
                                 Values = columns.GroupJoin(
                                     rowGroup,
                                     c => c,
                                     r => columnSelector(r),
                                     (c, columnGroup) => new
                                     {
                                         rejected = dataSelectorRejected(columnGroup),
                                         total = dataSelectorTotal(columnGroup)
                                     })
                             }

                             );

            List<string> items2 = new List<string>(); // list of actual values

            foreach (var row in rows)
            {
                var dataRow = table.NewRow();

                var items = row.Values.Cast<Object>();

                foreach (var item in items)
                {
                    string itemstring = item.ToString().Replace("{", "").Replace("}", "");

                    var itemsdic = itemstring.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                    .Select(part => part.Split('='))
                                    .ToDictionary(split => split[0].Trim(), split => split[1].Trim());

                    if (itemsdic["total"] != "0")
                    {
                        double tempval = Convert.ToDouble(itemsdic["rejected"]) / Convert.ToDouble(itemsdic["total"]);
                        items2.Add(itemsdic["total"]);
                        items2.Add(tempval.ToString());
                    }
                    else
                    {
                        foreach (var i in itemsdic)
                        {
                            items2.Add("0");
                        }

                    }
                    itemsdic.Clear();
                }

                //if values are missing add default

                int maxcellcount = 8;
                int currentcellcount = items2.Count;
                int missingcells = maxcellcount - currentcellcount;
                if (missingcells >= 1)
                {
                    for (int blanks = 1; blanks <= missingcells; blanks++)
                        items2.Add("0");
                }
                
                items2.Insert(0, row.Key.ToString());

                dataRow.ItemArray = items2.ToArray();

                table.Rows.Add(dataRow);
                items2.Clear();

                if (procurementType != "None")
                { 
                    //add values to ProcurementType column

                    int lastColumnIndex = table.Columns.Count - 1;

                    for (int i = 0; i < table.Rows.Count; i++)
                    {
                        table.Rows[i][lastColumnIndex] = procurementType;
                    }
                }
            }

            return table;
        }
        public static string ConvertDataTableToHtmlTable(this DataTable targetTable)
        {
            string htmlString = "";

            if (targetTable == null)
            {
                throw new System.ArgumentNullException("targetTable");
            }

            StringBuilder htmlBuilder = new StringBuilder();

            //Create Top Portion of HTML Table
            //htmlBuilder.Append("<html>");
            //htmlBuilder.Append("<head>");
            //htmlBuilder.Append("<title>");
            //htmlBuilder.Append("Page-");
            //htmlBuilder.Append(Guid.NewGuid().ToString());
            //htmlBuilder.Append("</title>");
            //htmlBuilder.Append("</head>");
            //htmlBuilder.Append("<body>");
            htmlBuilder.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            //htmlBuilder.Append("style='border: solid 1px Black; font-size: small;'>");

            //Create Header Row
            htmlBuilder.Append("<tr align='left' valign='top'>");

            foreach (DataColumn targetColumn in targetTable.Columns)
            {

                htmlBuilder.Append("<td align='left' valign='top'>");
                htmlBuilder.Append(targetColumn.ColumnName);
                htmlBuilder.Append("</td>");
            }

            htmlBuilder.Append("</tr>");

            //Create Data Rows
            foreach (DataRow myRow in targetTable.Rows)
            {
                htmlBuilder.Append("<tr align='left' valign='top'>");

                foreach (DataColumn targetColumn in targetTable.Columns)
                {
                    htmlBuilder.Append("<td align='left' valign='top'>");
                    htmlBuilder.Append(myRow[targetColumn.ColumnName].ToString());
                    htmlBuilder.Append("</td>");
                }

                htmlBuilder.Append("</tr>");
            }

            //Create Bottom Portion of HTML Document
            htmlBuilder.Append("</table>");
            //htmlBuilder.Append("</body>");
           // htmlBuilder.Append("</html>");

            //Create String to be Returned
            htmlString = htmlBuilder.ToString();

            return htmlString;
        }
        public static string ConvertDataTableToHtmlTable(this DataTable targetTable, int quarter, string tablestyle = "", string tableid = "grid6")
        {
            string htmlString = "";
            List<string> columnNames = new List<string> ();
            
            int columnNumber = (targetTable.Columns.Count - 2) / 4;

            for (int n = 1; n < targetTable.Columns.Count-1 ; n += 4)
            {
                var columnProper = targetTable.Columns[n].ColumnName.Split(' ');
                columnNames.Add(columnProper[0]);
            }

                if (targetTable == null)
                {
                    throw new System.ArgumentNullException("targetTable");
                }

            StringBuilder htmlBuilder = new StringBuilder();

            htmlBuilder.Append(String.Format("<table id='{0}'class='{1}'>",tableid,tablestyle));
            htmlBuilder.Append("<thead>");

            //Create Header Row(s)

            htmlBuilder.Append("<tr>");
            //htmlBuilder.Append(String.Format("<th rowspan='2'>{0} submission for EUD ex-ante control</th>", quarter.ToString() + quarter.GetOrdinalSuffix()));
            htmlBuilder.Append("<th rowspan='2'>Procurement Type<br/>Document Type</th>");
            
            //create each heading per procurement type
            foreach (var col in columnNames)
                htmlBuilder.Append(String.Format("<th colspan='4' style='text-align:center'>{0}</th>", col));
            htmlBuilder.Append("<th rowspan='2'>version</th>");
            htmlBuilder.Append("</tr>");

            htmlBuilder.Append("<tr>");
            foreach(var col in columnNames)
            { 
                htmlBuilder.Append("<th>Approved</th>");
                htmlBuilder.Append("<th>Suspended</th>");
                htmlBuilder.Append("<th>Rejected</th>");
                htmlBuilder.Append("<th>Total</th>");
            }

            htmlBuilder.Append("</tr>");
            //htmlBuilder.Append("<th rowspan='2'>version</th>");
            htmlBuilder.Append("</thead>");

            //Create Data Rows
            htmlBuilder.Append("<tbody>");
            var rowcounter = 1;
            foreach (DataRow myRow in targetTable.Rows)
            {
                htmlBuilder.Append("<tr align='left'>");

                

                    foreach (DataColumn targetColumn in targetTable.Columns)
                {
                    htmlBuilder.Append("<td align='center'>");
                    if (targetColumn.ColumnName == "version")
                    {
                        int versionnumber = Convert.ToInt16(myRow[targetColumn.ColumnName].ToString());
                        htmlBuilder.Append(versionnumber.ToString() + String.Format("{0} submission for EUD ex-ante control", versionnumber.GetOrdinalSuffix()));
                        rowcounter++;
                    }
                        
                    else
                    {
                        int rowindex = targetTable.Rows.IndexOf(myRow) + 1;
                        if ( rowindex % 2 == 0) //percentage row
                        {
                            string formated = String.Format("{0:P2}", myRow[targetColumn.ColumnName]);
                            htmlBuilder.Append(formated);
                        }
                        else
                        htmlBuilder.Append(myRow[targetColumn.ColumnName].ToString());
                    }
                    
                    htmlBuilder.Append("</td>");
                    
                }
                
                htmlBuilder.Append("</tr>");
            }

            htmlBuilder.Append("</tbody>");

            //Create Bottom Portion of HTML Document
            htmlBuilder.Append("</table>");

            //Create String to be Returned
            htmlString = htmlBuilder.ToString();

            return htmlString;
        }
        public static string ConvertDataTableToHtmlTable(this DataTable targetTable, string procurementType, string tablestyle = "", string tableid = "grid4")
        {//rpt4
            string htmlString = "";
            List<string> columnNames = new List<string>();

            int columnNumber = (targetTable.Columns.Count - 2) / 4;

            for (int n = 1; n <=4; n++)
            {
                //var columnProper = targetTable.Columns[n].ColumnName.Split(' ');
                columnNames.Add(String.Format("Q{0}",n.ToString()));
            }

            if (targetTable == null)
            {
                throw new System.ArgumentNullException("targetTable");
            }

            StringBuilder htmlBuilder = new StringBuilder();

            htmlBuilder.Append(String.Format("<table id='{0}'class='{1}'>", tableid, tablestyle));
            htmlBuilder.Append("<thead>");

            //Create Header Row(s)

            htmlBuilder.Append("<tr>");
            htmlBuilder.Append("<th rowspan='2'>Type of Document</th>");

            //create each heading per document type
            foreach (var col in columnNames)
                htmlBuilder.Append(String.Format("<th colspan='2' style='text-align:center'>{0}</th>", col));
            htmlBuilder.Append("<th rowspan='2'>version</th>");
            htmlBuilder.Append("</tr>");

            htmlBuilder.Append("<tr>");

            foreach (var col in columnNames)
            {
                htmlBuilder.Append("<th>Sub.</th>");
                htmlBuilder.Append("<th>Rate</th>");
            }

            htmlBuilder.Append("</tr>");
            htmlBuilder.Append("</thead>");

            //Create Data Rows
            htmlBuilder.Append("<tbody>");
            
            foreach (DataRow myRow in targetTable.Rows)
            {
                htmlBuilder.Append("<tr align='left'>");

                foreach (DataColumn targetColumn in targetTable.Columns)
                {
                    htmlBuilder.Append("<td align='center'>");
                    if (targetColumn.ColumnName == "ProcurementType")
                    {
                        htmlBuilder.Append(myRow[targetColumn.ColumnName].ToString());
                    }

                    else
                    {
                        if (targetColumn.ColumnName.Contains("RR")) //rejection rate column
                        {
                            string formated = String.Format("{0:P2}", myRow[targetColumn.ColumnName]);
                            htmlBuilder.Append(formated);
                        }
                        else
                            htmlBuilder.Append(myRow[targetColumn.ColumnName].ToString());
                    }

                    htmlBuilder.Append("</td>");

                }

                htmlBuilder.Append("</tr>");
            }

            htmlBuilder.Append("</tbody>");

            //Create Bottom Portion of HTML Document
            htmlBuilder.Append("</table>");

            //Create String to be Returned
            htmlString = htmlBuilder.ToString();

            return htmlString;
        }
        public static string ConvertDataTableToHtmlTable(this DataTable targetTable, string tablestyle = "", string tableid = "grid4")
        {//rpt2
            string htmlString = "";
            List<string> columnNames = new List<string>();

            int columnNumber = (targetTable.Columns.Count - 2) / 4;

            for (int n = 1; n <= 4; n++)
            {
                //var columnProper = targetTable.Columns[n].ColumnName.Split(' ');
                columnNames.Add(String.Format("Q{0}", n.ToString()));
            }

            if (targetTable == null)
            {
                throw new System.ArgumentNullException("targetTable");
            }

            StringBuilder htmlBuilder = new StringBuilder();

            htmlBuilder.Append(String.Format("<table id='{0}'class='{1}'>", tableid, tablestyle));
            htmlBuilder.Append("<thead>");

            //Create Header Row(s)

            htmlBuilder.Append("<tr>");
            htmlBuilder.Append("<th rowspan='2'>Program</th>");

            //create each heading per document type
            foreach (var col in columnNames)
                htmlBuilder.Append(String.Format("<th colspan='2' style='text-align:center'>{0}</th>", col));
            //htmlBuilder.Append("<th rowspan='2'>version</th>");
            htmlBuilder.Append("</tr>");

            htmlBuilder.Append("<tr>");

            foreach (var col in columnNames)
            {
                htmlBuilder.Append("<th>Sub.</th>");
                htmlBuilder.Append("<th>Rate</th>");
            }

            htmlBuilder.Append("</tr>");
            htmlBuilder.Append("</thead>");

            //Create Data Rows
            htmlBuilder.Append("<tbody>");

            foreach (DataRow myRow in targetTable.Rows)
            {
                htmlBuilder.Append("<tr align='left'>");

                foreach (DataColumn targetColumn in targetTable.Columns)
                {
                    htmlBuilder.Append("<td align='center'>");
                    if (targetColumn.ColumnName == "ProcurementType")
                    {
                        htmlBuilder.Append(myRow[targetColumn.ColumnName].ToString());
                    }

                    else
                    {
                        if (targetColumn.ColumnName.Contains("RR")) //rejection rate column
                        {
                            string formated = String.Format("{0:P2}", myRow[targetColumn.ColumnName]);
                            htmlBuilder.Append(formated);
                        }
                        else
                            htmlBuilder.Append(myRow[targetColumn.ColumnName].ToString());
                    }

                    htmlBuilder.Append("</td>");

                }

                htmlBuilder.Append("</tr>");
            }

            htmlBuilder.Append("</tbody>");

            //Create Bottom Portion of HTML Document
            htmlBuilder.Append("</table>");

            //Create String to be Returned
            htmlString = htmlBuilder.ToString();

            return htmlString;
        }
        private static string GetOrdinalSuffix(this int num)
        {
            if (num.ToString().EndsWith("11")) return "th";
            if (num.ToString().EndsWith("12")) return "th";
            if (num.ToString().EndsWith("13")) return "th";
            if (num.ToString().EndsWith("1")) return "st";
            if (num.ToString().EndsWith("2")) return "nd";
            if (num.ToString().EndsWith("3")) return "rd";
            return "th";
        }
    }
}