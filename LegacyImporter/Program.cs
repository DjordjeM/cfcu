﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SenseNet.ContentRepository;
using SenseNet.ContentRepository.Storage;
using SenseNet.Search;
using CsvHelper;
using System.IO;
using System.Web;
using System.Diagnostics;

namespace LegacyImporter
{
    class DataRecord
    {
        //Should have properties which correspond to the Column Names in the file
        //i.e. ContractType,ProjectNumber,ProjectNumber,Sector
        public String ContractType { get; set; }
        public String ProjectNumber { get; set; }
        public String Sector { get; set; }
        public String ContractTitle { get; set; }
        

    }
    class DataRecordTN
    {
        //Should have properties which correspond to the Column Names in the file
        //i.e. ContractType,ProjectNumber,ProjectNumber,Sector
        
        public String ProjectNumber { get; set; }
        public String DocumentNature { get; set; }
        public String SubmissionDate { get; set; }
        public String DecisionDate { get; set; }
        public char Approved { get; set; }
        public char Suspended { get; set; }
        public char Rejected { get; set; }
        public string Reason { get; set; }
        public string Comments { get; set; }
        public string Version { get; set; }
    }
    
    class DataRecordIR
    {
        //Should have properties which correspond to the Column Names in the file
        //i.e. ContractType,ProjectNumber,ProjectNumber,Sector

        public String ProjectNumber { get; set; }
        public String DocumentNature { get; set; }
        public string RecommendationStatus { get; set; }
        public string Version { get; set; }
        public string QCOfficer { get; set; }
        public string Supervision { get; set; }
        public String RecommendationDate { get; set; }
        
    }
    class DataRecordPR
    {
        public string ProjectNumber { get; set; }
        public string ContractType { get; set; }
        public string ContractTitle { get; set; }
    }

    public class MySafeQuery : ISafeQueryHolder
    {
        //public static string AllDevices { get { return "+InTree:/Root/System/Devices +TypeIs:Device .AUTOFILTERS:OFF"; } }
        public static string InFolderAndSomeName { get { return "+InTree:@0 +DisplayName:@1"; } }
    }

    class DataRecordExtended : DataRecord
    {
        public String ProjectType
        {
            get
            {
                return ProjectType;
            }
            set
            {
                switch (ContractType)
                {
                    case "Service":
                        ProjectType = "IPAProjectServices";
                        break;

                    case "Supply":
                        ProjectType = "IPAProjectSupplies";
                        break;

                    case "Work":
                        ProjectType = "IPAProjectWorks";
                        break;

                    case "Grant":
                        ProjectType = "IPAProjectGrants";
                        break;

                    case "Twinning":
                        ProjectType = "IPAProjectTwinnings";
                        break;

                    case "Framework":
                        ProjectType = "IPAProjectFramework";
                        break;
                }
            }

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
           //Check arguments
            //args[0] = "project_master_data_all_ready.csv";//"ex-ante_db_all.csv";
            //args[1] = "ws";

            if (args.Length == 0 || System.IO.File.Exists(args[0]) == false)
            {
                Console.WriteLine("No arguments supplied or ");
                Console.WriteLine ("File" + args[0] + " does not exist.");
                Environment.Exit(-1);
            }

            switch (args[1])
            {
                case "ws":
                    break;
                case "pr":
                    break;
                case "tn":
                    break;
                case "ir":
                    break;
                case "ex":
                    break;
                default:
                    Console.WriteLine("Content not specified, exiting...");
                    return;
            }

            var startSettings = new RepositoryStartSettings
            {
                Console = Console.Out,
                StartLuceneManager = StorageContext.Search.IsOuterEngineEnabled,
                PluginsPath = AppDomain.CurrentDomain.BaseDirectory,
                  StartWorkflowEngine = true
            };

            using (Repository.Start(startSettings))
            {
                
                using (var sr = new StreamReader(@args[0])) //@"ProjectMasterData_slice5.csv"
                {

                    var reader = new CsvReader(sr);
                    String ProjectType = "";
                    String Folder = "";
                    String targetpath = "/Root/Sites/Default_Site/workspaces2/IPA2013";
                    String templatepath="/Root/ContentTemplates";
                   
                    //CSVReader will now read the whole file into an enumerable
                    

                    switch (args[1])
                    {
                        case "ws":
                            //break;

                            IEnumerable<DataRecord> records = reader.GetRecords<DataRecord>();
                        
                            //First 5 records in CSV file will be printed to the Output Window
                            foreach (DataRecord record in records.Take(100))
                            {
                                switch (record.ContractType)
                                {
                                    case "Service":
                                        ProjectType = "IPAProjectServices";
                                        Folder = "/Services Workspaces/";
                                        break;

                                    case "Supply":
                                        ProjectType = "IPAProjectSupplies";
                                        Folder = "/Supplies Workspaces/";
                                        break;

                                    case "Work":
                                        ProjectType = "IPAProjectWorks";
                                        Folder = "/Works Workspaces/";
                                        break;

                                    case "Grant":
                                        ProjectType = "IPAProjectGrants";
                                        Folder = "/Grants Workspaces/";
                                        break;

                                    case "Twinning":
                                        ProjectType = "IPAProjectTwinnings";
                                        Folder = "/Twinnings Workspaces/";
                                        break;

                                    case "Framework":
                                        ProjectType = "IPAProjectFramework";
                                        Folder = "/Framework Workspaces/";
                                        break;
                                }

                                //Debug.Print("{0} {1}, {2}, {3}", record.ContractType, record.ProjectNumber, record.Sector, record.ContractTitle);
                                String wstargetpath = targetpath + Folder + HttpUtility.UrlEncode(record.ProjectNumber.Trim());
                                String wstemplatepath = templatepath + "/" + ProjectType + "/" + ProjectType;
                                String listtemplatepath = templatepath + "/InternalReviewList/InternalReviewList";
                                String tnlisttemplatepath = templatepath + "/TransmissionNoteList/TransmissionNoteList";

                                Dictionary<string, object> properties = new Dictionary<string, object>();
                                properties.Add("DisplayName", record.ProjectNumber.Trim());
                                properties.Add("ReferenceNumber", record.ProjectNumber.Trim());
                                properties.Add("Priority", record.Sector);

                                var workspace = CreateWorkspace(wstargetpath.Trim(),wstemplatepath,properties);
                                properties.Clear();

                                //CHANGED: Adding of content list handled by content template(s)

                                //string listtargetpath = workspace.Path + "/InternalReviewList";

                                //properties.Add("DisplayName", "Internal Reviews");
                                //var internalreviewlist = CreateWorkspace(listtargetpath, listtemplatepath, properties);
                                //properties.Clear();

                                //properties.Add("DisplayName", "Transmission Notes");
                                //string tnlistpath = workspace.Path + "/TransmissionNoteList";
                                //var transmissionnotelist = CreateWorkspace(tnlistpath, tnlisttemplatepath, properties);
                                //properties.Clear();
                            }
                            break;
                        case "pr":
                            {

                                //match project 
                                IEnumerable<DataRecordPR> recordsPR = reader.GetRecords<DataRecordPR>();

                                foreach (DataRecordPR recordPR in recordsPR.Take(10))
                                {
                                    switch (recordPR.ContractType)
                                    {
                                        case "Service":
                                            ProjectType = "IPAProjectServices";
                                            Folder = "/Services Workspaces/";
                                            break;

                                        case "Supply":
                                            ProjectType = "IPAProjectSupplies";
                                            Folder = "/Supplies Workspaces/";
                                            break;

                                        case "Work":
                                            ProjectType = "IPAProjectWorks";
                                            Folder = "/Works Workspaces/";
                                            break;

                                        case "Grant":
                                            ProjectType = "IPAProjectGrants";
                                            Folder = "/Grants Workspaces/";
                                            break;

                                        case "Twinning":
                                            ProjectType = "IPAProjectTwinnings";
                                            Folder = "/Twinnings Workspaces/";
                                            break;

                                        case "Framework":
                                            ProjectType = "IPAProjectFramework";
                                            Folder = "/Framework Workspaces/";
                                            break;
                                    }
                                    //check if parent node exists, if not skip to next
                                    var pnumber = HttpUtility.UrlEncode(recordPR.ProjectNumber.Trim());
                                    var pnumber_sanitized = pnumber.Replace("%2f", "_2f");
                                    var parentpath = targetpath;// +Folder;// +"/" + pnumber_sanitized;

                                    var projectexists = ContentQuery.Query(MySafeQuery.InFolderAndSomeName, null, parentpath, recordPR.ProjectNumber.Trim());

                                    if ((projectexists.Count >= 1))
                                        //Load Node
                                    {
                                        var projectpath = projectexists.Nodes.First().Path;
                                        var projectnode = Node.LoadNode(projectpath);

                                        //Update node
                                        projectnode["Description"] = recordPR.ContractTitle;
                                        projectnode.Save();
                                        Console.WriteLine("Description for project " + recordPR.ProjectNumber + " updated");
                                    }
                                    else
                                    {
                                        //Create project
                                        Console.WriteLine("Project " + recordPR.ProjectNumber + ": '" + recordPR.ContractTitle + "' does not exist.");
                                        continue;
                                    }
                                }
                                break;
                            }
                        case "desc":
                            {

                                //match project 
                                IEnumerable<DataRecordPR> recordsPR = reader.GetRecords<DataRecordPR>();

                                foreach (DataRecordPR recordPR in recordsPR.Take(10))
                                {
                                    switch (recordPR.ContractType)
                                    {
                                        case "Service":
                                            ProjectType = "IPAProjectServices";
                                            Folder = "/Services Workspaces/";
                                            break;

                                        case "Supply":
                                            ProjectType = "IPAProjectSupplies";
                                            Folder = "/Supplies Workspaces/";
                                            break;

                                        case "Work":
                                            ProjectType = "IPAProjectWorks";
                                            Folder = "/Works Workspaces/";
                                            break;

                                        case "Grant":
                                            ProjectType = "IPAProjectGrants";
                                            Folder = "/Grants Workspaces/";
                                            break;

                                        case "Twinning":
                                            ProjectType = "IPAProjectTwinnings";
                                            Folder = "/Twinnings Workspaces/";
                                            break;

                                        case "Framework":
                                            ProjectType = "IPAProjectFramework";
                                            Folder = "/Framework Workspaces/";
                                            break;
                                    }
                                    //check if parent node exists, if not skip to next
                                    var pnumber = HttpUtility.UrlEncode(recordPR.ProjectNumber.Trim());
                                    var pnumber_sanitized = pnumber.Replace("%2f", "_2f");
                                    var parentpath = targetpath;// +Folder;// +"/" + pnumber_sanitized;

                                    var projectexists = ContentQuery.Query(MySafeQuery.InFolderAndSomeName, null, parentpath, recordPR.ProjectNumber.Trim());

                                    if ((projectexists.Count >= 1))
                                    //Load Node
                                    {
                                        var projectpath = projectexists.Nodes.First().Path;
                                        var projectnode = Node.LoadNode(projectpath);

                                        //Update node
                                        projectnode["Description"] = recordPR.ContractTitle;
                                        projectnode.Save();
                                        Console.WriteLine("Description for project " + recordPR.ProjectNumber + " updated");
                                    }
                                    else
                                    {
                                        //Create project
                                        Console.WriteLine("Project " + recordPR.ProjectNumber + ": '" + recordPR.ContractTitle + "' does not exist.");
                                        continue;
                                    }
                                }
                                break;
                            }
                        case "ex": //TransmissionNote & Verso combined

                            IEnumerable<DataRecordTN> recordsEX = reader.GetRecords<DataRecordTN>();

                            foreach (DataRecordTN recordEX in recordsEX.Take(1000))
                            { 
                                //check if parent node exists, if not skip to next
                                var pnumber = HttpUtility.UrlEncode(recordEX.ProjectNumber.Trim());
                                var pnumber_sanitized = pnumber.Replace("%2f", "_2f");
                                var parentpath = targetpath + "/" + pnumber_sanitized;

                                var projectexists = ContentQuery.Query(MySafeQuery.InFolderAndSomeName,null, targetpath, recordEX.ProjectNumber);

                                if ((projectexists.Count >= 1))
                                {
                                    
                                    //check if ExAnteReviewList node exist
                                    var tnlist = projectexists.Nodes.First().Path + "/" + "ExAnteReviewList";
                                    
                                    if (Node.Exists(tnlist) == false)
                                    {
                                        //create ExAnteReviewList
                                        Dictionary<string, object> properties = new Dictionary<string, object>();
                                        var tnlisttemplatepath = templatepath + "/ExAnteReviewList/ExAnteReviewList";

                                        properties.Add("DisplayName", "Ex-Ante");
                                        //string tnlistpath = workspace.Path + "/TransmissionNoteList";
                                        var transmissionnotelist = CreateWorkspace(tnlist, tnlisttemplatepath, properties);
                                        properties.Clear();
                                    }
                                    
                                   
                                    //var tnlist = projectexists.Nodes.First().Path + "/" + "Transmission Notes";

                                    Dictionary<string, object> propertiesTN = new Dictionary<string, object>();
                                    propertiesTN.Add("DisplayName", "Transmission Note");
                                    propertiesTN.Add("EADocumentType", recordEX.DocumentNature.Trim());
                                    propertiesTN.Add("EADeliveryDate", Convert.ToDateTime(recordEX.SubmissionDate.Trim()));
                                    propertiesTN.Add("EASubmittedVersion", Int32.Parse(recordEX.Version.Trim()));
                                    //properties.Add("Priority", recordTN.Sector);

                                    propertiesTN.Add("EADecisionDate", Convert.ToDateTime(recordEX.DecisionDate.Trim()));
                                    propertiesTN.Add("EADecision", "None");

                                    if (recordEX.Approved.Equals('X') || recordEX.Approved.Equals('x'))
                                        propertiesTN["EADecision"] = "Approved";
                                    else if (recordEX.Suspended.Equals('X') || recordEX.Suspended.Equals('x'))
                                        propertiesTN["EADecision"] = "Suspended";
                                    else if (recordEX.Rejected.Equals('X') || recordEX.Rejected.Equals('x'))
                                        propertiesTN["EADecision"] = "Rejected";

                                    if (propertiesTN["EADecision"].Equals("Rejected"))
                                        propertiesTN.Add("EARejectionReasons", recordEX.Reason.Trim());

                                    propertiesTN.Add("EADecisionComments", recordEX.Comments);

                                    var tn = CreateEX(Content.Load(tnlist), "Transmission Note", propertiesTN);
                                     //tn["Name"] = name;
                                     //tn.Save();

                                    Console.WriteLine("Created Transmission Note for project {0}, document type '{1}', version '{2}' ", recordEX.ProjectNumber,recordEX.DocumentNature, recordEX.Version);
                                    
                                    //create verso
                                    //if (Node.Exists(tn.Path))
                                    //{
                                        //propertiesTN.Clear();
                                        //Console.WriteLine("Creating Verso to Transmission Note for " + recordEX.DocumentNature);

                                        //propertiesTN.Add("DisplayName", "Verso to Transmission Note");
                                        

                                        //properties.Add("DisplayName", recordTN.ProjectNumber.Trim());
                                        
                                       // var transmissionnotepath = tn.Path;
                                        //var verso = Content.CreateNew("Verso to Transmission Note", transmissionnotepath, "Verso", propertiesTN);
                                        //var verso = CreateVerso(Content.Load(transmissionnotepath),"Verso to Transmission Note",propertiesTN);
                                       // verso.Save();
                                    //}
                                    //else
                                    //{
                                    //    Console.WriteLine("Parent node Transmission Notes does not exist, skipping...");
                                    //    continue;
                                    //}
                                }
                                else{
                                    Console.WriteLine("Project {0} does not exist.", recordEX.ProjectNumber);
                                    continue;
                                }
                                //create transmission note

                                //create verso
                            }
                            break;


                        case "tn":

                            IEnumerable<DataRecordTN> recordsTN = reader.GetRecords<DataRecordTN>();

                            foreach (DataRecordTN recordTN in recordsTN.Take(5))
                            { 
                                //check if parent node exists, if not skip to next
                                var pnumber = HttpUtility.UrlEncode(recordTN.ProjectNumber.Trim());
                                var pnumber_sanitized = pnumber.Replace("%2f", "_2f");
                                var parentpath = targetpath + "/" + pnumber_sanitized;

                                var projectexists = ContentQuery.Query(MySafeQuery.InFolderAndSomeName,null, targetpath, recordTN.ProjectNumber);

                                if ((projectexists.Count >= 1))
                                {
                                    
                                    //create transmission note
                                   
                                    var tnlist = projectexists.Nodes.First().Path + "/" + "Transmission Notes";

                                    Dictionary<string, object> propertiesTN = new Dictionary<string, object>();
                                    propertiesTN.Add("DisplayName", "Transmission Note");
                                    propertiesTN.Add("DocumentType", recordTN.DocumentNature.Trim());
                                    propertiesTN.Add("DeliveryDate", Convert.ToDateTime(recordTN.SubmissionDate.Trim()));
                                    propertiesTN.Add("SubmittedVersion", Int32.Parse(recordTN.Version.Trim()));
                                    //properties.Add("Priority", recordTN.Sector);

                                    var tn = CreateTN(Content.Load(tnlist), "Transmission Note", propertiesTN);
                                     //tn["Name"] = name;
                                     //tn.Save();

                                    Console.WriteLine("Created Transmission Note for project " + recordTN.ProjectNumber);
                                    
                                    //create verso
                                    //if (Node.Exists(tn.Path))
                                    //{
                                        propertiesTN.Clear();
                                        Console.WriteLine("Creating Verso to Transmission Note for " + recordTN.DocumentNature);

                                        propertiesTN.Add("DisplayName", "Verso to Transmission Note");
                                        propertiesTN.Add("DecisionDate", Convert.ToDateTime(recordTN.DecisionDate.Trim()));
                                        propertiesTN.Add("Decision", "None");
                                       
                                        if (recordTN.Approved.Equals('X') || recordTN.Approved.Equals('x'))
                                            propertiesTN["Decision"] = "Approved";
                                        else if (recordTN.Suspended.Equals('X') || recordTN.Suspended.Equals('x'))
                                            propertiesTN["Decision"] = "Suspended";
                                        else if (recordTN.Rejected.Equals('X') || recordTN.Rejected.Equals('x'))
                                            propertiesTN["Decision"] = "Rejected";
                                        
                                        if (propertiesTN["Decision"].Equals("Rejected"))
                                            propertiesTN.Add("RejectionReasons", recordTN.Reason.Trim());

                                        //properties.Add("DisplayName", recordTN.ProjectNumber.Trim());
                                        
                                        var transmissionnotepath = tn.Path;
                                        //var verso = Content.CreateNew("Verso to Transmission Note", transmissionnotepath, "Verso", propertiesTN);
                                        var verso = CreateVerso(Content.Load(transmissionnotepath),"Verso to Transmission Note",propertiesTN);
                                        verso.Save();
                                    //}
                                    //else
                                    //{
                                    //    Console.WriteLine("Parent node Transmission Notes does not exist, skipping...");
                                    //    continue;
                                    //}
                                }
                                else{
                                    Console.WriteLine("Project " + recordTN.ProjectNumber + " does not exist.");
                                    continue;
                                }
                                //create transmission note

                                //create verso
                            }
                            break;
                        case "ir":

                            IEnumerable<DataRecordIR> recordsIR = reader.GetRecords<DataRecordIR>();

                            foreach (DataRecordIR recordIR in recordsIR.Take(10))
                            {
                                //check if project node node exists
                                var pnumber = HttpUtility.UrlEncode(recordIR.ProjectNumber.Trim());
                                var pnumber_sanitized = pnumber.Replace("%2f", "_2f");
                                var parentpath = targetpath + "/" + pnumber_sanitized;

                                var projectexists = ContentQuery.Query(MySafeQuery.InFolderAndSomeName, null, targetpath, recordIR.ProjectNumber.Trim());
                                
                                if (projectexists.Count >=1)
                                {
                                    //create internal review

                                    var irlist = projectexists.Nodes.First().Path + "/" + "Internal Reviews";

                                    Dictionary<string, object> propertiesIR = new Dictionary<string, object>();
                                    propertiesIR.Add("DisplayName", "Internal Review");
                                    propertiesIR.Add("DocumentType", recordIR.DocumentNature.Trim());
                                    propertiesIR.Add("Recommendation", recordIR.RecommendationStatus.Trim());
                                    propertiesIR.Add("DocumentVersion", Int32.Parse(recordIR.Version.Trim()));

                                    propertiesIR.Add("QCOfficer", recordIR.QCOfficer.Trim());
                                    propertiesIR.Add("Supervision", recordIR.Supervision.Trim());

                                    propertiesIR.Add("RecommendationDate", Convert.ToDateTime(recordIR.RecommendationDate.Trim()));
                                    
                                    var internalreview = CreateIR(Content.Load(irlist), "Internal Review", propertiesIR);
                                    //tn["Name"] = name;
                                    //tn.Save();

                                    Console.WriteLine("Created Internal Review for project " + recordIR.ProjectNumber);
                                }
                                else
                                {
                                    Console.WriteLine("Project " + recordIR + " does not exist.");
                                    continue;
                                }
                                
                            }
                            break;
                        default:
                        Console.WriteLine("Content not specified, exiting...");
                        return;
                    
                    }

                }
                //use the Content Repository here: load or save content, etc.
            }


        }

        //private static object CreateTN(Node tnlist, string p, Dictionary<string, object> propertiesTN)
        //{
        //    throw new NotImplementedException();
        //}
        private static Content CreateWorkspace(string targetPath, string templatePath, Dictionary<string, object> properties = null)
        {
            var parentPath = RepositoryPath.GetParentPath(targetPath);
            string name = RepositoryPath.GetFileName(targetPath);
            
            var namesanitized = name.Replace("%2f", "_2f");
            var parent = Node.LoadNode(parentPath);// as GenericContent;
            var template = Node.LoadNode(templatePath);

           // var allowedcontenttypes = parent.GetChildTypesToAllow().ToList();
            //if (allowedcontenttypes.Contains(""))
            //if (parent.IsAllowedChildType("ExAnteReviewList") == false)
            //{
            //    var allowedtypes = parent.GetAllowedChildTypes();
                
            //}
            //var workspace = ContentTemplate.CreateTemplated(parent, template, name);
            var workspace = ContentTemplate.CreateTemplated(parent, template, namesanitized);
            workspace["Name"] = namesanitized;
            //workspace["CreatedBy"] = "Alba Monday";

            if (properties != null)
            {
               foreach (var key in properties.Keys)
                {
                    workspace[key] = properties[key];
                }
            }

            workspace.Save();
            return workspace;
        }
        private static Content CreateEX(Content parent, string name, Dictionary<string, object> propertiesTN = null)
        {
            var tn = Content.CreateNew("ExAnteReview", parent.ContentHandler, name);
            tn["Name"] = name;

            if (propertiesTN != null)
            {
                foreach (var key in propertiesTN.Keys)
                {
                    tn[key] = propertiesTN[key];
                }
            }
            tn.Save();
            return tn;
        }
        private static Content CreateTN(Content parent, string name, Dictionary<string, object> propertiesTN = null)
        {
            var tn = Content.CreateNew("TransmissionNote", parent.ContentHandler, name);
            tn["Name"] =  name;

            if (propertiesTN != null)
            {
                foreach (var key in propertiesTN.Keys)
                {
                    tn[key] = propertiesTN[key];
                }
            }
            tn.Save();
            return tn;
        }
        private static Content CreateVerso(Content parent, string name, Dictionary<string, object> propertiesTN = null)
        {
            var verso = Content.CreateNew("Verso", parent.ContentHandler, name);
            verso["Name"] = name;

            if (propertiesTN != null)
            {
                foreach (var key in propertiesTN.Keys)
                {
                    
                    verso[key] = propertiesTN[key];
                }
            }

            verso.Save();
            return verso;
        }
        private static Content CreateIR(Content parent, string name, Dictionary<string, object> propertiesIR = null)
        {
            var ir = Content.CreateNew("InternalReview", parent.ContentHandler, name);
            ir["Name"] = name;

            if (propertiesIR != null)
            {
                foreach (var key in propertiesIR.Keys)
                {

                    ir[key] = propertiesIR[key];
                }
            }

            ir.Save();
            return ir;
        }
    }
}
